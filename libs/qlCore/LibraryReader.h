/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "LibraryObjects.h"


class QXmlStreamReader;
class QXmlStreamAttribute;

namespace lib
{

    class Library;
    class Cell;

    class CellStreamReader
    {
    public:
        CellStreamReader();

        void setXml(QXmlStreamReader *xml);

        // TODO: Stick with the reader pattern: bool readFooBar(); FooBar *takeFooBar();

        Library *readLibrary(); // FIXME: Split readers
        QList<Cell *> readCells();

        Cell *readCell();
        Attribution readAttribution();
        Documentation readDocumentation();
        QPair<QList<Port *>, QList<Parameter *> > readInterface();
        QList<SourceCodeView *> readImplementations();
        SourceCodeView *readImplementation();
        QList<SymbolView *> readRepresentations();
        SymbolView *readRepresentation();

        QList<Port *> readPorts();
        Port *readPort();
        QList<Parameter *> readParameters();
        Parameter *readParameter();

    private:
        QXmlStreamReader *m_xml;
        Library *m_library = nullptr;
        Cell *m_cell = nullptr;
        void skipCurrentAttributes();
        void skipAttribute(QXmlStreamAttribute attribute);
        void skipCurrentElement();
        void raiseError(const QString &reason);
        Qs::PortDirection portDirection(const QString &text);
        Parameter::DataType dataType(const QString &text);
        bool boolean(const QString &text);
    };

}
