/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "SymbolObjects.h"
#include "SymbolDocument.h"
#include "SymbolCommand.h"

#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>

UndoCommand::UndoCommand(UndoCommand *parent)
    : QUndoCommand(parent)
    , m_document(nullptr)
{

}

UndoCommand::UndoCommand(const QString &text, UndoCommand *parent)
    : QUndoCommand(text, parent)
    , m_document(nullptr)
{

}

UndoCommand::~UndoCommand()
{

}

void UndoCommand::setDocument(SymbolDocument *document)
{
    m_document = document;
    for (int i=0; i<childCount(); i++) {
        // FIXME: This is really gross on is actually UB
        // The proper solution is to make UndoCommand ctor takes a document as parameter
        // Which means that whoever wants to create a command need write access to the document
        auto cmd = const_cast<UndoCommand *>(static_cast<const UndoCommand *>(child(i)));
        cmd->setDocument(m_document);
    }
}

SymbolDocument *UndoCommand::document() const
{
    Q_ASSERT(m_document != nullptr);
    return m_document;
}

CreateObjectCommand::CreateObjectCommand(UndoCommand *parent)
    : UndoCommand("Create object", parent)
{

}

CreateObjectCommand::~CreateObjectCommand()
{

}

void CreateObjectCommand::setTypeName(const QString &name)
{
    m_typeName = name;
}

void CreateObjectCommand::setProperty(const QString &name, const QVariant &value)
{
    m_properties.insert(name, value);
}

void CreateObjectCommand::setProperties(const QMap<QString, QVariant> &properties)
{
    m_properties.unite(properties);
}

CreateObjectCommand *CreateObjectCommand::fromObject(const Object *object, UndoCommand *parent = nullptr)
{
    auto result = new CreateObjectCommand(parent);

    const QMetaObject *mo = object->metaObject();
    result->setTypeName(mo->className());
    while (mo != nullptr) {
        for (int i=0; i<mo->propertyCount(); i++) {
            auto mp = mo->property(i);
            result->setProperty(mp.name(), mp.read(object));
        }
        mo = mo->superClass();
    }
    return result;
}

void CreateObjectCommand::undo()
{
    document()->deleteObject(m_uid);
}

void CreateObjectCommand::redo()
{
    m_uid = document()->createObject(m_typeName, m_uid);
    document()->updateObject(m_uid, m_properties);
}

UpdateObjectCommand::UpdateObjectCommand(UndoCommand *parent)
    : UndoCommand("Update object", parent)
{

}

UpdateObjectCommand::~UpdateObjectCommand()
{

}

void UpdateObjectCommand::setUid(ObjectUid uid)
{
    m_uid = uid;
}

void UpdateObjectCommand::setProperty(const QString &name, const QVariant &value)
{
    m_newProperties.insert(name, value);
}

void UpdateObjectCommand::setProperties(const QMap<QString, QVariant> &properties)
{
    m_newProperties.unite(properties);
}

void UpdateObjectCommand::undo()
{
    document()->updateObject(m_uid, m_oldProperties);
}

void UpdateObjectCommand::redo()
{
    m_oldProperties = document()->readObject(m_uid);
    document()->updateObject(m_uid, m_newProperties);
}

DeleteObjectCommand::DeleteObjectCommand(UndoCommand *parent)
    : UndoCommand("Delete object", parent)
{

}

DeleteObjectCommand::~DeleteObjectCommand()
{

}

void DeleteObjectCommand::setUid(ObjectUid uid)
{
    m_uids.insert(uid);
}

void DeleteObjectCommand::setUids(QSet<ObjectUid> uids)
{
    m_uids = uids;
}

void DeleteObjectCommand::undo()
{
    for (const auto &item: m_items) {
        document()->createObject(item.typeName, item.uid);
        document()->updateObject(item.uid, item.properties);
    }
}

void DeleteObjectCommand::redo()
{
    m_items.clear();
    for (const auto &uid: m_uids) {
        Item item;
        item.typeName = document()->object(uid)->metaObject()->className();
        item.properties = document()->readObject(uid);
        item.uid = uid;
        document()->deleteObject(uid);
        m_items.append(item);
    }
}

MoveObjectCommand::MoveObjectCommand(UndoCommand *parent)
    : UndoCommand("Change object Z-Index", parent)
{

}

MoveObjectCommand::~MoveObjectCommand()
{

}

void MoveObjectCommand::setUid(ObjectUid uid)
{
    m_uid = uid;
}

void MoveObjectCommand::setOperation(Operation operation)
{
    m_operation = operation;
}

void MoveObjectCommand::undo()
{
    document()->moveObject(m_uid, m_oldIndex);
}

void MoveObjectCommand::redo()
{
    m_oldIndex = document()->objectIndex(m_uid);
    switch (m_operation) {
        case MoveObjectCommand::MoveToTop:
            m_newIndex = document()->objectCount() - 1;
            break;
        case MoveObjectCommand::MoveUp:
            m_newIndex = m_oldIndex+1;
            break;
        case MoveObjectCommand::MoveDown:
            m_newIndex = m_oldIndex-1;
            break;
        case MoveObjectCommand::MoveToBottom:
            m_newIndex = 0;
            break;
    }
    document()->moveObject(m_uid, m_newIndex);
}
