#pragma once

#include "Object.h"
#include "QlCore.h"

#include <QtCore/QPointF>
#include <QtCore/QSizeF>

// FIXME: No gui dependencies
#include <QtGui/QColor>

namespace draw
{

    class DrawingObject: public ::Object
    {
        Q_OBJECT
        Q_PROPERTY(QPointF location READ location WRITE setLocation NOTIFY locationChanged)
        Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
        Q_PROPERTY(bool mirrored READ isMirrored WRITE setMirrored NOTIFY mirroredChanged)
        Q_PROPERTY(bool visible READ isVisible WRITE setVisible NOTIFY visibleChanged)
        Q_PROPERTY(bool locked READ isLocked WRITE setLocked NOTIFY lockedChanged)

    public:
        explicit DrawingObject(Object *parent = nullptr);
        ~DrawingObject();

        QPointF location() const;
        qreal rotation() const;
        bool isMirrored() const;
        bool isVisible() const;
        bool isLocked() const;

        // FIXME
        template<class T>
        T *cloneAs() const
        {
            return qobject_cast<T*>(clone());
        }

    public slots:
        void setLocation(const QPointF &location);
        void setRotation(qreal rotation);
        void setMirrored(bool mirrored);
        void setVisible(bool visible);
        void setLocked(bool locked);

    signals:
        void locationChanged(const QPointF &location);
        void rotationChanged(qreal rotation);
        void mirroredChanged(bool mirrored);
        void visibleChanged(bool visible);
        void lockedChanged(bool locked);

    protected:
        void copyDrawingProperties(DrawingObject *other) const;

    private:
        QPointF m_location;
        qreal m_rotation = 0.0;
        bool m_mirrored = false;
        bool m_visible = true;
        bool m_locked = false;
    };

    class Ellipse: public DrawingObject
    {
        Q_OBJECT
        Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
        Q_PROPERTY(qreal startAngle READ startAngle WRITE setStartAngle NOTIFY startAngleChanged)
        Q_PROPERTY(qreal spanAngle READ spanAngle WRITE setSpanAngle NOTIFY spanAngleChanged)
        Q_PROPERTY(Qs::EllipseStyle style READ style WRITE setStyle NOTIFY styleChanged)
        Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
        Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
        Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
        Q_PROPERTY(Qs::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
        Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

    public:

        explicit Ellipse(Object *parent = nullptr);
        ~Ellipse();

        QSizeF size() const;
        qreal startAngle() const;
        qreal spanAngle() const;
        Qs::EllipseStyle style() const;
        Qs::StrokeWidth strokeWidth() const;
        Qs::StrokeStyle strokeStyle() const;
        QColor strokeColor() const;
        Qs::FillStyle fillStyle() const;
        QColor fillColor() const;

    public slots:
        void setSize(const QSizeF &size);
        void setStartAngle(qreal startAngle);
        void setSpanAngle(qreal spanAngle);
        void setStyle(Qs::EllipseStyle style);
        void setStrokeWidth(Qs::StrokeWidth strokeWidth);
        void setStrokeStyle(Qs::StrokeStyle strokeStyle);
        void setStrokeColor(const QColor &strokeColor);
        void setFillStyle(Qs::FillStyle fillStyle);
        void setFillColor(const QColor &fillColor);

    signals:
        void sizeChanged(const QSizeF &size);
        void startAngleChanged(qreal startAngle);
        void spanAngleChanged(qreal spanAngle);
        void styleChanged(Qs::EllipseStyle style);
        void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
        void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
        void strokeColorChanged(const QColor &strokeColor);
        void fillStyleChanged(Qs::FillStyle fillStyle);
        void fillColorChanged(const QColor &fillColor);

    private:
        QSizeF m_size;
        qreal m_startAngle = 0.0;
        qreal m_spanAngle = 360.0;
        Qs::EllipseStyle m_style = Qs::FullEllipsoid;
        Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
        Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
        QColor m_strokeColor = Qt::black;
        Qs::FillStyle m_fillStyle = Qs::NoFill;
        QColor m_fillColor = Qt::white;

        // Object interface
    public:
        virtual Object *clone() const override;
    };

    class Rectangle: public DrawingObject
    {
        Q_OBJECT
        Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
        Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
        Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
        Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
        Q_PROPERTY(Qs::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
        Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

    public:
        explicit Rectangle(Object *parent = nullptr);
        ~Rectangle();

        QSizeF size() const;
        Qs::StrokeWidth strokeWidth() const;
        Qs::StrokeStyle strokeStyle() const;
        QColor strokeColor() const;
        Qs::FillStyle fillStyle() const;
        QColor fillColor() const;

    public slots:
        void setSize(QSizeF size);
        void setStrokeWidth(Qs::StrokeWidth strokeWidth);
        void setStrokeStyle(Qs::StrokeStyle strokeStyle);
        void setStrokeColor(const QColor &strokeColor);
        void setFillStyle(Qs::FillStyle fillStyle);
        void setFillColor(const QColor &fillColor);

    signals:
        void sizeChanged(const QSizeF &size);
        void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
        void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
        void strokeColorChanged(const QColor &strokeColor);
        void fillStyleChanged(Qs::FillStyle fillStyle);
        void fillColorChanged(const QColor &fillColor);

    private:
        QSizeF m_size;
        Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
        Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
        QColor m_strokeColor = Qt::black;
        Qs::FillStyle m_fillStyle = Qs::NoFill;
        QColor m_fillColor = Qt::white;

        // Object interface
    public:
        virtual Object *clone() const override;
    };

    class Line: public DrawingObject
    {
        Q_OBJECT
        Q_PROPERTY(QPointF p1 READ p1 WRITE setP1 NOTIFY p1Changed)
        Q_PROPERTY(QPointF p2 READ p2 WRITE setP2 NOTIFY p2Changed)
        Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
        Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
        Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)

    public:
        explicit Line(Object *parent = nullptr);
        ~Line();

        QPointF p1() const;
        QPointF p2() const;
        Qs::StrokeWidth strokeWidth() const;
        Qs::StrokeStyle strokeStyle() const;
        QColor strokeColor() const;

    public slots:
        void setP1(const QPointF &point);
        void setP2(const QPointF &point);
        void setStrokeWidth(Qs::StrokeWidth strokeWidth);
        void setStrokeStyle(Qs::StrokeStyle strokeStyle);
        void setStrokeColor(const QColor &strokeColor);

    signals:
        void p1Changed(const QPointF &point);
        void p2Changed(const QPointF &point);
        void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
        void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
        void strokeColorChanged(const QColor &strokeColor);

    private:
        QPointF m_p1;
        QPointF m_p2;
        Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
        Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
        QColor m_strokeColor = Qt::black;

        // Object interface
    public:
        virtual Object *clone() const override;
    };

    class Arrow: public DrawingObject
    {
        Q_OBJECT
        Q_PROPERTY(QPointF p1 READ p1 WRITE setP1 NOTIFY p1Changed)
        Q_PROPERTY(QPointF p2 READ p2 WRITE setP2 NOTIFY p2Changed)
        Q_PROPERTY(Qs::ArrowStyle style READ style WRITE setStyle NOTIFY styleChanged)
        Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
        Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
        Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)

    public:
        explicit Arrow(Object *parent = nullptr);
        ~Arrow();

        QPointF p1() const;
        QPointF p2() const;
        Qs::ArrowStyle style() const;
        Qs::StrokeWidth strokeWidth() const;
        Qs::StrokeStyle strokeStyle() const;
        QColor strokeColor() const;

    public slots:
        void setP1(const QPointF &point);
        void setP2(const QPointF &point);
        void setStyle(Qs::ArrowStyle style);
        void setStrokeWidth(Qs::StrokeWidth strokeWidth);
        void setStrokeStyle(Qs::StrokeStyle strokeStyle);
        void setStrokeColor(const QColor &strokeColor);

    signals:
        void p1Changed(const QPointF &point);
        void p2Changed(const QPointF &point);
        void styleChanged(Qs::ArrowStyle style);
        void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
        void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
        void strokeColorChanged(const QColor &strokeColor);

    private:
        QPointF m_p1;
        QPointF m_p2;
        Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
        Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
        QColor m_strokeColor = Qt::black;

        // Object interface
        Qs::ArrowStyle m_style;

    public:
        virtual Object *clone() const override;
    };

    // TODO: Split b/w text frame and label
    class Label: public DrawingObject
    {
        Q_OBJECT
        Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
        Q_PROPERTY(qreal size READ size WRITE setSize NOTIFY sizeChanged)
        Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

    public:
        explicit Label(Object *parent = nullptr);
        ~Label();

        QString text() const;
        qreal size() const;
        QColor color() const;

    public slots:
        void setText(const QString &text);
        void setSize(qreal size);
        void setColor(const QColor &color);

    signals:
        void textChanged(const QString &text);
        void sizeChanged(qreal size);
        void colorChanged(const QColor &color);

    private:
        QString m_text;
        qreal m_size = 10.0;
        QColor m_color = Qt::black;

        // Object interface
    public:
        virtual Object *clone() const override;
    };

}
