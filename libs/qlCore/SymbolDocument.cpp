/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "SymbolObjects.h"
#include "SymbolDocument.h"

#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QStack>

SymbolDocument::SymbolDocument(QObject *parent)
    : QObject(parent)
    , m_root(new sym::Symbol)
{
    m_objectLookupTable.insert(m_root->uid(), m_root.data());
}

SymbolDocument::SymbolDocument(sym::Symbol *symbol, QObject *parent)
    : QObject(parent)
    , m_root(symbol)
{
    Q_ASSERT(m_root != nullptr);
    m_objectLookupTable.insert(m_root->uid(), m_root.data());
    for (auto child: m_root->childObjects())
        m_objectLookupTable.insert(child->uid(), child);
}

SymbolDocument::~SymbolDocument()
{

}

const sym::Symbol *SymbolDocument::symbol() const
{
    return m_root.data();
}

const Object *SymbolDocument::object(ObjectUid uid) const
{
    Q_ASSERT(m_objectLookupTable.contains(uid));
    return m_objectLookupTable.value(uid);
}

ObjectUid SymbolDocument::createObject(const QString &typeName, ObjectUid uid)
{
    Q_ASSERT(m_root != nullptr);

    emit aboutToAddObject();
    Object *object = nullptr;
    if (typeName == "Ellipse") {
        object = new draw::Ellipse(/*m_root*/);
    }
    else if (typeName == "Rectangle") {
        object = new draw::Rectangle(/*m_root*/);
    }
    else if (typeName == "Port") {
        object = new sym::Port(/*m_root*/);
    }
    else if (typeName == "Line") {
        object = new draw::Line(/*m_root*/);
    }
    else if (typeName == "Label") {
        object = new draw::Label(/*m_root*/);
    }
    else {
        Q_UNREACHABLE();
    }
    if (!uid.isNull()) {
        object->setUid(uid);
    }
    object->setParent(m_root.data());
    m_objectLookupTable.insert(object->uid(), object);
    emit objectAdded(object->uid());
    return object->uid();
}

QMap<QString, QVariant> SymbolDocument::readObject(ObjectUid uid) const
{
    Q_ASSERT(m_objectLookupTable.contains(uid));
    QMap<QString, QVariant> result;
    const Object *object = m_objectLookupTable.value(uid);
    const QMetaObject *metaObject = m_objectLookupTable.value(uid)->metaObject();
    for (int i = 0; i<metaObject->propertyCount(); i++) {
        const QMetaProperty metaProperty = metaObject->property(i);
        result.insert(metaProperty.name(), metaProperty.read(object));
    }
    return result;
}

void SymbolDocument::updateObject(ObjectUid uid, const QMap<QString, QVariant> &properties)
{
    Q_ASSERT(m_objectLookupTable.contains(uid));
    emit aboutToUpdateObject(uid);
    Object *object = m_objectLookupTable.value(uid);
    for (const QString &name: properties.keys())
        object->setProperty(name.toLatin1(), properties.value(name));
    emit objectUpdated(uid);
}

void SymbolDocument::deleteObject(ObjectUid uid)
{
    Q_ASSERT(m_objectLookupTable.contains(uid));
    if (m_selection.contains(uid)) {
        auto selection = m_selection;
        selection.remove(uid);
        select(selection);
    }
    emit aboutToRemoveObject(uid);
    delete m_objectLookupTable.take(uid);
    emit objectRemoved();
}

QList<ObjectUid> SymbolDocument::objectUids() const
{
    return m_objectLookupTable.keys();
}

int SymbolDocument::objectCount() const
{
    return m_objectLookupTable.count() - 1; // FIXME: we should count m_root
}

const Object *SymbolDocument::object(int index) const
{
    if (m_root == nullptr || index >= m_root->childObjects().count()) {
        qWarning() << "Child index out of range or no root_object";
        return nullptr;
    }
    return m_root->childObjects().value(index);
}

int SymbolDocument::objectIndex(ObjectUid uid) const
{
    return m_root->childObjects().indexOf(m_objectLookupTable.value(uid));
}

void SymbolDocument::moveObject(ObjectUid uid, int newIndex)
{
    auto objectToMove = m_objectLookupTable.value(uid);
    const int oldIndex = objectIndex(uid);
    const int lowestIndex = qMin(oldIndex, newIndex);
    const int highestIndex = m_root->childObjects().count()-1;

    QS_SOFT_ASSERT_RETURN_VOID(oldIndex >= 0);
    QS_SOFT_ASSERT_RETURN_VOID(newIndex >= 0);
    QS_SOFT_ASSERT_RETURN_VOID(highestIndex >= 0);
    QS_SOFT_ASSERT_RETURN_VOID(newIndex <= highestIndex);

    if (oldIndex == newIndex)
        return;

    emit aboutToMoveObject(uid, newIndex);

    QList<Object *> objects = m_root->childObjects().mid(lowestIndex);
    for (auto object: objects)
        object->setParent(nullptr);
    objects.takeAt(oldIndex-lowestIndex);
    objects.insert(newIndex-lowestIndex, objectToMove);
    for (auto object: objects)
        object->setParent(m_root.data());

    emit objectMoved();
}

void SymbolDocument::select(const QSet<ObjectUid> &objects) const
{
    auto previous = m_selection;
    m_selection = objects;
    emit selectionChanged(m_selection, previous);
}

QList<const QObject *> SymbolDocument::selectedObjects() const
{
    QList<const QObject *> result;
    for (auto uid: m_selection)
        result.append(m_objectLookupTable.value(uid));
    return result;
}

void SymbolDocument::selectAll() const
{
    auto list = m_objectLookupTable.keys();
    list.removeOne(m_root->uid());
    select(list.toSet());
}

void SymbolDocument::clearSelection() const
{
    select(QSet<ObjectUid>());
}

void SymbolDocument::invertSelection() const
{
    auto list = m_objectLookupTable.keys();
    list.removeOne(m_root->uid());
    select(list.toSet() - m_selection);
}

QSet<ObjectUid> SymbolDocument::selection() const
{
    return m_selection;
}
