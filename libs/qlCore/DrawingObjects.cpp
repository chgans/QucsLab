#include "DrawingObjects.h"

namespace draw
{

    /******************************************************************************
 * DrawingObject
 *****************************************************************************/

    DrawingObject::DrawingObject(Object *parent)
        : Object(parent)
    {

    }

    DrawingObject::~DrawingObject()
    {

    }

    QPointF DrawingObject::location() const
    {
        return m_location;
    }

    qreal DrawingObject::rotation() const
    {
        return m_rotation;
    }

    bool DrawingObject::isMirrored() const
    {
        return m_mirrored;
    }

    bool DrawingObject::isVisible() const
    {
        return m_visible;
    }

    bool DrawingObject::isLocked() const
    {
        return m_locked;
    }

    void DrawingObject::setLocation(const QPointF &location)
    {
        if (m_location == location)
            return;

        m_location = location;
        emit locationChanged(location);
    }

    void DrawingObject::setRotation(qreal rotation)
    {
        if (m_rotation == rotation)
            return;

        m_rotation = rotation;
        emit rotationChanged(rotation);
    }

    void DrawingObject::setMirrored(bool mirrored)
    {
        if (m_mirrored == mirrored)
            return;

        m_mirrored = mirrored;
        emit mirroredChanged(mirrored);
    }

    void DrawingObject::setVisible(bool visible)
    {
        if (m_visible == visible)
            return;

        m_visible = visible;
        emit visibleChanged(visible);
    }

    void DrawingObject::setLocked(bool locked)
    {
        if (m_locked == locked)
            return;

        m_locked = locked;
        emit lockedChanged(locked);
    }

    void DrawingObject::copyDrawingProperties(DrawingObject *other) const
    {
        other->m_location = m_location;
        other->m_locked = m_locked;
        other->m_mirrored = m_mirrored;
        other->m_rotation = m_rotation;
        other->m_visible = m_visible;
    }

    /******************************************************************************
 * Line
 *****************************************************************************/

    Line::Line(Object *parent)
        : DrawingObject(parent)
    {

    }

    Line::~Line()
    {

    }

    QPointF Line::p1() const
    {
        return m_p1;
    }

    QPointF Line::p2() const
    {
        return m_p2;
    }

    Qs::StrokeWidth Line::strokeWidth() const
    {
        return m_strokeWidth;
    }

    Qs::StrokeStyle Line::strokeStyle() const
    {
        return m_strokeStyle;
    }

    QColor Line::strokeColor() const
    {
        return m_strokeColor;
    }

    void Line::setP1(const QPointF &point)
    {
        if (m_p1 == point)
            return;
        m_p1 = point;
        emit p1Changed(m_p1);
    }

    void Line::setP2(const QPointF &point)
    {
        if (m_p2 == point)
            return;
        m_p2 = point;
        emit p2Changed(m_p2);
    }

    void Line::setStrokeWidth(Qs::StrokeWidth strokeWidth)
    {
        if (m_strokeWidth == strokeWidth)
            return;

        m_strokeWidth = strokeWidth;
        emit strokeWidthChanged(strokeWidth);
    }

    void Line::setStrokeStyle(Qs::StrokeStyle strokeStyle)
    {
        if (m_strokeStyle == strokeStyle)
            return;

        m_strokeStyle = strokeStyle;
        emit strokeStyleChanged(strokeStyle);
    }

    void Line::setStrokeColor(const QColor &strokeColor)
    {
        if (m_strokeColor == strokeColor)
            return;

        m_strokeColor = strokeColor;
        emit strokeColorChanged(strokeColor);
    }

    Object *Line::clone() const
    {
        auto *other = new Line();
        copyDrawingProperties(other);
        other->m_p1 = m_p1;
        other->m_p2 = m_p2;
        other->m_strokeColor = m_strokeColor;
        other->m_strokeStyle = m_strokeStyle;
        other->m_strokeWidth = m_strokeWidth;
        return other;
    }

    /******************************************************************************
 * Label
 *****************************************************************************/

    Label::Label(Object *parent)
        : DrawingObject(parent)
    {

    }

    Label::~Label()
    {

    }

    QString Label::text() const
    {
        return m_text;
    }

    qreal Label::size() const
    {
        return m_size;
    }

    QColor Label::color() const
    {
        return m_color;
    }

    void Label::setText(const QString &text)
    {
        if (m_text == text)
            return;

        m_text = text;
        emit textChanged(text);
    }

    void Label::setSize(qreal size)
    {
        if (qFuzzyCompare(m_size, size))
            return;

        m_size = size;
        emit sizeChanged(size);
    }

    void Label::setColor(const QColor &color)
    {
        if (m_color == color)
            return;

        m_color = color;
        emit colorChanged(color);
    }

    Object *Label::clone() const
    {
        auto *other = new Label();
        copyDrawingProperties(other);
        other->m_text = m_text;
        other->m_size = m_size;
        other->m_color = m_color;
        return other;
    }

    /******************************************************************************
 * Rectangle
 *****************************************************************************/

    Rectangle::Rectangle(Object *parent)
        : DrawingObject(parent)
    {

    }

    Rectangle::~Rectangle()
    {

    }

    void Rectangle::setSize(QSizeF size)
    {
        if (m_size == size)
            return;

        m_size = size;
        emit sizeChanged(size);
    }

    void Rectangle::setStrokeWidth(Qs::StrokeWidth strokeWidth)
    {
        if (m_strokeWidth == strokeWidth)
            return;

        m_strokeWidth = strokeWidth;
        emit strokeWidthChanged(strokeWidth);
    }

    void Rectangle::setStrokeStyle(Qs::StrokeStyle strokeStyle)
    {
        if (m_strokeStyle == strokeStyle)
            return;

        m_strokeStyle = strokeStyle;
        emit strokeStyleChanged(strokeStyle);
    }

    void Rectangle::setStrokeColor(const QColor &strokeColor)
    {
        if (m_strokeColor == strokeColor)
            return;

        m_strokeColor = strokeColor;
        emit strokeColorChanged(strokeColor);
    }

    void Rectangle::setFillStyle(Qs::FillStyle fillStyle)
    {
        if (m_fillStyle == fillStyle)
            return;

        m_fillStyle = fillStyle;
        emit fillStyleChanged(fillStyle);
    }

    void Rectangle::setFillColor(const QColor &fillColor)
    {
        if (m_fillColor == fillColor)
            return;

        m_fillColor = fillColor;
        emit fillColorChanged(fillColor);
    }

    QSizeF Rectangle::size() const
    {
        return m_size;
    }

    Qs::StrokeWidth Rectangle::strokeWidth() const
    {
        return m_strokeWidth;
    }

    Qs::StrokeStyle Rectangle::strokeStyle() const
    {
        return m_strokeStyle;
    }

    QColor Rectangle::strokeColor() const
    {
        return m_strokeColor;
    }

    Qs::FillStyle Rectangle::fillStyle() const
    {
        return m_fillStyle;
    }

    QColor Rectangle::fillColor() const
    {
        return m_fillColor;
    }

    Object *Rectangle::clone() const
    {
        auto *other = new Rectangle();
        copyDrawingProperties(other);
        other->m_fillColor = m_fillColor;
        other->m_fillStyle = m_fillStyle;
        other->m_size = m_size;
        other->m_strokeColor = m_strokeColor;
        other->m_strokeStyle = m_strokeStyle;
        other->m_strokeWidth = m_strokeWidth;
        return other;
    }

    /******************************************************************************
 * Ellipse
 *****************************************************************************/

    Ellipse::Ellipse(Object *parent)
        : DrawingObject(parent)
    {

    }

    Ellipse::~Ellipse()
    {

    }

    void Ellipse::setStyle(Qs::EllipseStyle style)
    {
        if (m_style == style)
            return;

        m_style = style;
        emit styleChanged(style);
    }

    void Ellipse::setStrokeWidth(Qs::StrokeWidth strokeWidth)
    {
        if (m_strokeWidth == strokeWidth)
            return;

        m_strokeWidth = strokeWidth;
        emit strokeWidthChanged(strokeWidth);
    }

    void Ellipse::setStrokeStyle(Qs::StrokeStyle strokeStyle)
    {
        if (m_strokeStyle == strokeStyle)
            return;

        m_strokeStyle = strokeStyle;
        emit strokeStyleChanged(strokeStyle);
    }

    void Ellipse::setStrokeColor(const QColor &strokeColor)
    {
        if (m_strokeColor == strokeColor)
            return;

        m_strokeColor = strokeColor;
        emit strokeColorChanged(strokeColor);
    }

    void Ellipse::setFillStyle(Qs::FillStyle fillStyle)
    {
        if (m_fillStyle == fillStyle)
            return;

        m_fillStyle = fillStyle;
        emit fillStyleChanged(fillStyle);
    }

    void Ellipse::setFillColor(const QColor &fillColor)
    {
        if (m_fillColor == fillColor)
            return;

        m_fillColor = fillColor;
        emit fillColorChanged(fillColor);
    }

    void Ellipse::setSpanAngle(qreal spanAngle)
    {
        if (m_spanAngle == spanAngle)
            return;

        m_spanAngle = spanAngle;
        emit spanAngleChanged(spanAngle);
    }

    void Ellipse::setStartAngle(qreal startAngle)
    {
        if (m_startAngle == startAngle)
            return;

        m_startAngle = startAngle;
        emit startAngleChanged(startAngle);
    }

    void Ellipse::setSize(const QSizeF &size)
    {
        if (m_size == size)
            return;

        m_size = size;
        emit sizeChanged(size);
    }

    QSizeF Ellipse::size() const
    {
        return m_size;
    }

    qreal Ellipse::startAngle() const
    {
        return m_startAngle;
    }

    qreal Ellipse::spanAngle() const
    {
        return m_spanAngle;
    }

    Qs::EllipseStyle Ellipse::style() const
    {
        return m_style;
    }

    Qs::StrokeWidth Ellipse::strokeWidth() const
    {
        return m_strokeWidth;
    }

    Qs::StrokeStyle Ellipse::strokeStyle() const
    {
        return m_strokeStyle;
    }

    QColor Ellipse::strokeColor() const
    {
        return m_strokeColor;
    }

    Qs::FillStyle Ellipse::fillStyle() const
    {
        return m_fillStyle;
    }

    QColor Ellipse::fillColor() const
    {
        return m_fillColor;
    }

    Object *Ellipse::clone() const
    {
        auto *other = new Ellipse();
        copyDrawingProperties(other);
        other->m_fillColor = m_fillColor;
        other->m_fillStyle = m_fillStyle;
        other->m_size = m_size;
        other->m_spanAngle = m_spanAngle;
        other->m_startAngle = m_startAngle;
        other->m_strokeColor = m_strokeColor;
        other->m_strokeStyle = m_strokeStyle;
        other->m_strokeWidth = m_strokeWidth;
        other->m_style = m_style;
        return other;
    }

    /******************************************************************************
 * Arrow
 *****************************************************************************/

    Arrow::Arrow(Object *parent)
        : DrawingObject(parent)
    {

    }

    Arrow::~Arrow()
    {

    }

    QPointF Arrow::p1() const
    {
        return m_p1;
    }

    QPointF Arrow::p2() const
    {
        return m_p2;
    }

    Qs::StrokeWidth Arrow::strokeWidth() const
    {
        return m_strokeWidth;
    }

    Qs::StrokeStyle Arrow::strokeStyle() const
    {
        return m_strokeStyle;
    }

    QColor Arrow::strokeColor() const
    {
        return m_strokeColor;
    }

    void Arrow::setP1(const QPointF &point)
    {
        if (m_p1 == point)
            return;
        m_p1 = point;
        emit p1Changed(m_p1);
    }

    void Arrow::setP2(const QPointF &point)
    {
        if (m_p2 == point)
            return;
        m_p2 = point;
        emit p2Changed(m_p2);
    }

    void Arrow::setStrokeWidth(Qs::StrokeWidth strokeWidth)
    {
        if (m_strokeWidth == strokeWidth)
            return;

        m_strokeWidth = strokeWidth;
        emit strokeWidthChanged(strokeWidth);
    }

    void Arrow::setStrokeStyle(Qs::StrokeStyle strokeStyle)
    {
        if (m_strokeStyle == strokeStyle)
            return;

        m_strokeStyle = strokeStyle;
        emit strokeStyleChanged(strokeStyle);
    }

    void Arrow::setStrokeColor(const QColor &strokeColor)
    {
        if (m_strokeColor == strokeColor)
            return;

        m_strokeColor = strokeColor;
        emit strokeColorChanged(strokeColor);
    }

    Object *Arrow::clone() const
    {
        auto *other = new Arrow();
        copyDrawingProperties(other);
        other->m_p1 = m_p1;
        other->m_p2 = m_p2;
        other->m_style = m_style;
        other->m_strokeColor = m_strokeColor;
        other->m_strokeStyle = m_strokeStyle;
        other->m_strokeWidth = m_strokeWidth;
        return other;
    }

    void Arrow::setStyle(Qs::ArrowStyle style)
    {
        if (m_style == style)
            return;

        m_style = style;
        emit styleChanged(style);
    }

    Qs::ArrowStyle Arrow::style() const
    {
        return m_style;
    }

}
