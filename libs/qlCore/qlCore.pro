#-------------------------------------------------
#
# Project created by QtCreator 2017-05-11T20:03:47
#
#-------------------------------------------------

# FIXME: Get rid of dependecies on QPen, QBrush and QColor which are part of QtGui
# QT -= gui

TARGET = qlCore
TEMPLATE = lib
CONFIG += staticlib

SOURCES += QlCore.cpp \
    LibraryManager.cpp \
    LibraryObjects.cpp \
    LibraryReader.cpp \
    SchematicObjects.cpp \
    SymbolDocument.cpp \
    SymbolReader.cpp \
    SymbolWriter.cpp \
    SymbolCommand.cpp \
    SymbolObjects.cpp \
    QucsConverter.cpp \
    DrawingObjects.cpp \
    SchematicReader.cpp \
    QucsObjects.cpp \
    QucsReader.cpp \
    Object.cpp

HEADERS += QlCore.h \
    LibraryManager.h \
    LibraryObjects.h \
    LibraryReader.h \
    SchematicObjects.h \
    SymbolDocument.h \
    SymbolReader.h \
    SymbolWriter.h \
    SymbolCommand.h \
    SymbolObjects.h \
    QucsConverter.h \
    DrawingObjects.h \
    SchematicReader.h \
    QucsObjects.h \
    QucsReader.h \
    Object.h

DISTFILES += qlCore.pri

include($$top_srcdir/QucsLab.pri)
