#include "SchematicObjects.h"


/* A Component instance is:
* - References to the library component
*   - lib refID
*   - component refID
*   - representation refID
* - From the library GraphicalRepresentation
*  - a list of drawing objects
*  - a port mapping
* - From the library InterfaceDefinition object
*   - port definition list
*   - parameter definition list
* - From the schematics document
*  - a parameter value map
*  - optional user parameters
*
* TODO: Can be a project sub-circuit or a configuration reference
*/

namespace sch {

    Schematic::Schematic(QObject *parent) :
        QObject(parent)
    {

    }

    void Schematic::addComponent(Component *component)
    {
        m_components.append(component);
    }

    QList<Component *> Schematic::components()
    {
        return m_components;
    }

    void Schematic::addWire(Wire *wire)
    {
        m_wires.append(wire);
    }

    QList<Wire *> Schematic::wires()
    {
        return m_wires;
    }

    void Schematic::addJunction(Junction *junction)
    {
        m_junctions.append(junction);
    }

    QList<Junction *> Schematic::junctions()
    {
        return m_junctions;
    }

    void Schematic::addNetLabel(NetLabel *label)
    {
        m_netLabels.append(label); // FIXME
    }

    QList<NetLabel *> Schematic::netLabels()
    {
        return m_netLabels; // FIME:
    }

    void Schematic::addNetTie(NetTie *tie)
    {
        m_netTies.append(tie);
    }

    QList<NetTie *> Schematic::netTies()
    {
        return m_netTies;
    }

    void Schematic::addDrawing(draw::DrawingObject *drawing)
    {
        m_drawings.append(drawing);
    }

    QList<draw::DrawingObject *> Schematic::drawings()
    {
        return m_drawings;
    }

    void Schematic::setSheetFormat(Qs::SheetFormat format)
    {
        m_sheetFormat = format;
    }

    Qs::SheetFormat Schematic::sheetFormat() const
    {
        return m_sheetFormat;
    }

    void Schematic::setSheetTemplateName(const QString &name)
    {
        m_sheetTemplateName = name;
    }

    QString Schematic::sheetTemplateName() const
    {
        return m_sheetTemplateName;
    }

    void Schematic::setSheetTemplateProperties(const QMap<QString, QString> &properties)
    {
        m_sheetTemplateProperties = properties;
    }

    QMap<QString, QString> Schematic::sheetTemplateProperties() const
    {
        return m_sheetTemplateProperties;
    }

    void Schematic::addDirective(Directive *directive)
    {
        m_directives.append(directive);
    }

    QList<Directive *> Schematic::directives()
    {
        return m_directives;
    }

    void Schematic::addDiagram(Diagram *diagram)
    {
        m_diagrams.append(diagram);
    }

    QList<Diagram *> Schematic::diagrams()
    {
        return m_diagrams;
    }

    void Schematic::addPort(OffSheetPort *port)
    {
        m_ports.append(port);
    }

    QList<OffSheetPort *> Schematic::ports()
    {
        return m_ports;
    }

} // namespace sch
