#include "LibraryManager.h"

#include "LibraryObjects.h"
#include "LibraryReader.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QXmlStreamReader>

LibraryManager::LibraryManager(QObject *parent)
    : QObject(parent)
{

}

LibraryManager::~LibraryManager()
{

}

QList<QString> LibraryManager::systemPathList() const
{
    return m_systemPathList;
}

QList<QString> LibraryManager::userPathList() const
{
    return m_userPathList;
}

void LibraryManager::loadLibraries()
{
    emit aboutToLoadLibraries();

    qDeleteAll(m_libraries);
    m_libraries.clear();

    for (auto library: loadLibraries(m_systemPathList)) {
        library->origin = lib::Library::SystemLibrary;
        m_libraries.append(library);
    }
    for (auto library: loadLibraries(m_userPathList)) {
        library->origin = lib::Library::UserLibrary;
        m_libraries.append(library);
    }

    setLoadNeeded(false);

    emit librariesLoaded();
}

bool LibraryManager::isLoadNeeded() const
{
    return m_loadNeeded;
}

QList<lib::Library *> LibraryManager::loadLibraries(const QList<QString> &pathList)
{
    QList<lib::Library *> result;
    for (const auto &dirPath: pathList) {
        for (const auto &entry: listLibraryFiles(dirPath)) {
            QFile file(entry);
            bool opened = file.open(QFile::ReadOnly);
            Q_ASSERT(opened); // Should not failed, see listLibraryFiles()
            QXmlStreamReader xml;
            xml.setDevice(&file);
            xml.readNextStartElement();
            lib::CellStreamReader reader;
            reader.setXml(&xml);
            auto library = reader.readLibrary();
            if (library == nullptr) {
                qWarning() << QString("Failed to load library '%1': %2").arg(entry, xml.errorString());
                continue;
            }
            library->path = entry;
            library->identification.caption = QFileInfo(entry).baseName();
            result.append(library);
        }
    }
    return result;
}

int LibraryManager::categoryCount()
{
    return m_libraries.count();
}

QList<lib::Cell *> LibraryManager::cells(QString &category) const
{
    for (const auto library: m_libraries)
        if (library->identification.caption == category)
            return library->cells;
    return QList<lib::Cell *>();
}

lib::Cell *LibraryManager::cellByName(const QString &modelName, const QString &type)
{
    for (auto library: m_libraries) {
        for (auto cell: library->cells) {
            if (cell->identification.uid.startsWith(QString("qucs:%1::").arg(modelName)))
                return cell; // Cell doesn't support sub-type
            if (cell->identification.uid.startsWith(QString("qucs:%1:%2:").arg(modelName).arg(type)))
                return cell; // Cell sub-type matches
        }
    }
    return nullptr;
}

lib::Cell *LibraryManager::cellByUid(const QString &uid) const
{
    for (auto library: m_libraries) {
        for (auto cell: library->cells) {
            if (cell->identification.uid.startsWith(uid))
                return cell;
        }
    }
    return nullptr;
}

// TODO: Return a question mark drawing for symbol w/o representation
// Same for unknow symbol
sym::Symbol *LibraryManager::createSymbol(lib::Cell *cell, const QString &which) const
{
    Q_ASSERT(cell->m_views.count() != 0);
    for (auto view: cell->m_views) {
        if (view->type() != lib::SymbolView::Type)
            continue;
        auto symbolView = static_cast<lib::SymbolView *>(view);
        if (symbolView->identification.caption == which) {
            auto symbol = new sym::Symbol();
            symbol->setCaption(cell->identification.caption);
            symbol->setDescription(cell->identification.description);
            for (const auto drawing: symbolView->drawings)
                symbol->addDrawing(drawing->cloneAs<draw::DrawingObject>());
            return symbol;
        }
    }
    return nullptr;
}

sym::Symbol *LibraryManager::createSymbol(const QString &modelName, const QString &symbolName) const
{
    for (auto library: m_libraries) {
        for (auto cell: library->cells) {

            if (!cell->identification.uid.startsWith(modelName))
                continue;

            auto symbolViews = cell->views<lib::SymbolView>();
            lib::SymbolView *symbolView = nullptr;
            if (symbolName.isEmpty() && !symbolViews.isEmpty())
                symbolView = symbolViews.first();
            else
                for (auto candidate: symbolViews)
                    if (candidate->identification.caption == symbolName)
                        symbolView = candidate;
            if (symbolView == nullptr)
                continue;

            auto symbol = new sym::Symbol();
            symbol->setCaption(cell->identification.caption);
            symbol->setDescription(cell->identification.description);
            for (const auto drawing: symbolView->drawings)
                symbol->addDrawing(drawing->cloneAs<draw::DrawingObject>());
            return symbol;
        }
    }
    return nullptr;
}

void LibraryManager::setSystemPathList(const QList<QString> &list)
{
    m_systemPathList = list;
    setLoadNeeded(true);
}

void LibraryManager::addSystemPath(const QString &path)
{
    m_systemPathList.append(path);
    setLoadNeeded(true);
}

void LibraryManager::removeSystemPath(const QString &path)
{
    m_systemPathList.removeOne(path);
    setLoadNeeded(true);
}

void LibraryManager::setUserPathList(const QList<QString> &list)
{
    m_userPathList = list;
    setLoadNeeded(true);
}

void LibraryManager::addUserPath(const QString &path)
{
    m_userPathList.append(path);
    setLoadNeeded(true);
}

void LibraryManager::removeUserPath(const QString &path)
{
    m_userPathList.removeOne(path);
    setLoadNeeded(true);
}

void LibraryManager::setLoadNeeded(bool needed)
{
    if (m_loadNeeded == needed)
        return;
    m_loadNeeded = needed;
    emit loadNeededChanged(m_loadNeeded);
}

QList<QString> LibraryManager::listLibraryFiles(const QString &path)
{
    QDir dir(path);
    if (!dir.exists() || !dir.isReadable())
        return QList<QString>();
    dir.setFilter(QDir::Files | QDir::Readable | QDir::NoSymLinks);
    dir.setNameFilters(QStringList() << "*.qlib");
    QList<QString> result;
    for (const auto &entry: dir.entryList())
        result.append(dir.filePath(entry));
    return result;
}
