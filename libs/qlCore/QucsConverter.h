#ifndef QUCSCONVERTER_H
#define QUCSCONVERTER_H

#include "QlCore.h"

#include <QtCore/QPointF>
#include <QtCore/QSizeF>
#include <QtCore/QScopedPointer>
// FIXME: no QtGui dependencies
#include <QtGui/QColor>

// QucsLab drawing
namespace draw {
    class DrawingObject;
}

// QucsLab schematic
namespace sch {
    class DesignatorLabel;
    class Component;
    class NetLabel;
    class NetTie;
    class OffSheetPort;
    class Wire;
    class Junction;
    class Directive;
    class Diagram;
    class Schematic;
}

// QucsLab Library
class LibraryManager;

namespace qucs
{
    namespace drawing
    {
        class Pen;
        class Brush;
        class Text;
        class Rectangle;
        class Ellipse;
        class Arc;
        class Line;
        class Arrow;
        class Drawings;
    }
    namespace symbol
    {
        class Port;
        class Property;
        class Symbol;
    }
    namespace diagram
    {
        class Marker;
        class Graph;
        class Axis;
        class Diagram;
    }
    namespace schematic
    {
        class Property;
        class Component;
        class Label;
        class Wire;
        class Schematic;
    }

    class Converter
    {
    public:
        Converter();

        void setLibraryManager(const LibraryManager *manager);
        const LibraryManager *libraryManager() const;

        sch::Schematic *convert(const qucs::schematic::Schematic *qucs);

    private:
        QScopedPointer<sch::Schematic> m_schematic;
        const LibraryManager *m_libraryManager;
        QPointF m_locationOffset;
        sch::Wire *createWire(const qucs::schematic::Wire *wire);
        sch::Component *createComponent(qucs::schematic::Component *qucsComponent);
        sch::OffSheetPort *createOffSheetPort(qucs::schematic::Component * qucsComponent);
        sch::NetTie *createNetTie(qucs::schematic::Component *qucsComponent);
        sch::Directive *createDirective(qucs::schematic::Component *qucsComponent);
        sch::Diagram *createDiagram(qucs::diagram::Diagram *qucsDiagram);
        QList<draw::DrawingObject *> createDrawings(const qucs::drawing::Drawings &drawings);
        draw::DrawingObject *createDrawing(qucs::drawing::Arrow *qucs);
        draw::DrawingObject *createDrawing(qucs::drawing::Rectangle *qucs);
        draw::DrawingObject *createDrawing(qucs::drawing::Ellipse *qucs);
        draw::DrawingObject *createDrawing(qucs::drawing::Arc *qucs);
        draw::DrawingObject *createDrawing(qucs::drawing::Line *qucs);
        draw::DrawingObject *createDrawing(qucs::drawing::Text *qucs);

        //void createPort(const QString &line);

        static qreal distance(int value);
        static qreal angle(int value);
        static qreal rotation(schematic::Component *component);
        QPointF location(const QPoint &value);
        inline QPointF location(int x, int y)
        { return location(QPoint(x, y)); }
        static QSizeF size(const QSize &value);
        static qreal textSize(int value);
        static QString text(const QString &value);
        static Qs::ArrowStyle arrowStyle(int value);
        static Qs::StrokeStyle strokeStyle(int value);
        static Qs::StrokeWidth strokeWidth(int value);
        static QColor color(const QString &value);
        static Qs::FillStyle fillStyle(int value, int filled);
        static Qs::SheetFormat sheetFormat(int value);

        void populateParameters(sch::Component *schComponent, const qucs::schematic::Component *qucsComponent);
    };
}
#endif // QUCSCONVERTER_H
