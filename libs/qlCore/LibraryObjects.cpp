/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "LibraryObjects.h"

namespace lib
{

    Parameter::Parameter(QObject *parent) : QObject(parent)
    {

    }

    QString Parameter::name() const
    {
        return m_name;
    }

    Parameter::DataType Parameter::dataType() const
    {
        return m_dataType;
    }

    QString Parameter::unit() const
    {
        return m_unit;
    }

    bool Parameter::visible() const
    {
        return m_visible;
    }

    QString Parameter::documentation() const
    {
        return m_documentation;
    }

    QString Parameter::defaultValue() const
    {
        return m_defaultValue;
    }

    QString Parameter::restriction() const
    {
        return m_restriction;
    }

    void Parameter::setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void Parameter::setDataType(Parameter::DataType dataType)
    {
        if (m_dataType == dataType)
            return;

        m_dataType = dataType;
        emit dataTypeChanged(dataType);
    }

    void Parameter::setUnit(QString unit)
    {
        if (m_unit == unit)
            return;

        m_unit = unit;
        emit unitChanged(unit);
    }

    void Parameter::setVisible(bool visible)
    {
        if (m_visible == visible)
            return;

        m_visible = visible;
        emit visibleChanged(visible);
    }

    void Parameter::setDocumentation(QString documentation)
    {
        if (m_documentation == documentation)
            return;

        m_documentation = documentation;
        emit documentationChanged(documentation);
    }

    void Parameter::setDefaultValue(QString defaultValue)
    {
        if (m_defaultValue == defaultValue)
            return;

        m_defaultValue = defaultValue;
        emit defaultValueChanged(defaultValue);
    }

    void Parameter::setRestriction(QString restriction)
    {
        if (m_restriction == restriction)
            return;

        m_restriction = restriction;
        emit restrictionChanged(restriction);
    }


    Port::Port(QObject *parent) : QObject(parent)
    {

    }

    Qs::PortDirection Port::direction() const
    {
        return m_direction;
    }

    QString Port::name() const
    {
        return m_name;
    }

    QString Port::documentation() const
    {
        return m_documentation;
    }

    void Port::setDirection(Qs::PortDirection direction)
    {
        if (m_direction == direction)
            return;

        m_direction = direction;
        emit directionChanged(direction);
    }

    void Port::setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void Port::setDocumentation(QString documentation)
    {
        if (m_documentation == documentation)
            return;

        m_documentation = documentation;
        emit documentationChanged(documentation);
    }

}
