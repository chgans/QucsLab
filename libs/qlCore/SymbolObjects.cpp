/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "SymbolObjects.h"
#include "SymbolCommand.h"

// For ObjectProxy
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QEvent>

namespace sym
{

    Symbol::Symbol(Object *parent)
        : Object(parent)
    {

    }

    Symbol::~Symbol()
    {

    }

    QString Symbol::caption() const
    {
        return m_caption;
    }

    QString Symbol::description() const
    {
        return m_description;
    }

    void Symbol::addDrawing(draw::DrawingObject *drawing)
    {
        drawing->setParent(this);
    }

    QList<draw::DrawingObject *> Symbol::drawings()
    {
        // FIXME:
        QList<draw::DrawingObject *> result;
        for (Object *drawing: childObjects())
            result.append(qobject_cast<draw::DrawingObject *>(drawing));
        return result;
    }

    void Symbol::setCaption(QString caption)
    {
        if (m_caption == caption)
            return;

        m_caption = caption;
        emit captionChanged(caption);
    }

    void Symbol::setDescription(QString description)
    {
        if (m_description == description)
            return;

        m_description = description;
        emit descriptionChanged(description);
    }

    Port::Port(Object *parent)
        : DrawingObject(parent)
    {

    }

    Port::~Port()
    {

    }

    QString Port::name() const
    {
        return m_name;
    }

    void Port::setName(const QString &name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    Object *Port::clone() const
    {
        auto *other = new Port();
        copyDrawingProperties(other);
        other->m_name = m_name;
        return other;
    }

    Object *Symbol::clone() const
    {
        Symbol *other = new Symbol();
        other->m_caption = m_caption;
        other->m_description = m_description;
        copyProperties(other);
        for (Object *child: findChildren<Object*>(QString(), Qt::FindDirectChildrenOnly)) {
            auto otherChild = child->clone();
            otherChild->setParent(other);
        }
        return other;
    }

}



ObjectProxy::ObjectProxy(QObject *parent)
    : QObject(parent)
{

}

ObjectProxy::~ObjectProxy()
{

}

bool ObjectProxy::event(QEvent *event)
{
    if (event->type() != QEvent::DynamicPropertyChange)
        return QObject::event(event);

    // A property has been added, removed or changed
    // FIXME: We assume a property change event
    auto changeEvent = static_cast<QDynamicPropertyChangeEvent *>(event);
    const auto name = changeEvent->propertyName();
    const auto newValue = property(name);
    const auto currentValue = m_object->property(name);

    // Don't emit a commad if the property value hasn't change on the proxied object
    if (newValue == currentValue)
        return true;

    m_command.reset(new UpdateObjectCommand());
    m_command->setUid(m_object->uid());
    m_command->setProperty(name, newValue);
    emit commandAvailable();

    return true;
}

void ObjectProxy::setObject(const Object *object)
{
    m_object = object;
    // Discard pending command if any
    delete m_command.take();

    if (m_object == nullptr)
        return;

    const auto metaObject = m_object->metaObject();
    for (int i=0; i<metaObject->propertyCount(); i++) {
        const auto metaProperty = metaObject->property(i);
        setProperty(metaProperty.name(), metaProperty.read(m_object));
    }
}

const Object *ObjectProxy::object() const
{
    return m_object;
}

UndoCommand *ObjectProxy::takeCommand()
{
    return m_command.take();
}
