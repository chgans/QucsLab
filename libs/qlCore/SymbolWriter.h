/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/QlCore.h"

#include <QtCore/QScopedPointer>

namespace sym
{
    class Symbol;
    class Port;
}

namespace draw {
    class Ellipse;
    class Line;
    class Label;
    class Rectangle;
}

class QIODevice;
class QString;

class ISymbolWriter
{
public:
    ISymbolWriter();
    virtual ~ISymbolWriter();

    virtual bool writeSymbol(QIODevice *device, const sym::Symbol *symbol) = 0;
    virtual bool hasError() const = 0;
    virtual QString errorString() const = 0;
};

class SymbolWriterPrivate;
class SymbolWriter: public ISymbolWriter
{
    Q_DECLARE_PRIVATE(SymbolWriter)
    QScopedPointer<SymbolWriterPrivate> d_ptr;

public:
    SymbolWriter();
    ~SymbolWriter();

    virtual bool writeSymbol(QIODevice *device, const sym::Symbol *symbol) override;
    virtual bool hasError() const override;
    virtual QString errorString() const override;

    bool writeObjects(QIODevice *device, const QList<const QObject *> &objects);
};


class QucsSymbolWriter: public ISymbolWriter
{
public:
    QucsSymbolWriter();
    ~QucsSymbolWriter();

    virtual bool writeSymbol(QIODevice *device, const sym::Symbol *symbol) override;
    virtual bool hasError() const override;
    virtual QString errorString() const override;

private:
    void writeId(const sym::Symbol *object);
    void writeRectangle(const draw::Rectangle *object);
    void writeEllipse(const draw::Ellipse *object);
    void writeLine(const draw::Line *object);
    void writeLabel(const draw::Label *object);
    void writePort(const sym::Port *object);
    static QString distance(qreal value);
    static QString angle(qreal value);
    static QString textSize(qreal value);
    static QString color(const QColor &value);
    static QString strokeWidth(Qs::StrokeWidth value);
    static QString strokeStyle(Qs::StrokeStyle value);
    static QString fillStyle(Qs::FillStyle value);
    static QString filled(Qs::FillStyle value);
    QIODevice *m_device = nullptr;
    QString m_errorString;
};
