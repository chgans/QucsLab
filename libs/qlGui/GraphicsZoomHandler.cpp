/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsView.h"
#include "GraphicsZoomHandler.h"

#include <QtCore/QtMath>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>


GraphicsZoomHandler::GraphicsZoomHandler(QObject *parent)
    : QObject (parent)
    , m_view(nullptr)
    , m_modifiers(Qt::ControlModifier)
    , m_baseZoomFactor(1.0015)
{

}

GraphicsZoomHandler::~GraphicsZoomHandler()
{

}

void GraphicsZoomHandler::setView(GraphicsView *view)
{
    if (m_view != nullptr)
        m_view->uninstallEventHandler(this);

    m_view = view;

    if (m_view != nullptr)
        m_view->installEventHandler(this);
}

GraphicsView *GraphicsZoomHandler::view() const
{
    return m_view;
}

void GraphicsZoomHandler::zoom(double factor)
{
    if (factor > 1.0 && m_view->transform().m11() > m_maxZoom)
        return;

    if (factor < 1.0 &&  m_view->transform().m11() < m_minZoom)
        return;

    m_view->scale(factor, factor);
    m_view->centerOn(m_targetScenePos);
    QPointF deltaViewportPos = m_targetViewportPos - QPointF(m_view->viewport()->width() / 2.0,
                                                             m_view->viewport()->height() / 2.0);
    QPointF viewportCenter = m_view->mapFromScene(m_targetScenePos) - deltaViewportPos;
    m_view->centerOn(m_view->mapToScene(viewportCenter.toPoint()));
}

void GraphicsZoomHandler::setMaximumZoomFactor(qreal max)
{
    m_maxZoom = max;
}

void GraphicsZoomHandler::setMinimumZoomFactor(qreal min)
{
    m_minZoom = min;
}

void GraphicsZoomHandler::setModifiers(Qt::KeyboardModifiers modifiers)
{
    m_modifiers = modifiers;
}

void GraphicsZoomHandler::setBaseZoomFactor(double value)
{
    m_baseZoomFactor = value;
}

bool GraphicsZoomHandler::graphicsViewWheelEvent(GraphicsView *view, QWheelEvent *event)
{
    Q_UNUSED(view);

    if (QApplication::keyboardModifiers() != m_modifiers)
        return false;

    if (event->orientation() != Qt::Vertical)
        return false;

    double angle = event->angleDelta().y();
    double factor = qPow(m_baseZoomFactor, angle);
    zoom(factor);
    return true;
}

bool GraphicsZoomHandler::graphicsViewMouseMoveEvent(GraphicsView *view, QMouseEvent *event)
{
    Q_UNUSED(view);

    QPointF delta = m_targetViewportPos - event->pos();
    if (qAbs(delta.x()) > 5 || qAbs(delta.y()) > 5) {
        m_targetViewportPos = event->pos();
        m_targetScenePos = m_view->mapToScene(event->pos());
    }
    return false;
}
