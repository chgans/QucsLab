/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "GraphicsEventHandler.h"

#include <QtCore/QObject>
#include <QtCore/QPointF>

class GraphicsView;
class GraphicsScene;

// TODO: Snap strategy: grid, center/middle/end point, shape, ...
class GraphicsSnapper : public QObject, public GraphicsSceneEventHandler
{
    Q_OBJECT
    Q_PROPERTY(qreal step READ step WRITE setStep NOTIFY stepChanged)

public:
    explicit GraphicsSnapper(QObject *parent = 0);
    ~GraphicsSnapper();

    void setView(GraphicsView *view);
    GraphicsView *view() const;

    qreal step() const;

    void snapEvent(const QGraphicsSceneMouseEvent *sourceEvent, QGraphicsSceneMouseEvent *event);
    QPoint snapScreenPos(const QPoint &pos);
    QPointF snapScenePos(const QPointF &pos);
    QPointF snapItemPos(const QPointF &pos);

signals:
    void stepChanged(qreal step);

public slots:
    void setStep(qreal step);

private:
    GraphicsView *m_view = nullptr;
    GraphicsScene *m_scene = nullptr;
    qreal m_step = 10.0;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};
