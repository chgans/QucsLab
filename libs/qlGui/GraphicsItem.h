/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "GraphicsHandle.h"

#include "qlCore/QlCore.h"

#include <QtGui/QPen>
#include <QtGui/QBrush>
#include <QtWidgets/QGraphicsItem>

class GraphicsEllipseItem : public QGraphicsItem, public GraphicsHandleContainer
{
public:
    enum {
        Type = UserType + 0x100 + 0x01
    };

    explicit GraphicsEllipseItem(QGraphicsItem *parent = nullptr);
    ~GraphicsEllipseItem();

    void setSize(const QSizeF &size);
    QSizeF size() const;
    void setStyle(Qs::EllipseStyle style);
    Qs::EllipseStyle style() const;
    void setStartAngle(qreal angle);
    qreal startAngle() const;
    void setSpanAngle(qreal angle);
    qreal spanAngle() const;
    void setPen(const QPen &pen);
    QPen pen() const;
    void setBrush(const QBrush &brush);
    QBrush brush() const;

private:
    void updateHandles();
    QSizeF m_size;
    Qs::EllipseStyle m_style;
    qreal m_startAngle;
    qreal m_spanAngle;
    QPen m_pen;
    QBrush m_brush;
    QPainterPath createPath() const;

    // QGraphicsItem interface
public:
    virtual int type() const override;
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // GraphicsHandleContainer interface
protected:
    virtual void handleScenePositionHasChanged(int id, const QPointF &scenePos) override;
};


class GraphicsRectangleItem : public QGraphicsItem, public GraphicsHandleContainer
{
public:
    enum {
        Type = UserType + 0x100 +0x02
    };
    explicit GraphicsRectangleItem(QGraphicsItem *parent = nullptr);
    ~GraphicsRectangleItem();

    void setSize(const QSizeF &size);
    QSizeF size() const;
    void setPen(const QPen &pen);
    QPen pen() const;
    void setBrush(const QBrush &brush);
    QBrush brush() const;

private:
    void updateHandles();
    QSizeF m_size;
    QPen m_pen;
    QBrush m_brush;


    // QGraphicsItem interface
public:
    virtual int type() const override;
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // GraphicsHandleContainer interface
protected:
    virtual void handleScenePositionHasChanged(int id, const QPointF &scenePos) override;
};

// FIXME: Symbol port (when defining a symbol) vs Sheet port (when defining a sub-schematic)
// vs Symbol port (when rendering a symbol on a schematic sheet)
class GraphicsPortItem : public QGraphicsItem
{
public:
    enum {
        Type = UserType + 0x100 +0x03
    };

    explicit GraphicsPortItem(QGraphicsItem *parent = nullptr);
    ~GraphicsPortItem();

    void setName(const QString &name);
    QString name() const;
    void setPen(const QPen &pen);
    QPen pen() const;

private:
    QSizeF m_size;
    QPen m_pen;
    QBrush m_brush;
    QGraphicsSimpleTextItem *m_textItem;

    // QGraphicsItem interface
public:
    virtual int type() const override;
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};

// FIXME: filter position change and update P1 & p2 instead
class GraphicsLineItem : public QGraphicsItem, public GraphicsHandleContainer
{
public:
    enum {
        Type = UserType + 0x100 +0x04
    };

    explicit GraphicsLineItem(QGraphicsItem *parent = nullptr);
    ~GraphicsLineItem();

    void setP1(const QPointF &point);
    QPointF p1() const;
    void setP2(const QPointF &point);
    QPointF p2() const;
    void setPen(const QPen &pen);
    QPen pen() const;

private:
    void updateHandles();
    QPointF m_p1;
    QPointF m_p2;
    QPen m_pen;

    // QGraphicsItem interface
public:
    virtual int type() const override;
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // GraphicsHandleContainer interface
protected:
    virtual void handleScenePositionHasChanged(int id, const QPointF &scenePos) override;
};

class GraphicsLabelItem : public QGraphicsItem
{
public:
    enum {
        Type = UserType + 0x100 +0x05
    };

    explicit GraphicsLabelItem(QGraphicsItem *parent = nullptr);
    ~GraphicsLabelItem();

    void setText(const QString &text);
    QString text() const;
    void setFont(const QFont &font);
    QFont font() const;
    void setBrush(const QBrush &brush);
    QBrush brush() const;

private:
    QGraphicsSimpleTextItem *m_textItem;

    // QGraphicsItem interface
public:
    virtual int type() const override;
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};

class GraphicsArrowItem : public QGraphicsItem, public GraphicsHandleContainer
{
public:
    enum {
        Type = UserType + 0x100 +0x06
    };

    explicit GraphicsArrowItem(QGraphicsItem *parent = nullptr);
    ~GraphicsArrowItem();

    void setEnd(const QPointF &point);
    QPointF end() const;
    void setStart(const QPointF &point);
    QPointF start() const;
    void setHeadWidth(qreal width);
    qreal headWidth() const;
    void setHeadHeight(qreal height);
    qreal headHeight() const;
    void setStyle(Qs::ArrowStyle style);
    Qs::ArrowStyle style() const;
    void setPen(const QPen &pen);
    QPen pen() const;

private:
    void updateHandles();
    void calculateArrow();
    QPointF m_end;
    QPointF m_start;
    QPolygonF m_head;
    qreal m_headWidth = 3;
    qreal m_headHeight = 3;
    Qs::ArrowStyle m_style = Qs::FilledHead;
    QPen m_pen;

    // QGraphicsItem interface
public:
    virtual int type() const override { return Type; }
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // GraphicsHandleContainer interface
protected:
    virtual void handleScenePositionHasChanged(int id, const QPointF &scenePos) override;
};
