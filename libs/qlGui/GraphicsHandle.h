/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QMap>
#include <QtCore/QtGlobal>
#include <QtWidgets/QGraphicsRectItem>

class GraphicsHandleContainer;

class GraphicsHandle: public QGraphicsRectItem
{
public:
    enum {
        Type = 0x200 + 0x01
    };

    explicit GraphicsHandle(GraphicsHandleContainer *container, QGraphicsItem *target);
    ~GraphicsHandle();

    void setId(int id);
    int id() const;

    QGraphicsItem *target() const;

private:
    GraphicsHandleContainer *m_container;
    QGraphicsItem *m_target;
    int m_id = -1;

    // QGraphicsItem interface
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // QGraphicsItem interface
public:
    virtual int type() const override { return Type; }
};

// move and handle pos changed could conveniently be expressed in target CS

class GraphicsHandleContainer
{
public:
    GraphicsHandleContainer(QGraphicsItem *target);
    virtual ~GraphicsHandleContainer();

protected:
    void addHandle(int id, Qt::CursorShape shape = Qt::ArrowCursor);
    void showHandles(bool show = true);
    void moveHandle(int id, const QPointF &pos);
    void moveHandle(int id, qreal x, qreal y);
    void clearHandles();

    virtual void handleScenePositionHasChanged(int id, const QPointF &pos);

private:
    friend class GraphicsHandle;
    QMap<int, GraphicsHandle *> m_handles;
    QGraphicsItem *m_target;
};
