/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsEventHandler.h"
#include "GraphicsScene.h"
#include "GraphicsView.h"

#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QMimeData>
#include <QtCore/QPointF>
#include <QtGui/QBrush>
#include <QtGui/QDragEnterEvent>
#include <QtGui/QDropEvent>
#include <QtGui/QLinearGradient>
#include <QtWidgets/QGraphicsItemGroup>

GraphicsView::GraphicsView(QWidget *parent)
    : QGraphicsView (parent)
{
    setMouseTracking(true);
}

GraphicsView::~GraphicsView()
{

}

void GraphicsView::installEventHandler(GraphicsViewEventHandler *handler)
{
    m_eventHandlers.append(handler);
}

void GraphicsView::uninstallEventHandler(GraphicsViewEventHandler *handler)
{
    m_eventHandlers.removeOne(handler);
}

GraphicsScene *GraphicsView::graphicsScene() const
{
    return static_cast<GraphicsScene *>(scene());
}

void GraphicsView::drawBackground(QPainter *painter, const QRectF &rect)
{
    QLinearGradient gradient(0, mapToScene(viewport()->rect().topRight()).y(),
                             0, mapToScene(viewport()->rect().bottomRight()).y());
    gradient.setColorAt(0.0, backgroundBrush().color());
    gradient.setColorAt(1.0, backgroundBrush().color().darker(115));
    QBrush brush(gradient);
    painter->setClipRect(rect);
    painter->fillRect(mapToScene(viewport()->rect()).boundingRect(), brush);
}

void GraphicsView::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter);
    Q_UNUSED(rect);
}

void GraphicsView::contextMenuEvent(QContextMenuEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewContextMenuEvent(this, event))
            return;
    QGraphicsView::contextMenuEvent(event);
}

void GraphicsView::dragEnterEvent(QDragEnterEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewDragEnterEvent(this, event))
            return;
    QGraphicsView::dragEnterEvent(event);
}

void GraphicsView::dragLeaveEvent(QDragLeaveEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewDragLeaveEvent(this, event))
            return;
    QGraphicsView::dragLeaveEvent(event);
}

void GraphicsView::dragMoveEvent(QDragMoveEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewDragMoveEvent(this, event))
            return;
    QGraphicsView::dragMoveEvent(event);
}

void GraphicsView::dropEvent(QDropEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewDropEvent(this, event))
            return;
    QGraphicsView::dropEvent(event);
}

void GraphicsView::keyPressEvent(QKeyEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewKeyPressEvent(this, event))
            return;
    QGraphicsView::keyPressEvent(event);
}

void GraphicsView::keyReleaseEvent(QKeyEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewKeyReleaseEvent(this, event))
            return;
    QGraphicsView::keyReleaseEvent(event);
}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewMousePressEvent(this, event))
            return;
    QGraphicsView::mousePressEvent(event);
}

void GraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewMouseReleaseEvent(this, event))
            return;
    QGraphicsView::mouseReleaseEvent(event);
}

void GraphicsView::mouseDoubleClickEvent(QMouseEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewMouseDoubleClickEvent(this, event))
            return;
    QGraphicsView::mouseDoubleClickEvent(event);
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewMouseMoveEvent(this, event))
            return;
    QGraphicsView::mouseMoveEvent(event);
}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsViewWheelEvent(this, event))
            return;
    QGraphicsView::wheelEvent(event);
}
