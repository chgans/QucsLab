/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsHandle.h"

#include <QtGui/QCursor>
#include <QtWidgets/QGraphicsScene>

// FIXME: Magic values:
// - Z-Index (potenital conflict with ports)
// - size
// - colors

GraphicsHandle::GraphicsHandle(GraphicsHandleContainer *container, QGraphicsItem *target)
    : QGraphicsRectItem()
    , m_container(container)
    , m_target(target)
{
    setFlag(ItemIgnoresTransformations);
    setFlag(ItemSendsScenePositionChanges);
    setFlag(ItemIsMovable);
    setRect(-4, -4, 8, 8);
    setBrush(Qt::darkGreen);
    setPen(QPen(Qt::darkRed, 0.0, Qt::SolidLine));
    setZValue(200); // Above ports
    setEnabled(true);
}

GraphicsHandle::~GraphicsHandle()
{

}

void GraphicsHandle::setId(int id)
{
    m_id = id;
}

int GraphicsHandle::id() const
{
    return m_id;
}

QGraphicsItem *GraphicsHandle::target() const
{
    return m_target;
}

QVariant GraphicsHandle::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change != ItemScenePositionHasChanged)
        return value;

    if (m_container == nullptr)
        return value;

    m_container->handleScenePositionHasChanged(m_id, scenePos());
    return value;
}

GraphicsHandleContainer::GraphicsHandleContainer(QGraphicsItem *target)
    : m_target(target)
{

}

GraphicsHandleContainer::~GraphicsHandleContainer()
{
    clearHandles();
}

void GraphicsHandleContainer::addHandle(int id, Qt::CursorShape shape)
{
    auto handle = new GraphicsHandle(this, m_target);
    handle->setId(id);
    handle->setCursor(shape);
    m_handles.insert(id, handle);
}

void GraphicsHandleContainer::showHandles(bool show)
{
    if (m_target == nullptr || m_target->scene() == nullptr)
        return;

    if (show) {
        for (auto handle: m_handles.values())
            m_target->scene()->addItem(handle);
    }
    else {
        for (auto handle: m_handles.values())
            m_target->scene()->removeItem(handle);
    }
}

void GraphicsHandleContainer::moveHandle(int id, const QPointF &pos)
{
    Q_ASSERT(m_handles.contains(id));
    m_handles.value(id)->setPos(pos);
}

void GraphicsHandleContainer::moveHandle(int id, qreal x, qreal y)
{
    Q_ASSERT(m_handles.contains(id));
    m_handles.value(id)->setPos(x, y);
}

void GraphicsHandleContainer::clearHandles()
{
    qDeleteAll(m_handles.values());
    m_handles.clear();
}

void GraphicsHandleContainer::handleScenePositionHasChanged(int id, const QPointF &pos)
{
    Q_UNUSED(id);
    Q_UNUSED(pos);
}
