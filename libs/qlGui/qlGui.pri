
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../libs/qlGui/release/ -lqlGui
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../libs/qlGui/debug/ -lqlGui
else:unix: LIBS += -L$$OUT_PWD/../../libs/qlGui/ -lqlGui

INCLUDEPATH += $$PWD/../../libs
DEPENDPATH += $$PWD/../../libs/qlGui

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlGui/release/libqlGui.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlGui/debug/libqlGui.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlGui/release/qlGui.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlGui/debug/qlGui.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlGui/libqlGui.a


win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/libs/qlGui/release/ -lqlGui
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/libs/qlGui/debug/ -lqlGui
else:unix: LIBS += -L$$top_builddir/libs/qlGui/ -lqlGui

