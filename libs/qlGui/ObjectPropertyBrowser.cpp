/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ObjectPropertyBrowser.h"

#include "qlCore/Object.h"

#include "qttreepropertybrowser.h"
#include "qtvariantproperty.h"
// FIXME: Should be:
//#include "qtpropertybrowser/qttreepropertybrowser.h"
//#include "qtpropertybrowser/qtvariantproperty.h"

#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QRegularExpression>
#include <QtCore/QRegularExpressionMatch>
#include <QtGui/QPen>
#include <QtWidgets//QSplitter>
#include <QtWidgets/QVBoxLayout>

ObjectPropertyBrowser::ObjectPropertyBrowser(QWidget *parent)
    : QWidget(parent)
    , m_style(TreeStyle)
{
    m_propertyBrowser = new QtTreePropertyBrowser();
    m_propertyManager = new QtVariantPropertyManager(this);
    m_propertyEditorFactory = new QtVariantEditorFactory(this);
    m_propertyBrowser->setFactoryForManager(m_propertyManager, m_propertyEditorFactory);

    setLayout(new QVBoxLayout);
    layout()->addWidget(m_propertyBrowser);
    layout()->setMargin(0);
    layout()->setSpacing(0);
}

ObjectPropertyBrowser::~ObjectPropertyBrowser()
{

}

void ObjectPropertyBrowser::setObject(const QObject *readObject, QObject *writeObject)
{
    if (m_readObject == readObject && m_writeObject == writeObject)
        return;

    if (m_readObject != nullptr) {
        unpopulateBrowser();
        m_readObject->disconnect(this);
    }

    m_readObject = readObject;
    m_writeObject = writeObject;

    if (m_readObject != nullptr)
    {
        connect(m_readObject, &QObject::destroyed,
                this, [this]() {
            unpopulateBrowser();
            m_readObject = nullptr;
        });
        populateBrowser();
    }
}

ObjectPropertyBrowser::Style ObjectPropertyBrowser::style() const
{
    return m_style;
}

QString ObjectPropertyBrowser::baseClassName() const
{
    return m_baseClassName;
}

void ObjectPropertyBrowser::setStyle(ObjectPropertyBrowser::Style style)
{
    if (m_style == style)
        return;

    m_style = style;

    auto readObject = m_readObject;
    auto writeObject = m_writeObject;
    setObject(nullptr, nullptr);
    setObject(readObject, writeObject);

    emit styleChanged(style);
}

void ObjectPropertyBrowser::setBaseClassName(QString baseClassName)
{
    if (m_baseClassName == baseClassName)
        return;

    m_baseClassName = baseClassName;

    auto readObject = m_readObject;
    auto writeObject = m_writeObject;
    setObject(nullptr, nullptr);
    setObject(readObject, writeObject);

    emit baseClassNameChanged(baseClassName);
}

void ObjectPropertyBrowser::populateBrowser()
{
    populateBrowser(m_readObject->metaObject(), m_readObject);
    connect(m_propertyManager, &QtVariantPropertyManager::valueChanged,
            this, &ObjectPropertyBrowser::onManagerPropertyValueChanged);
}

QtProperty *ObjectPropertyBrowser::addFlagProperty(const QObject *object, QMetaProperty metaProperty)
{
    QMetaEnum metaEnum = metaProperty.enumerator();
    auto property = m_propertyManager->addProperty(QtVariantPropertyManager::flagTypeId(),
                                                   metaProperty.name());
    QStringList names;
    for (int i =0; i<metaEnum.keyCount(); i++)
    {
        QString name = metaEnum.key(i);
        names.append(labelFromPropertyName(name));
    }
    m_propertyManager->setAttribute(property, "flagNames", names);
    auto value = QVariant(QVariant::Int, metaProperty.read(object).data());
    property->setValue(value);
    addProperty(property, object, metaProperty);
    return property;
}

QtProperty *ObjectPropertyBrowser::addEnumProperty(const QObject *object, QMetaProperty metaProperty)
{
    QMetaEnum metaEnum = metaProperty.enumerator();
    auto property = m_propertyManager->addProperty(QtVariantPropertyManager::enumTypeId(),
                                                   metaProperty.name());
    QStringList names;
    for (int i =0; i<metaEnum.keyCount(); i++)
    {
        QString name = metaEnum.key(i);
        names.append(labelFromPropertyName(name));
    }
    m_propertyManager->setAttribute(property, "enumNames", names);
    bool ok = false;
    auto value = metaProperty.read(object).toInt(&ok);
    property->setValue(value);
    addProperty(property, object, metaProperty);
    return property;
}

QtProperty *ObjectPropertyBrowser::addPenProperty(const QObject *object, QMetaProperty metaProperty)
{
    auto value = metaProperty.read(object).value<QPen>();
    auto groupProperty = m_propertyManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                                        metaProperty.name());
    {
        auto property = m_propertyManager->addProperty(QVariant::Double, "Width");
        property->setValue(value.widthF());
        groupProperty->addSubProperty(property);
    }
    {
        auto property = m_propertyManager->addProperty(QtVariantPropertyManager::enumTypeId(),
                                                       "Style");
        m_propertyManager->setAttribute(property, "enumNames", QStringList() << "Solid" << "Dash" << "Dot" << "Dash-dot" << "Dash-dot-dot");
        property->setValue(int(value.style()));
        groupProperty->addSubProperty(property);
    }
    {
        auto property = m_propertyManager->addProperty(QVariant::Color, "Color");
        property->setValue(value.color());
        groupProperty->addSubProperty(property);
    }
    addProperty(groupProperty, object, metaProperty);
    return groupProperty;
}

QtProperty *ObjectPropertyBrowser::addBrushProperty(const QObject *object, QMetaProperty metaProperty)
{
    auto groupProperty = m_propertyManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                                        metaProperty.name());

    //groupProperty->addSubProperty(property);
    addProperty(groupProperty, object, metaProperty);
    return groupProperty;
}

QtProperty *ObjectPropertyBrowser::addObjectUidProperty(const QObject *object, QMetaProperty metaProperty)
{
    auto property = m_propertyManager->addProperty(QVariant::String,
                                                   metaProperty.name());
    auto value = QString::number(metaProperty.read(object).value<ObjectUid>().value());
    property->setValue(value);
    addProperty(property, object, metaProperty);
    return property;
}

QtProperty *ObjectPropertyBrowser::addUserTypeProperty(const QObject *object, QMetaProperty metaProperty)
{
    const int userType = metaProperty.userType();
    if (userType == qMetaTypeId<ObjectUid>())
        return addObjectUidProperty(object, metaProperty);
#if 0
    const auto metaType = QMetaType(userType);
    if (metaType.flags() & QMetaType::IsEnumeration) {
        QMetaEnum metaEnum = QMetaEnum();
        auto property = m_propertyManager->addProperty(QtVariantPropertyManager::enumTypeId(),
                                                       metaProperty.name());
        QStringList names;
        for (int i =0; i<metaEnum.keyCount(); i++)
        {
            QString name = metaEnum.key(i);
            names.append(labelFromPropertyName(name));
        }
        m_propertyManager->setAttribute(property, "enumNames", names);
        bool ok = false;
        auto value = metaProperty.read(object).toInt(&ok);
        property->setValue(value);
        addProperty(property, object, metaProperty);
        return property;
    }
#endif
    return nullptr;
}

QtProperty *ObjectPropertyBrowser::addEnumProperty(const QObject *object, QMetaType metaType)
{
    Q_UNUSED(object);
    Q_UNUSED(metaType);
    return nullptr;
}

QtProperty *ObjectPropertyBrowser::addSimpleProperty(const QObject *object, QMetaProperty metaProperty)
{
    auto property = m_propertyManager->addProperty(metaProperty.type(),
                                                   metaProperty.name());
    auto value = metaProperty.read(object);
    property->setValue(value);
    addProperty(property, object, metaProperty);
    return property;
}

void ObjectPropertyBrowser::addProperty(QtProperty *property,
                                        const QObject *object,
                                        QMetaProperty metaProperty)
{
    property->setPropertyName(labelFromPropertyName(property->propertyName()));
    property->setEnabled(metaProperty.isWritable());
    m_managerPropertyToObjectPropertyName.insert(property, metaProperty.name());

    QMetaMethod notifyMethod = metaProperty.notifySignal();
    m_objectSignalIndexToManagerProperty.insert(notifyMethod.methodIndex(), property);
    connect(object, "2" + notifyMethod.methodSignature(),
            this, SLOT(onObjectPropertyValueChanged()));
}

void ObjectPropertyBrowser::populateBrowser(const QMetaObject *metaObject, const QObject *object)
{
    if (metaObject->superClass() != nullptr && metaObject->className() != m_baseClassName)
        populateBrowser(metaObject->superClass(), object);

    QList<QtProperty*> properties;
    for (int metaPropertyIndex = metaObject->propertyOffset();
         metaPropertyIndex < metaObject->propertyCount();
         metaPropertyIndex++) {
        QMetaProperty metaProperty = metaObject->property(metaPropertyIndex);

        if (!metaProperty.isDesignable() || !metaProperty.isReadable())
            continue;

        if (metaProperty.isFlagType())
            properties << addFlagProperty(object, metaProperty);
        else if (metaProperty.isEnumType())
            properties << addEnumProperty(object, metaProperty);
        else if (m_propertyManager->isPropertyTypeSupported(metaProperty.type()))
            properties << addSimpleProperty(object, metaProperty);
        else if (metaProperty.type() == QVariant::Pen)
            properties << addPenProperty(object, metaProperty);
        else if (metaProperty.type() == QVariant::Brush)
            properties << addBrushProperty(object, metaProperty);
        else if (metaProperty.type() == QVariant::UserType) {
            auto property = addUserTypeProperty(object, metaProperty);
            if (property != nullptr)
                properties << property;
            else
                qWarning() << "Property user type not supported" << metaProperty.userType();
        }
        else
            qWarning() << "Property type id not supported" << metaProperty.type();
    }

    if (properties.isEmpty())
        return;

    if (m_style == TreeStyle) {
        // Add all properties as sub properties of "ClassName" node
        // "ClassName" node is expanded, subproperties are collapsed
        auto group = m_propertyManager->addProperty(QtVariantPropertyManager::groupTypeId(), metaObject->className());
        for (auto property: properties)
            group->addSubProperty(property);
        auto item = m_propertyBrowser->addProperty(group);
        m_propertyBrowser->setExpanded(item, true);
        for (auto subItem: item->children())
            m_propertyBrowser->setExpanded(subItem, false);
    }
    else { // ListStyle
        for (auto property: properties) {
            auto item = m_propertyBrowser->addProperty(property);
            m_propertyBrowser->setExpanded(item, true);
            for (auto subItem: item->children())
                m_propertyBrowser->setExpanded(subItem, false);
        }
    }
}

void ObjectPropertyBrowser::unpopulateBrowser()
{
    m_propertyManager->disconnect(this);
    if (m_readObject != nullptr)
        m_readObject->disconnect(this);
    m_propertyManager->clear();
    m_managerPropertyToObjectPropertyName.clear();
    m_objectSignalIndexToManagerProperty.clear();
}

void ObjectPropertyBrowser::onObjectPropertyValueChanged()
{
    int signalIndex = senderSignalIndex();
    if (!m_objectSignalIndexToManagerProperty.contains(signalIndex))
    {
        qWarning() << "ObjectPropertyBrowser::onObjectPropertyValueChanged(): Received an unkown property notification";
        return;
    }
    QtProperty *managerProperty = m_objectSignalIndexToManagerProperty[signalIndex];
    if (!m_managerPropertyToObjectPropertyName.contains(managerProperty))
    {
        qWarning() << "ObjectPropertyBrowser::onObjectPropertyValueChanged(): Received an unkown property notification";
        return;
    }
    const char *objectPropertyName = m_managerPropertyToObjectPropertyName[managerProperty];
    m_propertyManager->setValue(managerProperty, m_readObject->property(objectPropertyName));
}

// FIXME: When an item "visible" is set to off, the document outline deselect the object,
// and by the time we receive the property notification, the object is gone
// FIXME: This connection should normally be gone, when setObject() is called
void ObjectPropertyBrowser::onManagerPropertyValueChanged(QtProperty *property, const QVariant &value)
{
    if (!m_managerPropertyToObjectPropertyName.contains(property))
    {
        // Can be a sub property, eg. size property creates 2 sub properties "widht" and "height"
        // FIXME: Can actually be sub-sub-... property
        for (const QtProperty *superProperty: m_managerPropertyToObjectPropertyName.keys())
            if (superProperty->subProperties().contains(property))
                return;
        qWarning() << "ObjectPropertyBrowser::onManagerPropertyValueChanged(): Received an unkown property notification: " << property->propertyName();
        return;
    }
    const char *objectPropertyName = m_managerPropertyToObjectPropertyName[property];
    m_writeObject->setProperty(objectPropertyName, value);
    if (m_readObject->property(objectPropertyName) != value) {
        qWarning() << "ObjectPropertyBrowser::onManagerPropertyValueChanged: read/write mismatch:";
        qWarning() << "Objects:" << m_readObject << m_writeObject;
        qWarning() << "Property" << objectPropertyName << ":" <<
                      m_readObject->property(objectPropertyName) << "vs" << value;
    }
}

QString ObjectPropertyBrowser::labelFromPropertyName(const QString &name)
{
    QRegularExpression regexp("^([A-Z]?[A-Za-z0-9]+)(.*)$");
    Q_ASSERT(regexp.isValid());
    QStringList words;
    auto reMatch = regexp.match(name);
    auto word = reMatch.captured(1);
    word.replace(0, 1, word.at(0).toUpper());
    words.append(word);

    regexp.setPattern("([A-Z][A-Za-z0-9]*)");
    auto reIter = regexp.globalMatch(reMatch.captured(2));
    while (reIter.hasNext())
    {
        word = reIter.next().captured(1);
        word.replace(0, 1, word.at(0).toLower());
        words.append(word);
    }

    return words.join(' ');
}


void ObjectPropertyBrowser::focusInEvent(QFocusEvent *event)
{
    Q_UNUSED(event)

    if (m_propertyBrowser->topLevelItems().isEmpty())
        return;
    QtBrowserItem *item = m_propertyBrowser->topLevelItems().first();
    while (!item->children().isEmpty())
        item = item->children().first();
    m_propertyBrowser->setCurrentItem(item);
    m_propertyBrowser->editItem(item);
}
