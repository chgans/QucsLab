/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QWidget>
#include <QtCore/QMap>


class QtTreePropertyBrowser;
class QtVariantPropertyManager;
class QtVariantEditorFactory;
class QtProperty;

// Allow to subclass and customise setting object's prop and notifying of object's property changes
// setObjectProperty(const QObject *object, const QString &propertyName, const QVariant &propertyValue)
// plus allow to filter properties and custom naming (eg skip base classes, ...)

class ObjectPropertyBrowser : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(Style style READ style WRITE setStyle NOTIFY styleChanged)
    Q_PROPERTY(QString baseClassName READ baseClassName WRITE setBaseClassName NOTIFY baseClassNameChanged)

public:
    enum Style {
        TreeStyle = 0,
        ListStyle
    };
    Q_ENUM(Style)

    explicit ObjectPropertyBrowser(QWidget *parent = 0);
    ~ObjectPropertyBrowser();

    void setObject(const QObject *readObject, QObject *writeObject);    

    Style style() const;
    QString baseClassName() const;

public slots:
    void setStyle(Style style);
    void setBaseClassName(QString baseClassName);

signals:
    void styleChanged(Style style);
    void baseClassNameChanged(QString baseClassName);

private:
    void populateBrowser();
    void populateBrowser(const QMetaObject *metaObject, const QObject *object);
    void unpopulateBrowser();
    QtProperty *addFlagProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addEnumProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addSimpleProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addPenProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addBrushProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addObjectUidProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addUserTypeProperty(const QObject *object, QMetaProperty metaProperty);
    QtProperty *addEnumProperty(const QObject *object, QMetaType metaType);
    void addProperty(QtProperty *property, const QObject *object, QMetaProperty metaProperty);
    QString labelFromPropertyName(const QString &name);

private slots:
    void onObjectPropertyValueChanged();
    void onManagerPropertyValueChanged(QtProperty *property, const QVariant &value);

private:
    QtTreePropertyBrowser *m_propertyBrowser = nullptr;
    QtVariantPropertyManager *m_propertyManager = nullptr;
    QtVariantEditorFactory *m_propertyEditorFactory = nullptr;
    QMap<int, QtProperty*> m_objectSignalIndexToManagerProperty;
    QMap<QtProperty*, const char *> m_managerPropertyToObjectPropertyName;
    const QObject *m_readObject = nullptr;
    QObject *m_writeObject = nullptr;
    Style m_style;
    QString m_baseClassName;

    // QWidget interface
protected:
    virtual void focusInEvent(QFocusEvent *event) override;
};
