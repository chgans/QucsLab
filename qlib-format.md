# XMD for QucsLab

## Document is RootObject
- name: document
- space=https://www.some.where/qucs-xml/1.0
- @version: String
- library: Library

## Library is Object
- identification: Identification
- attribution: Attribution
- documentation: Documentation
- interface: Interface
- implementations: list of Implementation
- representations: list of Representation

## Identification is Object
- @uid: String
- @caption: String
- description: String

## Attibution is Object
- statement: String
- license: String

## Documentation is Object
- text: text

## Interface is Object
- ports: list of Port
- parameters: list of Parameter

## Implementation is Object
- @caption: String
- description: String
- @language: Language
- code: Text

## Representation is Object
- @caption: String
- description: String
- portmap: Portmap
- graphics: list of GraphicsObject

## Parameter is Object
- @id: sequenceNumer
- @caption: String
- description: String
- @type: DataType
- @length?: Size=1
- value: string
- @required: Bool
- restriction: DataRestriction
- documentation: text

## Port is Object
- @id: sequenceNumer
- @caption: String
- description: String
- @type: PortType
- @size?: PortSize=1

## GraphicsObject is Object
- @x?: Real=0.0
- @y?: Real=0.0
- @rotation?: Real=0.0
- @mirrored?: Bool=false
- @locked?: Bool=false

## GraphicsLine is GraphicsObject
- @x1: Real
- @y1: Real
- @x2: Real
- @y2: Real
- stroke: Stroke

## GraphicsEllipse is GraphicsObject
- @width: Real
- @height: Real
- @start?: Real=0.0
- @span?: Real=360.0
- @style: EllipseStyle
- stroke: Stroke
- fill: Fill

## GraphicsRectangle is GraphicsObject
- @width: Real
- @height: Real
- stroke: Stroke
- fill: Fill

## GraphicsLabel is GraphicsObject
- text: String
- size: Real
- color?: Color="#000000"

## Stroke is Object
- @width?: StrokeWidth=MediumStroke
- @style?: StrokeStyle=SolidStroke
- @color?: Color="#000000"

## Fill is Object
- @style?: FillStyle=NoFill
- @color?: Color="#000000"

## EllipseStyle is Enum:
- arc means EllipsoidalArc
- chord means EllipsoidalChord
- pie means EllipsoidalPie
- full means FullEllipsoid

## StrokeWidth is Enum
- smallest means SmallestStroke
- small means SmallStroke
- medium means MediumStroke
- large means LargeStroke
- largest means LargestStroke

## StrokeStyle is Enum
- solid means SolidStroke
- dash means DashStroke
- dot means DotStroke
- dashdot mean DashDotStroke
- dashdotdot means DashDotDotStroke
- none means NoStroke

## FillStyle is Enum
- solid means SolidFill
- hor means HorLineFill
- ver means VerLineFill
- fw means FwLineFill
- bw means BwLineFill
- horver means HorVerLineFill
- fwbw means FwBwLineFill
- none means NoFill

## DataType is Enum
- bool means BooleanData
- int means IntegerData
- real means RealData
- string means StringData
- text means TextData
- choice means ChoiceData
- refdes means RefDesData
- url means UrlData

## Language is Enum
- spice means SpiceLang
- qucs means QucsLang
- vhdl means VhdlLang
- verilog means VerilogLang
- veriloga means VerilogaLang

