[![build status](https://gitlab.com/chgans/QucsLab/badges/master/build.svg)](https://gitlab.com/chgans/QucsLab/commits/master)

# Qucs Lab

In the short term, Qucs Lab aims to be a playground for providing "Library management" to [Qucs](https://github.com/Qucs/)
In the long term, this project could potentially replace the current Qucs-GUI, or not! :)

## Building

```
$ git clone https://gitlab.com/chgans/QucsLab.git
$ cd QucsLab
$ qmake
$ make -j10
$ ./apps/symbol-editor/symbol-editor

```

## User manual

Please checkout the [User's Manual](docs/UserManual.md).

This is a work in progress.

## Issues and Milestones

Please refer to https://gitlab.com/chgans/QucsLab/issues

## CI

TBD:
- Need to find and setup a windows runner
- Need to cleanup/upgrade current Linux and MacOSX runners
- Add gitlab CI meta file to the repository
- Add installers and snapshot upload

PS: See work done on LibreEDA's git [repo](https://gitlab.com/chgans/LibreEDA) and [server](https://download.libre-eda.org/) the same infrastructure could be used for QucsLab


Currently *test building* on Linux only, using Qt-5.7.

## Contributing

If you fancy have fun, here are few ideas of what could be added to the project in a independent way (like we won't walk on each other toes):
 - Lightweight [MarkDown editor view preview](https://doc.qt.io/qt-5/qtwebengine-webenginewidgets-markdowneditor-example.html)
 - Chart plotter, using [Qwt](http://qwt.sourceforge.net/), [QtCharts](http://doc.qt.io/qt-5/qtcharts-examples.html) or anything else you want
 - Port the Symbol editor to QtCreator, using [plugins](https://doc-snapshots.qt.io/qtcreator-extending/creating-plugins.html) (not kidding, it's not as hard as it sounds)
 - Gitlab CI setup
