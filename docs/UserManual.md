# QucsLab Symbol Editor User's Manual


![UI overview][ui_overview]

## The UI at a glance

- Left: The filesystem navigation panel, allows to quickly open symbols file. Can be very handy as QucsLab doesn't provide (yet) support for library and/or project.
- Center: the graphical document view
- Right: The tool option panel. This panel is context sensitive and the contents depends on the active tool.

This is the basic setup as of version 0.3.

The `Window` menu allows to hide/show more dock widgets and toobars.

## The graphics view

> :warning: TODO: Grid, sheet, and origin


## Navigating

### Zoom

To zoom in and out, simply point the mouse cursor to the spot of interest, and hold `CTRL` while rolling the mouse wheel up and down.

Some shortcuts are available, eg:
- Zoom to drawing: will fit the sheet within the view port
- Zoom to selection: will fit the selected item(s) within the view port
- Zoom to fit: will fit all drawing items within the view port

## Editing tools

### Select

This is the default tool and its shortcut is thus `Esc`.

The basic usage is:
- Click to select, hold shift or control to add/remove from selection.
- Drag from an empty area to start rectangular selection.
- Drag item's handles to interactively resize/reshape them.

The option panel provides:
- A quick action bar allowing to manipulate the current selection, currently provides only Z-orser related actions (Bring to front, raise, lower and send to back)
- A document outline view, allowing to navigate through all the items that make up the symbol, the selection is synchrnosied with the graphical view.
- An item property browser allowing to easily modify any property of the currently selected drawing item.

Tips:
- At any moment, you can use the usual `Select all`, `Clear Selection` and `Invert Selection`. These actions can be activated using `CTRL-A`, `CTRL-SHIFT-A` and `CTRL-I`.
- When another editing tool is active, it can be cancelled with `Esc` which will bring you back to the default `Select` mode.

> :construction: Eventually, the quick action bar will features the usual align and distribute actions as well as groupping/ungroupping of items..

> :construction: Eventually, the property broser will support multiple object property edition.


### Cut/Copy/Paste

Usual cut/copy/paste operations are available, you can cut/copy/paste to/from the within or between different symbol documents, in the same or between separate application windows.

> :construction: At some point copy/pasting will be allowed to/from XML text from eg, your preferred text editor

### Move

Activated with `M`, the move tools behaves differently depending on the mouse cursor position:
- If the selection is not empty, start moving it without requiring to click
- Otherwise, if there is an item under the cursor, start moving it without requiring to click
- Otherwise, first click to pick the item you want move (the mouse cursor will change to indicate pickable items)

Then simply move and click to drop the item(s).

### Clone

Activated with `C`, this tools behaves exactly like the move tools, except of course that it doesn't move the original items, but instead clone them before.

### Delete

Activated with the `Delete` key, the delete tool currently deletes only the current selection.

> :construction: This tool will be modified to behave like the `Clone` and `Move` tools for consistency.


## Drawing tools

### Place line

Activated with `L` this tools allows to place lines.

*Option panel*: The `Place line` option panel allows you to change the stroke style.

*Operation*: To place a line, simply click to insert the first point, click again for the second point.

### Ellipsoids

Activated with `E` this tools allows to place ellipsoids, pies, chords and arcs.

*Option panel*: The option panel allows you to change the stroke and fill style, and the ellipsoid style (Full, Pie, Chord or Arc)

*Operation*:

> :warning: TODO: How it works

### Rectangles

Activated with `R` this tools allows to place rectangles.

*Option panel*: The option panel allows you to change the stroke and fill style.

*Operation*: To place a rectangle, simply click to select to rectangle's center, click again to set the width/height.

### Text labels

Activated with `T` this tools allows to place text labels.

*Option panel*: The otion panel allows you to change the font size and the font color.

*Operation*: Click to place.

> :construction: Text labels are a work in progress, eventually editing the text will become more user friendly.

### Ports

Activated with `P` this tools allows to place electrical ports.

*Option panel*: The otion panel allows you to change the port sequence number ahead of time, and has an `auto increment` option.

*Operation*: Click to place, if auto-increment is enabled, any further click will insert a port with an ever incrementing sequence number.


# Experimental import and export of Qucs native symbol files

> :warning: TODO: Give some details


[ui_overview]:images/Screenshot_20170512_195826.png
