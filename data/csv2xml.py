#!/usr/bin/env python3

import sys
from xml.sax.saxutils import escape

# 1 Qucs drawing unit is 0.205882352941 mm
#  - a A4 Frame was measured to be 1020 tall (you can fit 17 resistors vertically)
#  - a resistor is 60 long
#
# Ports are aligned on a grid of size 10
# Resistor, capacitor and inductors are all 60 long (12.35mm)
# A resistor pin is 18 long (3.71mm)
# A capacitor pin is 26 long (5.35mm)
# A inductor pin is 18 long (3.71mm)
#
# We round 1 Qucs drawing unit to 0.2mm, which gives a 12 mm long resistor
# => Electric gric will then be 2mm
#

def convert(number):
    return float(number)/5

def convertTextSize(number):
    return float(number)*2

# Qucs pen width seems to be pixels...
def convertPenWidth(number):
    pw = convert(number)/2.5
    if pw <= 0.1:
        return "smallest"
    if pw <= 0.25:
        return "small"
    if pw <= 0.5:
        return "medium"
    if pw <= 1.0:
        return "large"
    return "largest"


class Item:
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.rotation = 0.0
        self.opacity = 0.0
        self.xMirrored = False
        self.yMirrored = False

    def toXml(self):
        print('x="{:e}" y="{:e}"'.format(self.x, self.y))
        # print('<z-value>{:e}</z-value>'.format(self.z))
        # print('<rotation>{:e}</rotation>'.format(self.rotation))
        # print('<opacity>{:e}</opacity>'.format(self.opacity))
        # print('<x-mirrored>{:s}</x-mirrored>'.format("true" if self.xMirrored else "false"))
        # print('<y-mirrored>{:s}</y-mirrored>'.format("true" if self.yMirrored else "false"))
        
class Shape(Item):
    def __init__(self):
        Item.__init__(self)
        self.pen = {}
        self.pen["width"] = "medium"
        self.pen["color"] = "#000000"
        self.pen["style"] = "solid"
        self.brush = {}
        self.brush["style"] = "0"
        self.brush["color"] = "#000000"
        self.brushStyles = { "0": "none", "1": "solid" }

    def brushStyle(self):
        return self.brushStyles[self.brush["style"]]

class Ellipse(Shape):
    def __init__(self):
        Shape.__init__(self)
        self.xCenter = 0.0
        self.yCenter = 0.0
        self.xRadius = 0.0
        self.yRadius = 0.0
        self.startAngle = 0.0
        self.spanAngle = 360.0

    def toXml(self):
        print("<ellipse")
        print(' x="{:e}" y="{:e}"'.format(self.xCenter, self.yCenter))
        print(' width="{:e}" height="{:e}"'.format(2*self.xRadius, 2*self.yRadius))
        print(' startAngle="{:e}" spanAngle="{:e}"'.format(self.startAngle, self.spanAngle))
        print(' style="full"')
        print(">")
        print(' <stroke width="{:s}" color="{:s}"/>'.format(self.pen["width"], self.pen["color"]))
        print(' <fill color="{:s}" style="{:s}"/>'.format(self.brush["color"], self.brushStyle()))
        print("</ellipse>")

    # ellips;x;y;width;height;pen.width;pen.color;pen.style;brush.color;brush.style
    def fromCsv(self, tokens):
        _,x,y,w,h,pw,pc,ps,bc,bs = tokens
        self.xCenter = convert(x) + convert(w)/2.0
        self.yCenter = convert(y) + convert(h)/2.0
        self.xRadius = convert(w)/2.0
        self.yRadius = convert(h)/2.0
        self.pen["width"] = convertPenWidth(pw)
        self.pen["color"] = pc
        self.pen["style"] = ps
        self.brush["color"] = bc
        self.brush["style"] = bs

class Rect(Shape):
    def __init__(self):
        Shape.__init__(self)
        self.xTopLeft = 0.0
        self.yTopLeft = 0.0
        self.xBottomRight = 0.0
        self.yBottomRight = 0.0
        
    def toXml(self):
        print("<rectangle")
        print(' tlx="{:e}" tly="{:e}"'.format(self.xTopLeft, self.yTopLeft))
        print(' brx="{:e}" bry="{:e}"'.format(self.xBottomRight, self.yBottomRight))
        print(">")
        print(' <stroke width="{:s}" color="{:s}"/>'.format(self.pen["width"], self.pen["color"]))
        print(' <fill color="{:s}" style="{:s}"/>'.format(self.brush["color"], self.brushStyle()))
        print("</rectangle>")

    # rect;x;y;width;height;pen.width;pen.color;pen.style;brush.color;brush.style
    def fromCsv(self, tokens):
        _,x,y,w,h,pw,pc,ps,bc,bs = tokens
        self.x = convert(x)
        self.y = convert(y)
        self.xTopLeft = 0
        self.yTopLeft = 0
        self.xBottomRight = convert(w)
        self.yBottomRight = convert(h)
        self.pen["width"] = convertPenWidth(pw)
        self.pen["color"] = pc
        self.pen["style"] = ps
        self.brush["color"] = bc
        self.brush["style"] = bs

class Line(Item):
    def __init__(self):
        Item.__init__(self)
        self.x1 = 0.0
        self.y1 = 0.0
        self.x2 = 0.0
        self.y2 = 0.0
        self.pen = {}
        self.pen["width"] = 0.0
        self.pen["color"] = "#000000"
        self.pen["style"] = "SolidLine"

    def toXml(self):
        print("<line")
        print(' x1="{:e}" y1="{:e}"'.format(self.x1, self.y1))
        print(' x2="{:e}" y2="{:e}"'.format(self.x2, self.y2))
        print(">")
        print(' <stroke width="{:s}" color="{:s}"/>'.format(self.pen["width"], self.pen["color"]))
        print("</line>")

    # line;x1;y1;x2;y2;pen.width;pen.color;pen.style
    def fromCsv(self, tokens):
        self.x1 = convert(tokens[1])
        self.y1 = convert(tokens[2])
        self.x2 = convert(tokens[3])
        self.y2 = convert(tokens[4])
        self.pen["width"] = convertPenWidth(tokens[5])
        self.pen["color"] = tokens[6]
        self.pen["style"] = tokens[7]
        
class Arc(Shape):
    def __init__(self):
        Shape.__init__(self)
        self.xCenter = 0.0
        self.yCenter = 0.0
        self.xRadius = 0.0
        self.yRadius = 0.0
        self.startAngle = 0.0
        self.spanAngle = 360.0

    def toXml(self):
        print("<ellipse")
        print(' x="{:e}" y="{:e}"'.format(self.xCenter, self.yCenter))
        print(' width="{:e}" height="{:e}"'.format(2*self.xRadius, 2*self.yRadius))
        print(' startAngle="{:e}" spanAngle="{:e}"'.format(self.startAngle, self.spanAngle))
        print(' style="arc"')
        print(">")
        print(' <stroke width="{:s}" color="{:s}"/>'.format(self.pen["width"], self.pen["color"]))
        print("</ellipse>")

    # arc;x;y;width;height;angle;arclen;pen.width;pen.color;pen.style
    def fromCsv(self, tokens):
        _,x,y,w,h,sta,spa,pw,pc,ps = tokens
        self.xCenter = convert(x) + convert(w)/2.0
        self.yCenter = convert(y) + convert(h)/2.0
        self.xRadius = convert(w)/2.0
        self.yRadius = convert(h)/2.0
        self.startAngle = int(sta)/16.0
        self.spanAngle = int(spa)/16.0
        self.pen["width"] = convertPenWidth(pw)
        self.pen["color"] = pc
        self.pen["style"] = ps
        
class Port(Item):
    def __init__(self):
        Item.__init__(self)
        self.name = ""
        self.description=""

    def toXml(self):
        print('<port')
        print(' x="{:e}" y="{:e}"'.format(self.x, self.y))
        print(' name="{:s}"'.format(escape(self.name)))
        print(">")
        print("</port>")
        
    # port;seq;x;y;avail;type
    def fromCsv(self, tokens):
        self.name = tokens[1]
        self.x = convert(tokens[2])
        self.y = convert(tokens[3])

class Text(Shape):
    def __init__(self):
        Shape.__init__(self)
        self.content = ""
        
    def toXml(self):
        print('<label')
        print(' x="{:e}" y="{:e}"'.format(self.x, self.y))
        print(' text="{:s}"'.format(escape(self.content)));
        print(' size="{:e}" color="{:s}"'.format(self.size, self.pen["color"]))
        print(">")
        # size, color
        print("</label>")
        
    # text;x;y;content;color;size;sin;cos;over;under
    def fromCsv(self, tokens):
        self.x = convert(tokens[1])
        self.y = convert(tokens[2])
        self.content = tokens[3]
        self.size = convertTextSize(tokens[5])
        self.brush["style"] = "solid"
        self.brush["color"] = tokens[4]
        #if tokens[6] != "0" and tokens[7] != "1":
        #    raise Exception("Orientation not supported")
        if tokens[8] != "0" and tokens[9] != "0":
            raise Exception("Text under/over line not supported")
    
# component;flags;cx;cy;tx;ty;x1;y1;x2;y2
class Symbol():
    def __init__(self, caption, description):
        self.caption = caption
        self.description = description
        self.items = []
        
    def toXml(self):
        print('<symbol xmlns="http://www.leda.org/xdl" version="1.0">')
        print(' <caption>{:s}</caption>'.format(self.caption))
        print(' <description>{:s}</description>'.format(self.description));
        for item in self.items:
            item.toXml()
        print("</symbol>")        
                      
    def fromCsv(self, lines):
        for line in lines:
            line = line.strip()
            item = None
            if line.startswith('#'):
                continue
            elif line.startswith('component;'):
                continue
            elif line.startswith('line;'):
                item = Line()
            elif line.startswith('arc;'):
                item = Arc()
            elif line.startswith('rect;'):
                item = Rect()
            elif line.startswith('ellips;'):
                item = Ellipse()
            elif line.startswith('port;'):
                item = Port()
            elif line.startswith('text;'):
                item = Text()
            else:
                raise Exception('Unsupported primitive: {:s}'.format(l))
            item.fromCsv(line.split(';'))
            self.items.append(item)
                      
f = open(sys.argv[1])
lines = f.readlines()
caption = lines[1].split(";")[1].strip()
description = lines[2].split(";")[1].strip()

f = open(sys.argv[2])
lines = f.readlines()

sym = Symbol(caption, description)
sym.fromCsv(lines)
sym.toXml()
sys.exit(0)
