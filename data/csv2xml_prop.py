#!/usr/bin/env python3

import sys
from xml.sax.saxutils import escape

class Generic:
    def __init__(self):
        self.name=""
        self.unit=""
        self.dataType="string"
        self.defaultValue=""
        self.displayed = ""

    # id;value;displayed;description
    def fromCsv(self, tokens):
        name, value, displayed, description = tokens
        self.name = name.strip()
        self.defaultValue = value.replace('"', '').strip()
        self.displayed = displayed.replace('"', '').strip()
        self.description = description.replace('"', '').strip()

    def toXml(self):
        print('<parameter name="{:s}">'.format(self.name))
        if len(self.unit) != 0:
            print(' unit="{:s}"'.format(self.unit))
        print(' display="{:s}"'.format(self.displayed))
        print(' type="{:s}"'.format(self.dataType))
        print('>')
        if len(self.defaultValue) != 0:
            print(' <default>{:s}</default>'.format(self.defaultValue))
        print(' <description>{:s}</description>'.format(self.description))
        print('</parameter>')

class Port:
    def __init__(self, name):
        self.name=name
        self.mode="inout"
        self.signalType="real"
        self.initialValue=""

    def toXml(self):
        print('<port id="{:s}">'.format(self.name))
        print('</port>')

class Entity:
    def __init__(self, caption, identifier):
        self.name = identifier;
        self.generics=[]
        self.ports=[]

    def toXml(self):
        print('<entity id="{:s}">'.format(self.name))
        for generic in self.generics:
            generic.toXml()
        for port in self.ports:
            port.toXml()
        print("</entity>")

    # text;x;y;content;color;size;sin;cos;over;under
    def fromCsv(self, tokens):
        pass

f = open(sys.argv[1])
lines = f.readlines()
caption = lines[1].split(";")[1].strip()
description = lines[2].split(";")[1].strip()
identifier = lines[3].split(";")[1].strip().replace("`", "")
nbProps = int(lines[7].split(";")[1].strip())
#nbPorts = int(lines[9].split(";")[1].strip())

f = open(sys.argv[2])
lines = f.readlines()

#entity = Entity(caption, identifier)
#for i in range(nbPorts):
#    entity.ports.append(Port(str(i)))

for line in lines:
    if line[0] == "#":
        continue
    prop = Generic()
    prop.fromCsv(line.strip().split(";"))
    prop.toXml()

#entity.toXml()

sys.exit(0)
