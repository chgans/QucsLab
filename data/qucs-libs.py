#!/usr/bin/env python3

import sys, os, math
from enum import Enum
from xml.sax.saxutils import escape

# 1 Qucs drawing unit is 0.205882352941 mm
#  - a A4 Frame was measured to be 1020 tall (you can fit 17 resistors vertically)
#  - a resistor is 60 long
#
# Ports are aligned on a grid of size 10
# Resistor, capacitor and inductors are all 60 long (12.35mm)
# A resistor pin is 18 long (3.71mm)
# A capacitor pin is 26 long (5.35mm)
# A inductor pin is 18 long (3.71mm)
#
# We round 1 Qucs drawing unit to 0.2mm, which gives a 12 mm long resistor
# => Electric gric will then be 2mm
#

def convert(number):
    return float(number)/5

def convertTextSize(number):
    return float(number)*2

# Qucs pen width seems to be pixels...
def convertPenWidth(number):
    pw = convert(number)/2.5
    if pw <= 0.1:
        return "smallest"
    if pw <= 0.25:
        return "small"
    if pw <= 0.5:
        return "medium"
    if pw <= 1.0:
        return "large"
    return "largest"

def fromQucsLibPen(pc, pw, ps):
    pen = {}
    penStyles = { "0": "none", "1": "solid", "2": "dash", "3": "dot", "4": "dashdot", "5": "dashdotdot" }
    pen["width"] = convertPenWidth(pw)
    pen["color"] = pc
    pen["style"] = penStyles[ps]
    return pen

def fromQucsLibBrush(bc, bs, bf):
    brush = {}
    brushStyles = { "0": "none", "1": "solid" }
    brush["style"] = brushStyles[bs]
    brush["color"] = bc
    return brush

class Item:
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.rotation = 0.0
        self.opacity = 0.0
        self.xMirrored = False
        self.yMirrored = False
        self.pen = {}
        self.pen["width"] = "medium"
        self.pen["color"] = "#000000"
        self.pen["style"] = "solid"
        self.brush = {}
        self.brush["style"] = "0"
        self.brush["color"] = "#000000"

class Ellipse(Item):
    def __init__(self):
        Item.__init__(self)
        self.xCenter = 0.0
        self.yCenter = 0.0
        self.xRadius = 0.0
        self.yRadius = 0.0
        self.startAngle = 0.0
        self.spanAngle = 360.0

    def toXml(self):
        return "\n".join([
            '<ellipse x="{:g}" y="{:g}" width="{:g}" height="{:g}" startAngle="{:g}" spanAngle="{:g}" style="full">'.format(self.xCenter, self.yCenter, 2*self.xRadius, 2*self.yRadius, self.startAngle, self.spanAngle),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            ' <fill color="{:s}" style="{:s}"/>'.format(self.brush["color"], self.brush["style"]),
            '</ellipse>'])

    def fromQucsLib(self, line):
        _,x,y,w,h,pc,pw,ps,bc,bs,bf = line.strip()[1:-1].split()
        self.xCenter = convert(x) + convert(w)/2.0
        self.yCenter = convert(y) + convert(h)/2.0
        self.xRadius = convert(w)/2.0
        self.yRadius = convert(h)/2.0
        self.pen = fromQucsLibPen(pc, pw, ps)
        self.brush = fromQucsLibBrush(bc, bs, bf)

class Rect(Item):
    def __init__(self):
        Item.__init__(self)
        self.width = 0.0
        self.height = 0.0
        
    def toXml(self):
        return "\n".join([
            '<rectangle x="{:g}" y="{:g}" width="{:g}" height="{:g}">'.format(self.x, self.y, self.width, self.height),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            ' <fill color="{:s}" style="{:s}"/>'.format(self.brush["color"], self.brush["style"]),
            '</rectangle>'])

    def fromQucsLib(self, line):
        _,x,y,w,h,pc,pw,ps,bc,bs,bf = line.strip()[1:-1].split()
        self.width = convert(w)
        self.height = convert(h)
        self.x = convert(x) + self.width/2.0
        self.y = convert(y) + self.height/2.0
        self.pen = fromQucsLibPen(pc, pw, ps)
        self.brush = fromQucsLibBrush(bc, bs, bf)


class Line(Item):
    def __init__(self):
        Item.__init__(self)
        self.x1 = 0.0
        self.y1 = 0.0
        self.x2 = 0.0
        self.y2 = 0.0        
        self.pen = {}
        self.pen["width"] = 0.0
        self.pen["color"] = "#000000"
        self.pen["style"] = "SolidLine"

    def toXml(self):
        return "\n".join([
            '<line x1="{:e}" y1="{:e}" x2="{:e}" y2="{:e}">'.format(self.x1, self.y1, self.x2, self.y2),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            '</line>'])

    def fromQucsLib(self, line):
        _, x1, y1, x2, y2, pc, pw, ps = line.strip()[1:-1].split()
        self.x1 = convert(x1)
        self.y1 = convert(y1)
        self.x2 = self.x1 + convert(x2) # WTF?
        self.y2 = self.y1 + convert(y2) # WTF?
        self.pen = fromQucsLibPen(pc, pw, ps)


class Arrow(Item):
    def __init__(self):
        Item.__init__(self)
        self.x1 = 0.0
        self.y1 = 0.0
        self.x2 = 0.0
        self.y2 = 0.0
        self.x3 = 0.0
        self.y3 = 0.0
        self.x4 = 0.0
        self.y4 = 0.0
        self.pen = {}
        self.pen["width"] = 0.0
        self.pen["color"] = "#000000"
        self.pen["style"] = "SolidLine"

    def toXml(self):
        return "\n".join([
            '<line x1="{:g}" y1="{:g}" x2="{:g}" y2="{:g}">'.format(self.x1, self.y1, self.x2, self.y2),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            '</line>'
            '<line x1="{:g}" y1="{:g}" x2="{:g}" y2="{:g}">'.format(self.x2, self.y2, self.x3, self.y3),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            '</line>'
            '<line x1="{:g}" y1="{:g}" x2="{:g}" y2="{:g}">'.format(self.x2, self.y2, self.x4, self.y4),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            '</line>'

        ])

    def fromQucsLib(self, line):
        _, x1, y1, x2, y2, x3, y3, pc, pw, ps, unknown = line.strip()[1:-1].split()
        self.x1 = convert(x1)
        self.y1 = convert(y1)
        self.x2 = convert(x2)
        self.y2 = convert(y2)
        self.x3 = convert(x3)
        self.y3 = convert(y3)

        beta   = math.atan2(self.y3, self.x3);
        phi    = math.atan2(self.y2, self.x2);
        Length = math.sqrt(self.y3*self.y3 + self.x3*self.x3);
        self.x2 = self.x2 + self.x1
        self.y2 = self.y2 + self.y1
        w = beta+phi
        self.x3= self.x2-Length*math.cos(w)
        self.y3= self.y2-Length*math.sin(w)
        w = phi-beta
        self.x4 = self.x2-Length*math.cos(w)
        self.y4 = self.y2-Length*math.sin(w)
        
        self.pen = fromQucsLibPen(pc, pw, ps)
        

class Arc(Item):
    def __init__(self):
        Item.__init__(self)
        self.xCenter = 0.0
        self.yCenter = 0.0
        self.xRadius = 0.0
        self.yRadius = 0.0
        self.startAngle = 0.0
        self.spanAngle = 360.0

    def toXml(self):
        return "\n".join([
            '<ellipse x="{:g}" y="{:g}" width="{:g}" height="{:g}" startAngle="{:g}" spanAngle="{:g}" style="arc">'.format(self.xCenter, self.yCenter, 2*self.xRadius, 2*self.yRadius, self.startAngle, self.spanAngle),
            ' <stroke width="{:s}" color="{:s}" style="{:s}"/>'.format(self.pen["width"], self.pen["color"], self.pen["style"]),
            '</ellipse>'])

    # <EArc -15 -11 16 13 1532 1348 #000080 1 1>
    def fromQucsLib(self, line):
        if len(line.strip()[1:-1].split()) != 10:
            raise(Exception(line))
        _,x,y,w,h,sta,spa,pc,pw,ps = line.strip()[1:-1].split()
        self.xCenter = convert(x) + convert(w)/2.0
        self.yCenter = convert(y) + convert(h)/2.0
        self.xRadius = convert(w)/2.0
        self.yRadius = convert(h)/2.0
        self.startAngle = int(sta)/16.0
        self.spanAngle = int(spa)/16.0
        self.pen = fromQucsLibPen(pc, pw, ps)
        
class Port(Item):
    def __init__(self):
        Item.__init__(self)
        self.name = ""
        self.description=""

    def toXml(self):
        return "\n".join([
            '<port x="{:g}" y="{:g}" name="{:s}"/>'.format(self.x, self.y, self.name)])
        
    def fromQucsLib(self, line):
        _, x, y, num, _ = line.strip()[1:-1].split()
        self.name = num
        self.x = convert(x)
        self.y = convert(y)

class Id(Item):
    def __init__(self):
        Item.__init__(self)
        self.name = ""
        self.parameters=[]

    def toXml(self):
        return "\n".join([
            '<id x="{:g}" y="{:g}" name="{:s}">'.format(self.x, self.y, self.name),
            '\n'.join([' <parameter>{:s}</parameter>'.format("|".join(param for param in parameter)) for parameter in self.parameters]),
            '</id>'])
        
    def fromQucsLib(self, line):
        tokens = line.strip()[1:-1].split()
        x, y, name = tokens[1:4]
        self.name = name
        self.x = convert(x)
        self.y = convert(y)
        if len(tokens) >= 5:
            for param in tokens[4:]:
                params = param.strip('"').split('=')
                self.parameters.append(params)

class Text(Item):
    def __init__(self):
        Item.__init__(self)
        self.content = ""
        
    def toXml(self):
        return "\n".join([
            '<label x="{:g}" y="{:g}" text="{:s}" size="{:g}" color="{:s}">'.format(self.x, self.y, escape(self.content), self.size, self.pen["color"]),
            # size, color
            '</label>'])
        
    def fromQucsLib(self, line):
        tokens = line.strip()[1:-1].split();
        _, x,y,size,color,rot = tokens[0:6]
        text = " ".join(tokens[6:]).strip('"')
        self.x = convert(x)
        self.y = convert(y)
        self.content = text
        self.size = convertTextSize(size)
        self.brush["style"] = "solid"
        self.brush["color"] = color

    # text;x;y;content;color;size;sin;cos;over;under
    def fromCsv(self, tokens):
        self.x = convert(tokens[1])
        self.y = convert(tokens[2])
        self.content = tokens[3]
        self.size = convertTextSize(tokens[5])
        self.brush["style"] = "solid"
        self.brush["color"] = tokens[4]
        #if tokens[6] != "0" and tokens[7] != "1":
        #    raise Exception("Orientation not supported")
        if tokens[8] != "0" and tokens[9] != "0":
            raise Exception("Text under/over line not supported")
    
# component;flags;cx;cy;tx;ty;x1;y1;x2;y2
class Symbol():
    def __init__(self):
        self.caption = ""
        self.description = ""
        self.items = []
        
    def toXml(self):
        return "\n".join([
            '<?xml version="1.0"?>',
            '<symbol xmlns="http://www.leda.org/xdl" version="1.0">',
            ' <caption>{:s}</caption>'.format(self.caption),
            ' <description>{:s}</description>'.format(self.description),
            '\n'.join(item.toXml() for item in self.items),
            '</symbol>'])
                      
    def fromQucsLib(self, lines):
        for line in lines:
            line = line.strip()
            item = None
            if line.startswith('<Rectangle '): # Ouput shape first to get them in the BG
                item = Rect()
            elif line.startswith('<Ellipse '):
                item = Ellipse()
            elif line.startswith('<Line '):
                item = Line()
            elif line.startswith('<EArc '):
                item = Arc()
            elif line.startswith('<Port '):
                item = Port()
            elif line.startswith('<.PortSym '):
                item = Port()
            elif line.startswith('<Text '):
                item = Text()
            elif line.startswith('<.ID '):
                continue
                #item = Id()
            elif line.startswith('<Arrow '):
                item = Arrow()
            else:
                raise Exception('Unsupported primitive: {:s}'.format(line))
            item.fromQucsLib(line)
            self.items.append(item)

class Component:
    def __init__(self):
        self.name=""
        self.description=[]
        self.qucs=[]
        self.spice=[]
        self.symbol=None

    def toXml(self):
        return "\n".join([
            '  <component name="{:s}">'.format(escape(self.name)),
            '    <description>{:s}</description>'.format(escape("\n".join(self.description))),
            '    <qucs>{:s}</qucs>'.format(escape("\n".join(self.qucs))) if len(self.qucs) else '',
            '    <spice>{:s}</spice>'.format(escape("\n".join(self.spice))) if len(self.spice) else '',
            self.symbol.toXml() if self.symbol is not None else '',
            '  </component>'])

    def fromQucsLib(self, lines):
        InComponent=1
        InDescription=2
        InQucs=3
        InSpice=4
        InSymbol=5
        state=InComponent
        subLines=[]
        for line in lines:
            if state == InComponent:
                if line.startswith("<Description>"):
                    state = InDescription
                    continue
                if line.startswith("<Model>"):
                    state = InQucs
                    continue
                if line.startswith("<Spice>"):
                    state = InSpice
                    continue
                if line.startswith("<Symbol>"):
                    state = InSymbol
                    continue
                if line.startswith("<SpiceAttach "):
                    continue
                if line.startswith("<ModelIncludes "):
                    continue
                
            if state == InDescription:
                if line.startswith("</Description>"):
                    state = InComponent
                    continue
                self.description.append(line)
                continue

            if state == InQucs:
                if line.startswith("</Model>"):
                    state = InComponent
                    continue
                self.qucs.append(line)
                continue

            if state == InSpice:
                if line.startswith("</Spice>"):
                    state = InComponent
                    continue
                self.spice.append(line)
                continue
                
            if state == InSymbol:
                if line.startswith("</Symbol>"):
                    self.symbol = Symbol()
                    self.symbol.fromQucsLib(subLines)
                    state = InComponent
                    continue
                subLines.append(line)
                continue

            raise(Exception("Garbage: '{:s}".format(line)))

class Library:

    def __init__(self):
        self.name=None
        self.symbol=None
        self.components=[]
        self.debug = True
        
    def toXml(self):
        return "\n".join([
            '<library name="{:s}">'.format(self.name),
            self.symbol.toXml() if self.symbol is not None else '',
            '\n'.join(component.toXml() for component in self.components),
            '</library>'])

    def fromQucsLib(self, lines):
        InLibrary=1
        InSymbol=1
        InComponent=2
        state = InLibrary
        subLines=[]
        component=None
        for line in lines:
            if state == InLibrary:
                if line.startswith("#"):
                    continue
                if line.startswith("<DefaultSymbol>"):
                    state = InSymbol
                    subLines=[]
                    continue
                if line.startswith("<Component"):
                    state = InComponent
                    subLines=[]
                    component = Component()
                    component.name = line.split()[1][:-1]
                    continue

            if state == InSymbol:
                if line.startswith("</DefaultSymbol>"):
                    self.symbol = Symbol()
                    self.symbol.fromQucsLib(subLines)
                    state = InLibrary
                    continue
                subLines.append(line)
                continue

            if state == InComponent:
                if line == "</Component>":
                    component.fromQucsLib(subLines)
                    self.components.append(component)
                    state = InLibrary
                    continue
                subLines.append(line)
                continue

            raise(Exception("Garbage: '{:s}".format(line)))


class LibraryReader:
    def __init__(self):
        self.library = None

    def read(self, path):
        InDocument=1
        InLibrary=2
        state = InDocument
        self.library = Library()
        self.library.name=os.path.basename(sys.argv[1])

        lines = [line.strip() for line in open(sys.argv[1])]
        subLines=[]
        for line in lines:
            if len(line) == 0:
                continue
            if state == InDocument:
                if line.startswith("<Qucs Library "):
                    state = InLibrary
                    continue
                
            if state == InLibrary:
                subLines.append(line)
                continue

            raise(Exception("{:s}: Got garbage data: '{:s}".format(path, line)))

        if state != InLibrary:
            raise("Premature end of file")

        self.library.fromQucsLib(subLines)
        return self.library
    
def printStats():
    libName=""
    compCount = 0
    compSymCount = 0
    libSymCount = 0
    reader = LibraryReader()
    try:
        lib=reader.read(sys.argv[1])
        libName=lib.name
        if lib.symbol is not None:
            libSymCount = 1
        for comp in lib.components:
            compCount = compCount + 1
            if comp.symbol is not None:
                compSymCount = compSymCount + 1
    except:
        #raise
        pass
    print("{0:20s}\t{1:3d} components\t{2:3d} local symbols\t{3:4d} global symbols".format(libName, compCount, compSymCount, libSymCount))


def exportSymbol(sym, filename):
    with open(filename.split("/")[-1]+".qsym", 'w') as out:
        out.write(sym + '\n')

def exportSymbolsFromLib(filename):
    libName=""
    compName = 0
    reader = LibraryReader()
    try:
        lib=reader.read(filename)
    except:
        raise
        sys.exit(-1)
    libName=lib.name
    if lib.symbol is not None:
        exportSymbol(lib.symbol.toXml(), libName.replace(".", "_"))
    i = 1
    for comp in lib.components:
        if comp.symbol is not None:
            exportSymbol(comp.symbol.toXml(), "{:s}_{:03d}_{:s}".format(libName.replace(".", "_"), i, comp.name))
            i=i+1

def exportSymbolFromStandalone(filename):
    with open(filename, 'r') as data:
        sym = Symbol()
        subLines=[]
        for line in data.readlines():
            line=line.strip()
            if len(line) ==0 or line == "<Symbol>" or line == "</Symbol>":
                continue
            subLines.append(line)
        sym.fromQucsLib(subLines)
        exportSymbol(sym.toXml(), filename.replace(".", "_"))

        
if sys.argv[1].endswith(".lib"):
    exportSymbolsFromLib(sys.argv[1])
elif sys.argv[1].endswith(".sym"):
    exportSymbolFromStandalone(sys.argv[1])
else:
    raise(Exception("Unknown input file"))
