<?xml version="1.0" encoding="UTF-8"?>
<library version="0.0">
 <components count="5">
  <component uuid="qucs:DFF::Y:dflipflop:D-FlipFlop:" caption="D-FlipFlop" name="Y" model="DFF">
   <description>D flip flop with asynchron reset</description>
   <attribution>
    <statement>Copyright 2017, The Qucs Project</statement>
    <license>https://www.some.org/license</license>
   </attribution>
   <documentation></documentation>
   <interface>
    <ports>
     <port name="0" direction="inout"/>
     <port name="1" direction="inout"/>
     <port name="2" direction="inout"/>
     <port name="3" direction="inout"/>
    </ports>
    <parameters count="1">
     <parameter name="t" type="real" display="false">
      <description>Delay time</description>
      <value>0.000000e+00</value>
     </parameter>
    </parameters>
   </interface>
   <implementations>
    <implementation caption="Verilog">
     <description>Implementation for Verilog</description>
     <code language="verilog">
  // Y${?} D-flipflop
  assign  ${2} = net_regY${?}${2};
  reg     net_regY${?}${2} = 0;
  always @ (${1} or ${3}) begin
    if (${3}) net_regY${?}${2} &lt;= 0;
    else if (~${3} &amp;&amp; ${1}) net_regY${?}${2} &lt;= ${0};
  end

</code>
    </implementation>
    <implementation caption="Vhdl">
     <description>Implementation for Vhdl</description>
     <code language="vhdl">  Y${?} : process (${0}, ${1})
  begin
    if (${3}='1') then  ${2} &lt;= '0';
    elsif (${1}='1' and ${1}'event) then
      ${2} &lt;= ${0};
    end if;
  end process;

</code>
    </implementation>
   </implementations>
   <representations>
    <representation>
     <portMapping>
      <map from="0" to="-30;-10"/>
      <map from="1" to="-30;10"/>
      <map from="2" to="30;-10"/>
      <map from="3" to="0;30"/>
     </portMapping>
     <graphics count="10">
      <caption>Default</caption>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-4" x2="4" y2="-4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="4" x2="4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-4" x2="-4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="4" y1="-4" x2="4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="-2" x2="-4" y2="-2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="2" x2="-4" y2="2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="-2" x2="4" y2="-2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="0" y1="4" x2="0" y2="6">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="1.2" x2="-2.4" y2="2">
       <stroke width="smallest" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="2.8" x2="-2.4" y2="2">
       <stroke width="smallest" style="solid" color="#000080"/>
      </line>
      <label x="-3.6" y="-4.2" rotation="0" locked="false" mirrored="false" visible="true" text="D" size="24" color="#000080"/>
      <label x="1.2" y="-4.2" rotation="0" locked="false" mirrored="false" visible="true" text="Q" size="24" color="#000080"/>
      <label x="-0.8" y="0.8" rotation="0" locked="false" mirrored="false" visible="true" text="R" size="18" color="#000080"/>
     </graphics>
    </representation>
   </representations>
  </component>
  <component uuid="qucs:DLS_1ton::Y:DLS_1ton:D2A_Level_Shifter:" caption="D2A_Level_Shifter" name="Y" model="DLS_1ton">
   <description>data voltage level shifter (digital to analogue) verilog device</description>
   <attribution>
    <statement>Copyright 2017, The Qucs Project</statement>
    <license>https://www.some.org/license</license>
   </attribution>
   <documentation></documentation>
   <interface>
    <ports>
     <port name="0" direction="inout"/>
     <port name="1" direction="inout"/>
    </ports>
    <parameters count="2">
     <parameter name="LEVEL" type="real" unit="V" display="false">
      <description>Voltage level</description>
      <value>5.000000e+00</value>
     </parameter>
     <parameter name="Delay" type="real" unit="s" display="false">
      <description>Time delay (s)</description>
      <value>1.000000e-09</value>
     </parameter>
    </parameters>
   </interface>
   <implementations>
    <implementation caption="Qucs">
     <description>Implementation for Qucs</description>
     <code language="qucs">DLS_1ton:Y${?} ${0} ${1} LEVEL=&quot;${LEVEL}&quot; Delay=&quot;${Delay}&quot;
</code>
    </implementation>
   </implementations>
   <representations>
    <representation>
     <portMapping>
      <map from="0" to="-40;0"/>
      <map from="1" to="40;0"/>
     </portMapping>
     <graphics count="9">
      <caption>Default</caption>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="-6" x2="6" y2="-6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="-6" x2="6" y2="6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="6" x2="-6" y2="6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="6" x2="-6" y2="-6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="6" x2="6" y2="-6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-8" y1="0" x2="-6" y2="0">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="0" x2="8" y2="0">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-5" y1="-4" x2="-3" y2="-4">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="5" y1="4" x2="3" y2="4">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <label x="-2" y="-6.4" rotation="0" locked="false" mirrored="false" visible="true" text="1" size="24" color="#800000"/>
      <label x="0" y="1.6" rotation="0" locked="false" mirrored="false" visible="true" text="n" size="24" color="#800000"/>
     </graphics>
    </representation>
   </representations>
  </component>
  <component uuid="qucs:DLS_nto1::Y:DLS_nto1:A2D_Level_Shifter:" caption="A2D_Level_Shifter" name="Y" model="DLS_nto1">
   <description>data voltage level shifter (analogue to digital) verilog device</description>
   <attribution>
    <statement>Copyright 2017, The Qucs Project</statement>
    <license>https://www.some.org/license</license>
   </attribution>
   <documentation></documentation>
   <interface>
    <ports>
     <port name="0" direction="inout"/>
     <port name="1" direction="inout"/>
    </ports>
    <parameters count="2">
     <parameter name="LEVEL" type="real" unit="V" display="false">
      <description>Voltage level (V)</description>
      <value>5.000000e+00</value>
     </parameter>
     <parameter name="Delay" type="real" unit="s" display="false">
      <description>Time delay (s)</description>
      <value>1.000000e-09</value>
     </parameter>
    </parameters>
   </interface>
   <implementations>
    <implementation caption="Qucs">
     <description>Implementation for Qucs</description>
     <code language="qucs">DLS_nto1:Y${?} ${0} ${1} LEVEL=&quot;${LEVEL}&quot; Delay=&quot;${Delay}&quot;
</code>
    </implementation>
   </implementations>
   <representations>
    <representation>
     <portMapping>
      <map from="0" to="-40;0"/>
      <map from="1" to="40;0"/>
     </portMapping>
     <graphics count="9">
      <caption>Default</caption>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="-6" x2="6" y2="-6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="-6" x2="6" y2="6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="6" x2="-6" y2="6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="6" x2="-6" y2="-6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="6" x2="6" y2="-6">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-8" y1="0" x2="-6" y2="0">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="0" x2="8" y2="0">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-5" y1="-4" x2="-3" y2="-4">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="5" y1="4" x2="3" y2="4">
       <stroke width="small" style="solid" color="#800000"/>
      </line>
      <label x="-2" y="-6.4" rotation="0" locked="false" mirrored="false" visible="true" text="n" size="24" color="#800000"/>
      <label x="0" y="1.6" rotation="0" locked="false" mirrored="false" visible="true" text="1" size="24" color="#800000"/>
     </graphics>
    </representation>
   </representations>
  </component>
  <component uuid="qucs:JKFF::Y:jkflipflop:JK-FlipFlop:" caption="JK-FlipFlop" name="Y" model="JKFF">
   <description>JK flip flop with asynchron set and reset</description>
   <attribution>
    <statement>Copyright 2017, The Qucs Project</statement>
    <license>https://www.some.org/license</license>
   </attribution>
   <documentation></documentation>
   <interface>
    <ports>
     <port name="0" direction="inout"/>
     <port name="1" direction="inout"/>
     <port name="2" direction="inout"/>
     <port name="3" direction="inout"/>
     <port name="4" direction="inout"/>
     <port name="5" direction="inout"/>
     <port name="6" direction="inout"/>
    </ports>
    <parameters count="1">
     <parameter name="t" type="real" display="false">
      <description>Delay time</description>
      <value>0.000000e+00</value>
     </parameter>
    </parameters>
   </interface>
   <implementations>
    <implementation caption="Verilog">
     <description>Implementation for Verilog</description>
     <code language="verilog">
  // Y${?} JK-flipflop
  assign  ${2} = net_regY${?}${2};
  assign  ${3} = ~${2};
  reg     net_regY${?}${2} = 0;
  always @ (${4} or ${6} or ${5}) begin
    if (${6}) net_regY${?}${2} &lt;= 0;
    else if (${5}) net_regY${?}${2} &lt;= 1;
    else if (${4})
      net_regY${?}${2} &lt;= (${0} &amp;&amp; ~${2}) || (~${1} &amp;&amp; ${2});
  end

</code>
    </implementation>
    <implementation caption="Vhdl">
     <description>Implementation for Vhdl</description>
     <code language="vhdl">  Y${?} : process (${5}, ${6}, ${4})
  begin
    if (${6}='1') then  ${2} &lt;= '0';
    elsif (${5}='1') then  ${2} &lt;= '1';
    elsif (${4}='1' and ${4}'event) then
      ${2} &lt;= (${0} and not ${2}) or (not ${1} and ${2});
    end if;
  end process;
  ${3} &lt;= not ${2};

</code>
    </implementation>
   </implementations>
   <representations>
    <representation>
     <portMapping>
      <map from="0" to="-30;-20"/>
      <map from="1" to="-30;20"/>
      <map from="2" to="30;-20"/>
      <map from="3" to="30;20"/>
      <map from="4" to="-30;0"/>
      <map from="5" to="0;-40"/>
      <map from="6" to="0;40"/>
     </portMapping>
     <graphics count="13">
      <caption>Default</caption>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-6" x2="4" y2="-6">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="6" x2="4" y2="6">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-6" x2="-4" y2="6">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="4" y1="-6" x2="4" y2="6">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="-4" x2="-4" y2="-4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="4" x2="-4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="-4" x2="4" y2="-4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="4" x2="4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="0" x2="-4" y2="0">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="0" y1="-6" x2="0" y2="-8">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="0" y1="6" x2="0" y2="8">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-0.8" x2="-2.4" y2="0">
       <stroke width="smallest" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="0.8" x2="-2.4" y2="0">
       <stroke width="smallest" style="solid" color="#000080"/>
      </line>
      <label x="-0.8" y="-5.8" rotation="0" locked="false" mirrored="false" visible="true" text="S" size="18" color="#000080"/>
      <label x="-0.8" y="2.8" rotation="0" locked="false" mirrored="false" visible="true" text="R" size="18" color="#000080"/>
      <label x="-3.6" y="-6.2" rotation="0" locked="false" mirrored="false" visible="true" text="J" size="24" color="#000080"/>
      <label x="-3.6" y="1.6" rotation="0" locked="false" mirrored="false" visible="true" text="K" size="24" color="#000080"/>
      <label x="1.2" y="-6.2" rotation="0" locked="false" mirrored="false" visible="true" text="Q" size="24" color="#000080"/>
      <label x="1.2" y="1.6" rotation="0" locked="false" mirrored="false" visible="true" text="Q" size="24" color="#000080"/>
     </graphics>
    </representation>
   </representations>
  </component>
  <component uuid="qucs:RSFF::Y:rsflipflop:RS-FlipFlop:" caption="RS-FlipFlop" name="Y" model="RSFF">
   <description>RS flip flop</description>
   <attribution>
    <statement>Copyright 2017, The Qucs Project</statement>
    <license>https://www.some.org/license</license>
   </attribution>
   <documentation></documentation>
   <interface>
    <ports>
     <port name="0" direction="inout"/>
     <port name="1" direction="inout"/>
     <port name="2" direction="inout"/>
     <port name="3" direction="inout"/>
    </ports>
    <parameters count="1">
     <parameter name="t" type="real" display="false">
      <description>Delay time</description>
      <value>0.000000e+00</value>
     </parameter>
    </parameters>
   </interface>
   <implementations>
    <implementation caption="Verilog">
     <description>Implementation for Verilog</description>
     <code language="verilog">
  // Y${?} RS-flipflop
  assign ${2} = ~(${0} | ${3});
  assign ${3} = ~(${1} | ${2});

</code>
    </implementation>
    <implementation caption="Vhdl">
     <description>Implementation for Vhdl</description>
     <code language="vhdl">  ${2} &lt;= ${0} nor ${3};
  ${3} &lt;= ${1} nor ${2};

</code>
    </implementation>
   </implementations>
   <representations>
    <representation>
     <portMapping>
      <map from="0" to="-30;-10"/>
      <map from="1" to="-30;10"/>
      <map from="2" to="30;-10"/>
      <map from="3" to="30;10"/>
     </portMapping>
     <graphics count="8">
      <caption>Default</caption>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-4" x2="4" y2="-4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="4" x2="4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-4" y1="-4" x2="-4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="4" y1="-4" x2="4" y2="4">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="-2" x2="-4" y2="-2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="-6" y1="2" x2="-4" y2="2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="-2" x2="4" y2="-2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <line x="0" y="0" rotation="0" locked="false" mirrored="false" visible="true" x1="6" y1="2" x2="4" y2="2">
       <stroke width="small" style="solid" color="#000080"/>
      </line>
      <label x="-3.6" y="-4.2" rotation="0" locked="false" mirrored="false" visible="true" text="R" size="24" color="#000080"/>
      <label x="-3.6" y="-0.2" rotation="0" locked="false" mirrored="false" visible="true" text="S" size="24" color="#000080"/>
      <label x="1.2" y="-4.2" rotation="0" locked="false" mirrored="false" visible="true" text="Q" size="24" color="#000080"/>
      <label x="1.2" y="-0.2" rotation="0" locked="false" mirrored="false" visible="true" text="Q" size="24" color="#000080"/>
     </graphics>
    </representation>
   </representations>
  </component>
 </components>
</library>
