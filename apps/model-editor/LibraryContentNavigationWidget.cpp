/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "LibraryModel.h"
#include "LibraryContentNavigationWidget.h"

#include "qlCore/LibraryObjects.h"

#include <QtCore/QSortFilterProxyModel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QVBoxLayout>

LibraryContentNavigationWidget::LibraryContentNavigationWidget(QWidget *parent)
    : QWidget(parent)
    , m_libraryModel(new LibraryModel(this))
    , m_listView(new QListView())
    , m_sortFilterProxyModel(new QSortFilterProxyModel(this))
    , m_filterLineEdit(new QLineEdit)
{
    m_filterLineEdit->setPlaceholderText("Filter...");
    m_sortFilterProxyModel->setSourceModel(m_libraryModel);
    m_sortFilterProxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_listView->setModel(m_sortFilterProxyModel);
    connect(m_filterLineEdit, &QLineEdit::textChanged,
            m_sortFilterProxyModel, &QSortFilterProxyModel::setFilterWildcard);
    connect(m_listView, &QListView::activated,
            this, [this](const QModelIndex &index) {
        auto sourceIndex = m_sortFilterProxyModel->mapToSource(index);
        emit componentActivated(m_libraryModel->library()->cells.value(sourceIndex.row()));
    });
    setLayout(new QVBoxLayout());
    layout()->addWidget(m_filterLineEdit);
    layout()->addWidget(m_listView);
}

void LibraryContentNavigationWidget::setLibrary(lib::Library *library)
{
    m_libraryModel->setLibrary(library);
    m_listView->setCurrentIndex(m_sortFilterProxyModel->index(0, 0));
}

lib::Library *LibraryContentNavigationWidget::library() const
{
    return m_libraryModel->library();
}
