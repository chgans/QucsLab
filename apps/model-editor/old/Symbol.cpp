/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "Symbol.h"
#include "UndoCommand.h"

// For ObjectProxy
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QEvent>

Object::Object(Object *parent)
    : QObject(parent)
    , m_uid(ObjectUid::createId())
{

}

Object::~Object()
{

}

QList<Object *> Object::childObjects()
{
    return findChildren<Object*>(QString(), Qt::FindDirectChildrenOnly);
}

QList<const Object *> Object::childObjects() const
{
    return findChildren<const Object*>(QString(), Qt::FindDirectChildrenOnly);
}

ObjectUid Object::uid() const
{
    return m_uid;
}

QPointF Object::location() const
{
    return m_location;
}

qreal Object::rotation() const
{
    return m_rotation;
}

bool Object::isMirrored() const
{
    return m_mirrored;
}

bool Object::isVisible() const
{
    return m_visible;
}

bool Object::isLocked() const
{
    return m_locked;
}

void Object::setLocation(const QPointF &location)
{
    if (m_location == location)
        return;

    m_location = location;
    emit locationChanged(location);
}

void Object::setRotation(qreal rotation)
{
    if (m_rotation == rotation)
        return;

    m_rotation = rotation;
    emit rotationChanged(rotation);
}

void Object::setMirrored(bool mirrored)
{
    if (m_mirrored == mirrored)
        return;

    m_mirrored = mirrored;
    emit mirroredChanged(mirrored);
}

void Object::setVisible(bool visible)
{
    if (m_visible == visible)
        return;

    m_visible = visible;
    emit visibleChanged(visible);
}

void Object::setLocked(bool locked)
{
    if (m_locked == locked)
        return;

    m_locked = locked;
    emit lockedChanged(locked);
}

void Object::setUid(ObjectUid uid)
{
    if (m_uid == uid)
        return;
    m_uid = uid;
    emit uidChanged(m_uid);
}


Ellipse::Ellipse(Object *parent)
    : Object(parent)
{

}

Ellipse::~Ellipse()
{

}

void Ellipse::setStyle(Qs::EllipseStyle style)
{
    if (m_style == style)
        return;

    m_style = style;
    emit styleChanged(style);
}

void Ellipse::setStrokeWidth(Qs::StrokeWidth strokeWidth)
{
    if (m_strokeWidth == strokeWidth)
        return;

    m_strokeWidth = strokeWidth;
    emit strokeWidthChanged(strokeWidth);
}

void Ellipse::setStrokeStyle(Qs::StrokeStyle strokeStyle)
{
    if (m_strokeStyle == strokeStyle)
        return;

    m_strokeStyle = strokeStyle;
    emit strokeStyleChanged(strokeStyle);
}

void Ellipse::setStrokeColor(const QColor &strokeColor)
{
    if (m_strokeColor == strokeColor)
        return;

    m_strokeColor = strokeColor;
    emit strokeColorChanged(strokeColor);
}

void Ellipse::setFillStyle(Qs::FillStyle fillStyle)
{
    if (m_fillStyle == fillStyle)
        return;

    m_fillStyle = fillStyle;
    emit fillStyleChanged(fillStyle);
}

void Ellipse::setFillColor(const QColor &fillColor)
{
    if (m_fillColor == fillColor)
        return;

    m_fillColor = fillColor;
    emit fillColorChanged(fillColor);
}

void Ellipse::setSpanAngle(qreal spanAngle)
{
    if (m_spanAngle == spanAngle)
        return;

    m_spanAngle = spanAngle;
    emit spanAngleChanged(spanAngle);
}

void Ellipse::setStartAngle(qreal startAngle)
{
    if (m_startAngle == startAngle)
        return;

    m_startAngle = startAngle;
    emit startAngleChanged(startAngle);
}

void Ellipse::setSize(const QSizeF &size)
{
    if (m_size == size)
        return;

    m_size = size;
    emit sizeChanged(size);
}

QSizeF Ellipse::size() const
{
    return m_size;
}

qreal Ellipse::startAngle() const
{
    return m_startAngle;
}

qreal Ellipse::spanAngle() const
{
    return m_spanAngle;
}

Qs::EllipseStyle Ellipse::style() const
{
    return m_style;
}

Qs::StrokeWidth Ellipse::strokeWidth() const
{
    return m_strokeWidth;
}

Qs::StrokeStyle Ellipse::strokeStyle() const
{
    return m_strokeStyle;
}

QColor Ellipse::strokeColor() const
{
    return m_strokeColor;
}

Qs::FillStyle Ellipse::fillStyle() const
{
    return m_fillStyle;
}

QColor Ellipse::fillColor() const
{
    return m_fillColor;
}

ObjectProxy::ObjectProxy(QObject *parent)
    : QObject(parent)
{

}

ObjectProxy::~ObjectProxy()
{

}

bool ObjectProxy::event(QEvent *event)
{
    if (event->type() != QEvent::DynamicPropertyChange)
        return QObject::event(event);

    // A property has been added, removed or changed
    // FIXME: We assume a property change event
    auto changeEvent = static_cast<QDynamicPropertyChangeEvent *>(event);
    const auto name = changeEvent->propertyName();
    const auto value = property(name);

    m_command.reset(new UpdateObjectCommand());
    m_command->setUid(m_object->uid());
    m_command->setProperty(name, value);
    emit commandAvailable();

    return true;
}

void ObjectProxy::setObject(const Object *object)
{
    m_object = object;
    // Discard pending command if any
    delete m_command.take();
}

const Object *ObjectProxy::object() const
{
    return m_object;
}

UndoCommand *ObjectProxy::takeCommand()
{
    return m_command.take();
}

Symbol::Symbol(Object *parent)
    : Object(parent)
{

}

Symbol::~Symbol()
{

}

QString Symbol::caption() const
{
    return m_caption;
}

QString Symbol::description() const
{
    return m_description;
}

void Symbol::setCaption(QString caption)
{
    if (m_caption == caption)
        return;

    m_caption = caption;
    emit captionChanged(caption);
}

void Symbol::setDescription(QString description)
{
    if (m_description == description)
        return;

    m_description = description;
    emit descriptionChanged(description);
}

Rectangle::Rectangle(Object *parent)
    : Object(parent)
{

}

Rectangle::~Rectangle()
{

}

void Rectangle::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    m_size = size;
    emit sizeChanged(size);
}

void Rectangle::setStrokeWidth(Qs::StrokeWidth strokeWidth)
{
    if (m_strokeWidth == strokeWidth)
        return;

    m_strokeWidth = strokeWidth;
    emit strokeWidthChanged(strokeWidth);
}

void Rectangle::setStrokeStyle(Qs::StrokeStyle strokeStyle)
{
    if (m_strokeStyle == strokeStyle)
        return;

    m_strokeStyle = strokeStyle;
    emit strokeStyleChanged(strokeStyle);
}

void Rectangle::setStrokeColor(const QColor &strokeColor)
{
    if (m_strokeColor == strokeColor)
        return;

    m_strokeColor = strokeColor;
    emit strokeColorChanged(strokeColor);
}

void Rectangle::setFillStyle(Qs::FillStyle fillStyle)
{
    if (m_fillStyle == fillStyle)
        return;

    m_fillStyle = fillStyle;
    emit fillStyleChanged(fillStyle);
}

void Rectangle::setFillColor(const QColor &fillColor)
{
    if (m_fillColor == fillColor)
        return;

    m_fillColor = fillColor;
    emit fillColorChanged(fillColor);
}

QSizeF Rectangle::size() const
{
    return m_size;
}

Qs::StrokeWidth Rectangle::strokeWidth() const
{
    return m_strokeWidth;
}

Qs::StrokeStyle Rectangle::strokeStyle() const
{
    return m_strokeStyle;
}

QColor Rectangle::strokeColor() const
{
    return m_strokeColor;
}

Qs::FillStyle Rectangle::fillStyle() const
{
    return m_fillStyle;
}

QColor Rectangle::fillColor() const
{
    return m_fillColor;
}

Port::Port(Object *parent)
    : Object(parent)
{

}

Port::~Port()
{

}

QString Port::name() const
{
    return m_name;
}

void Port::setName(const QString &name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(name);
}

Line::Line(Object *parent)
    : Object(parent)
{

}

Line::~Line()
{

}

QPointF Line::p1() const
{
    return m_p1;
}

QPointF Line::p2() const
{
    return m_p2;
}

Qs::StrokeWidth Line::strokeWidth() const
{
    return m_strokeWidth;
}

Qs::StrokeStyle Line::strokeStyle() const
{
    return m_strokeStyle;
}

QColor Line::strokeColor() const
{
    return m_strokeColor;
}

void Line::setP1(const QPointF &point)
{
    if (m_p1 == point)
        return;
    m_p1 = point;
    emit p1Changed(m_p1);
}

void Line::setP2(const QPointF &point)
{
    if (m_p2 == point)
        return;
    m_p2 = point;
    emit p2Changed(m_p2);
}

void Line::setStrokeWidth(Qs::StrokeWidth strokeWidth)
{
    if (m_strokeWidth == strokeWidth)
        return;

    m_strokeWidth = strokeWidth;
    emit strokeWidthChanged(strokeWidth);
}

void Line::setStrokeStyle(Qs::StrokeStyle strokeStyle)
{
    if (m_strokeStyle == strokeStyle)
        return;

    m_strokeStyle = strokeStyle;
    emit strokeStyleChanged(strokeStyle);
}

void Line::setStrokeColor(const QColor &strokeColor)
{
    if (m_strokeColor == strokeColor)
        return;

    m_strokeColor = strokeColor;
    emit strokeColorChanged(strokeColor);
}

Label::Label(Object *parent)
    : Object(parent)
{

}

Label::~Label()
{

}

QString Label::text() const
{
    return m_text;
}

qreal Label::size() const
{
    return m_size;
}

QColor Label::color() const
{
    return m_color;
}

void Label::setText(const QString &text)
{
    if (m_text == text)
        return;

    m_text = text;
    emit textChanged(text);
}

void Label::setSize(qreal size)
{
    if (qFuzzyCompare(m_size, size))
        return;

    m_size = size;
    emit sizeChanged(size);
}

void Label::setColor(const QColor &color)
{
    if (m_color == color)
        return;

    m_color = color;
    emit colorChanged(color);
}
