/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsGrid.h"

#include <QtCore/QtMath>
#include <QtCore/QRectF>
#include <QtGui/QPainter>
#include <QtWidgets/QStyleOptionGraphicsItem>

GraphicsGrid::GraphicsGrid(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_rect(QRectF())
    , m_pen(QPen(Qt::black, 0.0))
    , m_step(10.0)
    , m_minimumGridSize(20)
{

}

GraphicsGrid::~GraphicsGrid()
{

}

void GraphicsGrid::setRect(const QRectF &rect)
{
    if (m_rect == rect)
        return;
    m_rect = rect;
    update();
}

QRectF GraphicsGrid::rect() const
{
    return m_rect;
}

void GraphicsGrid::setColor(const QColor &color)
{
    if (m_pen.color() == color)
        return;
    m_pen.setColor(color);
    update();
}

QColor GraphicsGrid::color() const
{
    return m_pen.color();
}

void GraphicsGrid::setStep(qreal step)
{
    if (qFuzzyCompare(m_step, step))
        return;
    m_step = step;
    update();
}

qreal GraphicsGrid::step() const
{
    return m_step;
}

void GraphicsGrid::setMinimumGridSize(int pixels)
{
    if (m_minimumGridSize == pixels)
        return;
    m_minimumGridSize = pixels;
    update();
}

int GraphicsGrid::minimumGridSize() const
{
    return m_minimumGridSize;
}

QRectF GraphicsGrid::boundingRect() const
{
    return m_rect;
}

void GraphicsGrid::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);

    qreal step = m_step;

    const qreal zoom = option->levelOfDetailFromTransform(painter->worldTransform());
    const qreal gridSize = zoom * step;
    if (gridSize < m_minimumGridSize) {
        step = m_minimumGridSize / zoom;
    }
//    else if (gridSize > m_maximumGridSize) {
//        step = m_maximumGridSize / zoom;
//    }

    const int hCount = qFloor(option->exposedRect.height()/step);
    const int vCount = qFloor(option->exposedRect.width()/step);
    const QPointF topLeft = option->exposedRect.topLeft();
    const QPointF bottomRight = option->exposedRect.bottomRight();
    qreal left = topLeft.x() + step - std::fmod(topLeft.x(), step);
    qreal top = topLeft.y() + step - std::fmod(topLeft.y(), step);

    QVector<QPointF> lines(hCount*2+vCount*2);
    QPointF *data = lines.data();

    const qreal gridLeft = topLeft.x();
    const qreal gridRight = bottomRight.x();
    for (int i=0; i<hCount; i++) {
        (*data).setX(gridLeft);
        (*data++).setY(top);
        (*data).setX(gridRight);
        (*data++).setY(top);
        top += step;
    }

    const qreal gridTop = topLeft.y();
    const qreal gridBottom = bottomRight.y();
    for (int i=0; i<vCount; i++) {
        (*data).setX(left);
        (*data++).setY(gridTop);
        (*data).setX(left);
        (*data++).setY(gridBottom);
        left += step;
    }

    painter->setPen(m_pen);
    painter->drawLines(lines);
}
