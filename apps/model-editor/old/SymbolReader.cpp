/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "Symbol.h"
#include "SymbolReader.h"

#include <QtCore/QDebug>
#include <QtCore/QXmlStreamReader>

class SymbolReaderPrivate
{
public:
    SymbolReaderPrivate()
    {}

    bool read(QXmlStreamReader *xml);
    bool read(QIODevice *device);
    void readSymbol();
    void readLine();
    void readEllipse();
    void readRectangle();
    void readLabel();
    void readPort();
    void readStroke(Object *object);
    void readFill(Object *object);

    Qs::StrokeStyle toStrokeStyle(QStringRef text);
    Qs::StrokeWidth toStrokeWidth(QStringRef text);
    Qs::FillStyle toFillStyle(QStringRef text);
    Qs::EllipseStyle toEllipseStyle(QStringRef text);

    qreal toReal(QStringRef text);
    QColor toColor(QStringRef text);

    void skipCurrentElement();
    void skipAttribute(const QXmlStreamAttribute &attribute);

    QXmlStreamReader *xml = nullptr;
    Symbol *symbol = nullptr;
};

SymbolReader::SymbolReader()
    : d_ptr(new SymbolReaderPrivate())
{

}

SymbolReader::~SymbolReader()
{

}

bool SymbolReader::readDocument(QIODevice *device)
{
    Q_D(SymbolReader);
    return d->read(device);
}

Symbol *SymbolReader::document() const
{
    Q_D(const SymbolReader);

    return d->symbol;
}

// FIXME: quick hack for copy/paste
bool SymbolReader::readObjects(QIODevice *device)
{
    return readDocument(device);
}

bool SymbolReader::readDocument(QXmlStreamReader *xml)
{
    Q_D(SymbolReader);
    return d->read(xml);
}

QList<Object *> SymbolReader::objects() const
{
    Q_D(const SymbolReader);

    return d->symbol->childObjects();
}

QString SymbolReader::errorString() const
{
    Q_D(const SymbolReader);

    return QObject::tr("%1\nLine %2, column %3")
            .arg(d->xml->errorString())
            .arg(d->xml->lineNumber())
            .arg(d->xml->columnNumber());
}

bool SymbolReaderPrivate::read(QXmlStreamReader *_xml)
{
    xml = _xml;
    readSymbol();
    return xml->hasError();
}

bool SymbolReaderPrivate::read(QIODevice *device)
{
    xml->setDevice(device);

    if (xml->readNextStartElement()) {
        if (xml->name() == "symbol" && xml->attributes().value("version") == "1.0")
            readSymbol();
        else
            xml->raiseError(QObject::tr("The file is not an QUCS Symbol version 1.0 file."));
    }
    return !xml->hasError();
}

void SymbolReaderPrivate::readSymbol()
{
    Q_ASSERT(xml->isStartElement() && (xml->name() == "symbol" || xml->name() == "graphics"));

    symbol = new Symbol;
    while (xml->readNextStartElement()) {
        if (xml->name() == "caption") // Should be attribute
            symbol->setCaption(xml->readElementText());
        else if (xml->name() == "description")
            symbol->setDescription(xml->readElementText());
        else if (xml->name() == "ellipse")
            readEllipse();
        else if (xml->name() == "line")
            readLine();
        else if (xml->name() == "rectangle")
            readRectangle();
        else if (xml->name() == "label")
            readLabel();
        else if (xml->name() == "port")
            readPort();
        else
            skipCurrentElement();
    }
    // TODO: check attributes and emit warning
}

void SymbolReaderPrivate::readLine()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "line");

    auto line = new Line(symbol);

    QPointF p1;
    QPointF p2;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "x1")
            p1.setX(toReal(attribute.value()));
        else if (attribute.name() == "y1")
            p1.setY(toReal(attribute.value()));
        else if (attribute.name() == "x2")
            p2.setX(toReal(attribute.value()));
        else if (attribute.name() == "y2")
            p2.setY(toReal(attribute.value()));
        else
            skipAttribute(attribute);
    }
    line->setP1(p1);
    line->setP2(p2);
    while (xml->readNextStartElement()) {
        if (xml->name() == "stroke")
            readStroke(line);
        else
            skipCurrentElement();
    }
}

void SymbolReaderPrivate::readEllipse()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "ellipse");

    auto ellipse = new Ellipse(symbol);
    QSizeF size;
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "width")
            size.setWidth(toReal(attribute.value()));
        else if (attribute.name() == "height")
            size.setHeight(toReal(attribute.value()));
        else if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else if (attribute.name() == "startAngle")
            ellipse->setStartAngle(toReal(attribute.value()));
        else if (attribute.name() == "spanAngle")
            ellipse->setSpanAngle(toReal(attribute.value()));
        else if (attribute.name() == "style")
            ellipse->setStyle(toEllipseStyle(attribute.value()));
        else
            skipAttribute(attribute);
    }
    ellipse->setSize(size);
    ellipse->setLocation(pos);

    while (xml->readNextStartElement()) {
        if (xml->name() == "stroke")
            readStroke(ellipse);
        else if (xml->name() == "fill")
            readFill(ellipse);
        else
            skipCurrentElement();
    }
}

void SymbolReaderPrivate::readRectangle()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "rectangle");

    auto rectangle = new Rectangle(symbol);
    QSizeF size;
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "width")
            size.setWidth(toReal(attribute.value()));
        else if (attribute.name() == "height")
            size.setHeight(toReal(attribute.value()));
        else if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else
            skipAttribute(attribute);
    }
    rectangle->setSize(size);
    rectangle->setLocation(pos);

    while (xml->readNextStartElement()) {
        if (xml->name() == "stroke")
            readStroke(rectangle);
        else if (xml->name() == "fill")
            readFill(rectangle);
        else
            skipCurrentElement();
    }
}

void SymbolReaderPrivate::readLabel()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "label");

    auto label = new Label(symbol);
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else if (attribute.name() == "size")
            label->setSize(toReal(attribute.value()));
        else if (attribute.name() == "text")
            label->setText(attribute.value().toString());
        else if (attribute.name() == "color")
            label->setColor(toColor(attribute.value()));
        else
            skipAttribute(attribute);
    }
    label->setLocation(pos);

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

void SymbolReaderPrivate::readPort()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "port");

    auto port = new Port(symbol);
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else if (attribute.name() == "name")
            port->setName(attribute.value().toString());
        else
            skipAttribute(attribute);
    }
    port->setLocation(pos);

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

void SymbolReaderPrivate::readStroke(Object *object)
{
    Q_ASSERT(xml->name() == "stroke");
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "width")
            object->setProperty("strokeWidth", toStrokeWidth(attribute.value()));
        else if (attribute.name() == "style")
            object->setProperty("strokeStyle", toStrokeStyle(attribute.value()));
        else if (attribute.name() == "color")
            object->setProperty("strokeColor", toColor(attribute.value()));
        else
            skipAttribute(attribute);
    }

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

void SymbolReaderPrivate::readFill(Object *object)
{
    Q_ASSERT(xml->name() == "fill");
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "style")
            object->setProperty("fillStyle", toFillStyle(attribute.value()));
        else if (attribute.name() == "color")
            object->setProperty("fillColor", toColor(attribute.value()));
        else
            skipAttribute(attribute);
    }

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

Qs::StrokeStyle SymbolReaderPrivate::toStrokeStyle(QStringRef text)
{
    if (text == "none")
        return Qs::NoStroke;
    if (text == "solid")
        return Qs::SolidStroke;
    if (text == "dash")
        return Qs::DashStroke;
    if (text == "dot")
        return Qs::DotStroke;
    if (text == "dashdot")
        return Qs::DashDotStroke;
    if (text == "dashdotdot")
        return Qs::DasDotDotStroke;

    xml->raiseError(QString("Unknown stroke style: '%1'").arg(text.toString()));
    return Qs::SolidStroke;
}

Qs::StrokeWidth SymbolReaderPrivate::toStrokeWidth(QStringRef text)
{
    if (text == "smallest")
        return Qs::SmallestStroke;
    if (text == "small")
        return Qs::SmallStroke;
    if (text == "medium")
        return Qs::MediumStroke;
    if (text == "large")
        return Qs::LargeStroke;
    if (text == "largest")
        return Qs::LargestStroke;

    xml->raiseError(QString("Unknown stroke width: '%1'").arg(text.toString()));
    return Qs::MediumStroke;
}

Qs::FillStyle SymbolReaderPrivate::toFillStyle(QStringRef text)
{
    if (text == "none")
        return Qs::NoFill;
    if (text == "solid")
        return Qs::SolidFill;
    if (text == "hor")
        return Qs::HorLineFill;
    if (text == "ver")
        return Qs::VerLineFill;
    if (text == "horver")
        return Qs::HorVerLineFill;
    if (text == "fw")
        return Qs::FwLineFill;
    if (text == "bw")
        return Qs::BwLineFill;
    if (text == "fwbw")
        return Qs::FwBwLineFill;

    xml->raiseError(QString("Unknown fill style: '%1'").arg(text.toString()));
    return Qs::NoFill;
}

Qs::EllipseStyle SymbolReaderPrivate::toEllipseStyle(QStringRef text)
{
    if (text == "full")
        return Qs::FullEllipsoid;
    if (text == "arc")
        return Qs::EllipsoidalArc;
    if (text == "pie")
        return Qs::EllipsoidalPie;
    if (text == "chord")
        return Qs::EllipsoidalChord;

    xml->raiseError(QString("Unknown ellipse style: '%1'").arg(text.toString()));
    return Qs::FullEllipsoid;
}

qreal SymbolReaderPrivate::toReal(QStringRef text)
{
    bool ok;
    qreal result = text.toDouble(&ok);
    if (!ok)
        xml->raiseError(QString("Malformed real number: '%1'").arg(text.toString()));
    return result;
}

QColor SymbolReaderPrivate::toColor(QStringRef text)
{
    if (QColor::isValidColor(text.toString()))
        return QColor(text.toString());

    xml->raiseError(QString("Malformed color: '%1'").arg(text.toString()));
    return QColor();
}

void SymbolReaderPrivate::skipCurrentElement()
{
    qWarning() << QString("Line %1, column %2: Skipping unknown element: %3")
                  .arg(xml->lineNumber()).arg(xml->columnNumber()).arg(xml->name().toString());
    xml->skipCurrentElement();
}

void SymbolReaderPrivate::skipAttribute(const QXmlStreamAttribute &attribute)
{
    qWarning() << QString("Line %1, column %2: Skipping unknown attribute: %3::%4")
                  .arg(xml->lineNumber()).arg(xml->columnNumber()).arg(xml->name().toString()).arg(attribute.name().toString());
}
