/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QDebug>
#include <QtCore/QHashFunctions>

class ObjectUid
{
    constexpr explicit ObjectUid(quint64 id) noexcept
        : m_value(id)
    {}
public:
    constexpr ObjectUid() noexcept
        : m_value(0)
    {}

    static ObjectUid createId() noexcept;

    constexpr bool isNull() const noexcept
    {
        return m_value == 0;
    }

    constexpr bool operator ==(ObjectUid other) const noexcept
    {
        return other.m_value == m_value;
    }

    constexpr bool operator !=(ObjectUid other) const noexcept
    {
        return !operator ==(other);
    }

    constexpr bool operator <(ObjectUid other) const noexcept
    {
        return m_value < other.m_value;
    }

    constexpr bool operator >(ObjectUid other) const noexcept
    {
        return m_value > other.m_value;
    }

    constexpr quint64 value() const noexcept
    {
        return m_value;
    }

    constexpr operator bool() const noexcept
    {
        return m_value != 0;
    }

private:
    quint64 m_value;
    friend QDataStream &operator<<(QDataStream &out, const ObjectUid &uid);
    friend QDataStream &operator>>(QDataStream &in, ObjectUid &uid);
};

Q_DECLARE_TYPEINFO(ObjectUid, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(ObjectUid)

QDebug operator<<(QDebug d, ObjectUid id);
QDataStream &operator<<(QDataStream &out, const ObjectUid &uid);
QDataStream &operator>>(QDataStream &in, ObjectUid &uid);

inline constexpr uint qHash(ObjectUid uid, uint seed = 0) noexcept
{
    using QT_PREPEND_NAMESPACE(qHash);
    return qHash(uid.value(), seed);
}

