/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QList>
#include <QtWidgets/QMainWindow>

namespace lib
{
    class Library;
    class Cell;
}

class ComponentMetadataForm;
class ComponentInterfaceForm;
class SourceCodeViewMultiForm;
class SymbolViewMultiForm;
class FileSystemNavigationWidget;
class LibraryContentNavigationWidget;

class QTabWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void openDocument(const QString &path);

private:
    QTabWidget *m_editor;
    ComponentMetadataForm *m_metadataForm;
    ComponentInterfaceForm *m_interfaceForm;
    SourceCodeViewMultiForm *m_sourceCodeViewMultiForm;
    SymbolViewMultiForm *m_symbolViewMultiForm;

    void setupLcNavigator();
    LibraryContentNavigationWidget *m_lcNavigationWidget;
    QDockWidget *m_lcNavigationDockWidget;

    void setupFsNavigator();
    FileSystemNavigationWidget *m_fsNavigationWidget;
    QDockWidget *m_fsNavigationDockWidget;

    void editLibrary(lib::Library *library);
    lib::Library *m_currentLibrary;
    void editCell(lib::Cell *cell);
    lib::Cell *m_currentCell;
};
