/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QMap>
#include <QtWidgets/QWidget>

namespace lib
{
    class SourceCodeView;
    class Cell;
    class Port;
    class Parameter;
}

class QComboBox;
class QLineEdit;
class QTextEdit;

class SourceCodeViewForm : public QWidget
{
    Q_OBJECT
public:
    explicit SourceCodeViewForm(QWidget *parent = 0);

    void setCell(lib::Cell *cell);
    lib::Cell *cell() const;
    void setView(lib::SourceCodeView *view);
    lib::SourceCodeView *view() const;

signals:

public slots:

private:
    lib::Cell *m_cell = nullptr;
    lib::SourceCodeView *m_view = nullptr;
    QLineEdit *m_captionLineEdit = nullptr;
    QLineEdit *m_descriptionLineEdit = nullptr;
    QComboBox *m_languageComboBox = nullptr;
    QTextEdit *m_codeEditor = nullptr;
    QMap<QString, QString> m_languageToCaption;
    QMap<QString, QString> m_captionToDefinition;
};
