/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ItemDelegates.h"
#include "ParameterTableModel.h"
#include "ParameterTableWidget.h"

#include "qlCore/LibraryObjects.h"

#include <QtCore/QStringListModel>
#include <QtCore/QSortFilterProxyModel>
#include <QtGui/QRegularExpressionValidator>
#include <QtWidgets/QCompleter>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

// TODO: filter completer with user history
ParameterTableWidget::ParameterTableWidget(QWidget *parent)
    : QWidget(parent)
    , m_sortFilterModel(new QSortFilterProxyModel(this))
    , m_model(new ParameterTableModel(this))
    , m_view(new QTableView())
    , m_dataTypeDelegate(new EnumItemDelegate(this))
    , m_unitDelegate(new StringItemDelegate(this))
    , m_unitListModel(new QStringListModel(this))
    , m_unitCompleter(new QCompleter(this))
    , m_unitValidator(new QRegularExpressionValidator(this))
    , m_nameDelegate(new StringItemDelegate(this))
    , m_nameListModel(new QStringListModel(this))
    , m_nameCompleter(new QCompleter(this))
    , m_nameValidator(new QRegularExpressionValidator(this))
    , m_valueDelegate(new StringItemDelegate(this))
    , m_filterLineEdit(new QLineEdit())
{
    m_model->setInvalidItemBackgroundBrush(QBrush(QColor("#dc322f")));

    m_sortFilterModel->setSourceModel(m_model);
    m_sortFilterModel->setFilterKeyColumn(-1); // All columns

    m_nameValidator->setRegularExpression(QRegularExpression("[a-zA-Z][a-zA-Z0-9]*",
                                                             QRegularExpression::OptimizeOnFirstUsageOption));
    m_nameCompleter->setModel(m_nameListModel);
    m_nameCompleter->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    m_nameCompleter->setFilterMode(Qt::MatchContains);
    m_nameDelegate->setCompleter(m_nameCompleter);
    m_nameDelegate->setValidator(m_nameValidator);
    m_model->setNameValidator(m_nameValidator);
    m_view->setItemDelegateForColumn(ParameterTableModel::NameSection, m_nameDelegate);

    m_dataTypeDelegate->addEnum(lib::Parameter::BooleanData, "Boolean");
    m_dataTypeDelegate->addEnum(lib::Parameter::IntegerData, "Integer");
    m_dataTypeDelegate->addEnum(lib::Parameter::FloatData, "Float");
    m_dataTypeDelegate->addEnum(lib::Parameter::EnumData, "Choice");
    m_dataTypeDelegate->addEnum(lib::Parameter::SingleLineTextData, "String");
    m_dataTypeDelegate->addEnum(lib::Parameter::MultiLineTextData, "Text");
    m_view->setItemDelegateForColumn(ParameterTableModel::DataTypeSection, m_dataTypeDelegate);

    m_unitCompleter->setModel(m_unitListModel);
    m_unitCompleter->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    m_unitCompleter->setFilterMode(Qt::MatchContains);
    m_unitCompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    m_unitDelegate->setCompleter(m_unitCompleter);
    m_unitDelegate->setValidator(m_unitValidator);
    m_model->setUnitValidator(m_unitValidator);
    m_view->setItemDelegateForColumn(ParameterTableModel::UnitSection, m_unitDelegate);

    m_view->setItemDelegateForColumn(ParameterTableModel::DefaultValueSection, m_valueDelegate);

    // FIXME: load from a known unit file, and a user history file
    m_unitListModel->setStringList(QStringList()
                                   << "°"
                                   << "%"
                                   << "%/℃"
                                   << "℃"
                                   << "1/℃"
                                   << "1/cm²"
                                   << "1/cm³"
                                   << "1/K"
                                   << "1/K²"
                                   << "1/m"
                                   << "1/V"
                                   << "1/W"
                                   << "A"
                                   << "A/℃"
                                   << "A²/Hz"
                                   << "A².s"
                                   << "A/cm²"
                                   << "A/m²"
                                   << "A/µm²"
                                   << "A.s/m²"
                                   << "A/V²"
                                   << "A/W"
                                   << "C"
                                   << "cm²/V.s"
                                   << "℃"
                                   << "dB"
                                   << "dB.m"
                                   << "eV"
                                   << "F"
                                   << "F/cm²"
                                   << "F/m"
                                   << "F/m²"
                                   << "F/µm²"
                                   << "H"
                                   << "H/m"
                                   << "Hz"
                                   << "K"
                                   << "K/W"
                                   << "m"
                                   << "M^(1-AF)"
                                   << "m²"
                                   << "nm"
                                   << "ppm/℃"
                                   << "s"
                                   << "S"
                                   << "S/m"
                                   << "V"
                                   << "V/℃"
                                   << "V²/Hz"
                                   << "V/K"
                                   << "√(V).m"
                                   << "V/m"
                                   << "V.m"
                                   << "V/s"
                                   << "W.s"
                                   << "Ws/K"
                                   << "Ω"
                                   << "Ω/m"
                                   << "Ω/sq");

    m_view->setModel(m_sortFilterModel);
    m_view->setSortingEnabled(true);
    m_view->horizontalHeader()->setStretchLastSection(true);

    m_filterLineEdit->setPlaceholderText("Filter..."); // FIXME: user history file
    connect(m_filterLineEdit, &QLineEdit::textChanged,
            m_sortFilterModel, &QSortFilterProxyModel::setFilterWildcard);

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_filterLineEdit);
    layout()->addWidget(m_view);
}

ParameterTableWidget::~ParameterTableWidget()
{

}

void ParameterTableWidget::setParameters(const QList<lib::Parameter *> &parameters)
{
    m_model->setParameterList(parameters);
}

QList<lib::Parameter *> ParameterTableWidget::parameters() const
{
    return m_model->parameterList();
}
