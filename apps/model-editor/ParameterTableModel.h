/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QAbstractTableModel>
#include <QtCore/QList>

#include <QtGui/QBrush>

#include "qlCore/LibraryObjects.h" // for enum
//class ParameterDefinition;

class QValidator;

class ParameterTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum {
        NameSection = 0,
        DataTypeSection,
        DefaultValueSection,
        UnitSection,
        VisibleSection,
        DocumentationSection,
        RestrictionSection,
        SectionCount
    };

    explicit ParameterTableModel(QObject *parent = 0);
    ~ParameterTableModel();

    void setParameterList(const QList<lib::Parameter *> &list);
    QList<lib::Parameter *> parameterList() const;

    void setNameValidator(QValidator *validator);
    QValidator *nameValidator() const;
    void setUnitValidator(QValidator *validator);
    QValidator *unitValidator() const;
    void setIntValueValidator(QValidator *validator);
    QValidator *intValueValidator() const;
    void setFloatValueValidator(QValidator *validator);
    QValidator *floatValueValidator() const;

    void setInvalidItemBackgroundBrush(const QBrush &brush);
    QBrush invalidItemBackgroundBrush() const;

private:
    static QString toString(lib::Parameter::DataType dataType);
    bool isValidName(const QString &name) const;
    bool isValidUnit(const QString &unit) const;
    bool isValidDefaultValue(lib::Parameter::DataType dataType, const QString &value) const;
    bool isValidRestriction(lib::Parameter *definition, const QString &value) const;
    QList<lib::Parameter *> m_parameters;
    QValidator *m_nameValidator = nullptr;
    QValidator *m_unitValidator = nullptr;
    QValidator *m_intValueValidator = nullptr;
    QValidator *m_floatValueValidator = nullptr;
    QBrush m_invalidItemBackgroundBrush;

public: //QTableModel interface
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

};
