/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsTool.h"
#include "GraphicsToolPropertyWidget.h"

#include "qlGui/ObjectPropertyBrowser.h"

#include <QtWidgets/QVBoxLayout>

GraphicsToolPropertyWidget::GraphicsToolPropertyWidget(QWidget *parent)
    : QWidget(parent)
    , m_propertyBrowser(new ObjectPropertyBrowser)
{
    setLayout(new QVBoxLayout);
    layout()->addWidget(m_propertyBrowser);
}

void GraphicsToolPropertyWidget::setTool(GraphicsTool *tool)
{
    if (m_tool == tool)
        return;
    m_tool = tool;
    m_propertyBrowser->setObject(m_tool, m_tool);
}

GraphicsTool *GraphicsToolPropertyWidget::tool() const
{
    return m_tool;
}
