#include "ComponentRepresentationMultiForm.h"
#include "GraphicsWidget.h"

#include "qlCore/LibraryObjects.h"

#include <QtCore/QStringListModel>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QListView>
#include <QtWidgets/QStackedWidget>

SymbolViewMultiForm::SymbolViewMultiForm(QWidget *parent)
    : QWidget(parent)
    , m_listModel(new QStringListModel())
    , m_listView(new QListView())
    , m_stackedWidget(new QStackedWidget())
{
    m_listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_listView->setModel(m_listModel);
    auto hBoxLayout = new QHBoxLayout();
    hBoxLayout->addWidget(m_listView);
    hBoxLayout->addWidget(m_stackedWidget);
    hBoxLayout->setStretch(1, 5);
    setLayout(hBoxLayout);
    connect(m_listView, &QListView::activated,
            this, [this]() {
        auto name = m_listModel->data(m_listView->currentIndex(), Qt::EditRole).toString();
        m_stackedWidget->setCurrentWidget(m_nameToGraphicsWidget.value(name));
    });
}

void SymbolViewMultiForm::setViews(const QList<lib::SymbolView *> &views)
{
    while (m_stackedWidget->count() > 0)
        delete m_stackedWidget->widget(0);

    m_symbolViewList = views;

    m_nameToGraphicsWidget.clear();
    for (auto symbolView: m_symbolViewList) {
        auto name = symbolView->identification.caption; //QString("Symbol#%1").arg(++i);
        auto view = new GraphicsWidget();
        auto objects = symbolView->drawings;
        view->setGraphicsObjects(objects);
        m_stackedWidget->addWidget(view);
        Q_ASSERT(!m_nameToGraphicsWidget.contains(name));
        m_nameToGraphicsWidget.insert(name, view);
    }

    m_listModel->setStringList(m_nameToGraphicsWidget.keys());
    m_listView->setCurrentIndex(m_listModel->index(0));
}

QList<lib::SymbolView *> SymbolViewMultiForm::representations() const
{
    return m_symbolViewList;
}
