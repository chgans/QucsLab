/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QScrollArea>

namespace lib
{
    class SourceCodeView;
    class Parameter;
    class Port;
    class View;
    class Cell;
}

class SourceCodeViewForm;

class QGroupBox;
class QBoxLayout;

class SourceCodeViewMultiForm : public QScrollArea
{
    Q_OBJECT
public:
    explicit SourceCodeViewMultiForm(QWidget *parent = 0);

    void setCell(lib::Cell *cell);
    lib::Cell *cell() const;
    void setViews(QList<lib::SourceCodeView *> views);
    QList<lib::SourceCodeView *> views() const;

signals:

public slots:

private:
    lib::Cell *m_cell = nullptr;
    QList<lib::SourceCodeView *> m_views;
    QList<QGroupBox *> m_groupBoxes;
    QBoxLayout *m_layout;
};
