/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/LibraryObjects.h"

#include <QtWidgets/QWidget>

namespace Ui {
class ComponentMetadataForm;
}

class ComponentMetadataForm : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(lib::Identification identification READ identification WRITE setIdentification NOTIFY identificationChanged)
    Q_PROPERTY(lib::Attribution attribution READ attribution WRITE setAttribution NOTIFY attributionChanged)
    Q_PROPERTY(lib::Documentation documentation READ documentation WRITE setDocumentation NOTIFY documentationChanged)

public:
    explicit ComponentMetadataForm(QWidget *parent = 0);
    ~ComponentMetadataForm();

    lib::Identification identification() const;
    lib::Attribution attribution() const;
    lib::Documentation documentation() const;

public slots:
    void setIdentification(lib::Identification identification);
    void setAttribution(lib::Attribution attribution);
    void setDocumentation(lib::Documentation documentation);

signals:
    void identificationChanged();
    void attributionChanged();
    void documentationChanged();

private:
    Ui::ComponentMetadataForm *ui;
    lib::Identification m_identification;
    lib::Attribution m_attribution;
    lib::Documentation m_documentation;
};
