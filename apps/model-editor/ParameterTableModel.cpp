/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ParameterTableModel.h"

#include "qlCore/LibraryObjects.h"

#include <QtGui/QValidator>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCompleter>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLineEdit>


ParameterTableModel::ParameterTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

ParameterTableModel::~ParameterTableModel()
{

}

void ParameterTableModel::setParameterList(const QList<lib::Parameter *> &list)
{
    beginResetModel();
    m_parameters = list;
    endResetModel();
}

QList<lib::Parameter *> ParameterTableModel::parameterList() const
{
    return m_parameters;
}

void ParameterTableModel::setNameValidator(QValidator *validator)
{
    m_nameValidator = validator;
}

QValidator *ParameterTableModel::nameValidator() const
{
    return m_nameValidator;
}

void ParameterTableModel::setUnitValidator(QValidator *validator)
{
    m_unitValidator = validator;
}

QValidator *ParameterTableModel::unitValidator() const
{
    return m_unitValidator;
}

void ParameterTableModel::setIntValueValidator(QValidator *validator)
{
    m_intValueValidator = validator;
}

QValidator *ParameterTableModel::intValueValidator() const
{
    return m_intValueValidator;
}

void ParameterTableModel::setFloatValueValidator(QValidator *validator)
{
    m_floatValueValidator = validator;
}

QValidator *ParameterTableModel::floatValueValidator() const
{
    return m_floatValueValidator;
}

void ParameterTableModel::setInvalidItemBackgroundBrush(const QBrush &brush)
{
    m_invalidItemBackgroundBrush = brush;
}

QBrush ParameterTableModel::invalidItemBackgroundBrush() const
{
    return m_invalidItemBackgroundBrush;
}

QVariant ParameterTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Horizontal)
        return QVariant();

    Q_ASSERT(section >= 0 && section < SectionCount);

    switch (section) {
        case NameSection:
            return "Name";
        case DataTypeSection:
            return "Type";
        case DefaultValueSection:
            return "Default";
        case UnitSection:
            return "Unit";
        case VisibleSection:
            return "Visible";
        case DocumentationSection:
            return "Documentation";
        case RestrictionSection:
            return "Restriction";
        default:
            Q_UNREACHABLE();
            return QVariant();
    }
}


int ParameterTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_parameters.count();
}

int ParameterTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return SectionCount;
}

QVariant ParameterTableModel::data(const QModelIndex &modelIndex, int role) const
{
    if (!modelIndex.isValid())
        return QVariant();

    const int section = modelIndex.column();
    const int row = modelIndex.row();
    Q_ASSERT(row >= 0 && row < m_parameters.count());
    Q_ASSERT(section >= 0 && section < SectionCount);

    const auto param = m_parameters.value(row);
    if (role == Qt::DisplayRole) {
        switch (section) {
            case NameSection:
                return param->name();
            case DataTypeSection:
                return toString(param->dataType());
            case DefaultValueSection:
                switch (param->dataType()) {
                    case lib::Parameter::BooleanData:
                        return param->defaultValue() == "true";
                    case lib::Parameter::IntegerData:
                        return param->defaultValue().toInt();
                    case lib::Parameter::FloatData:
                        return param->defaultValue().toDouble();
                    case lib::Parameter::EnumData: // FIXME: combobox
                        return param->defaultValue();
                    case lib::Parameter::SingleLineTextData:
                        return param->defaultValue();
                    case lib::Parameter::MultiLineTextData:
                        return param->defaultValue(); // FIXME: special editor
                    default:
                        Q_UNREACHABLE();
                        return QVariant();
                }
            case UnitSection:
                return param->unit();
            case VisibleSection:
                return param->visible() ? "Yes" : "No";
            case DocumentationSection:
                return param->documentation();
            case RestrictionSection:
                return param->restriction();
            default:
                Q_UNREACHABLE();
                return QVariant();
        }
    }
    if (role == Qt::UserRole) { // FIXME: Don't use UserRole, define a EnumItemRole
        switch (section) {
            case DefaultValueSection:
                switch (param->dataType()) {
                    case lib::Parameter::BooleanData:
                    case lib::Parameter::IntegerData:
                    case lib::Parameter::FloatData:
                    case lib::Parameter::SingleLineTextData:
                    case lib::Parameter::MultiLineTextData:
                        return QVariant();
                    case lib::Parameter::EnumData:
                        return param->restriction().remove("(").remove(")").split("|");
                    default:
                        Q_UNREACHABLE();
                        return QVariant();
                }
                break;
            case NameSection:
            case DataTypeSection:
            case UnitSection:
            case VisibleSection:
            case DocumentationSection:
            case RestrictionSection:
                return QVariant();
            default:
                Q_UNREACHABLE();
                return QVariant();
        }
    }
    if (role == Qt::EditRole) {
        switch (section) {
            case NameSection:
                return param->name();
            case DataTypeSection:
                return param->dataType();
            case DefaultValueSection:
                switch (param->dataType()) {
                    case lib::Parameter::BooleanData:
                        return param->defaultValue() == "true";
                    case lib::Parameter::IntegerData:
                        return param->defaultValue().toInt();
                    case lib::Parameter::FloatData:
                        return param->defaultValue().toDouble();
                    case lib::Parameter::EnumData: // FIXME: combobox
                        return param->defaultValue();
                    case lib::Parameter::SingleLineTextData:
                        return param->defaultValue();
                    case lib::Parameter::MultiLineTextData:
                        return param->defaultValue(); // FIXME: special editor
                    default:
                        Q_UNREACHABLE();
                        return QVariant();
                }
                break;
            case UnitSection:
                return param->unit();
            case VisibleSection:
                return QVariant();
            case DocumentationSection:
                return param->documentation();
            case RestrictionSection:
                return param->restriction();
            default:
                Q_UNREACHABLE();
                return QVariant();
        }
    }
    if (role == Qt::CheckStateRole) {
        switch (section) {
            case VisibleSection:
                return param->visible() ? Qt::Checked : Qt::Unchecked;
            default:
                return QVariant();
        }
    }
    if (role == Qt::BackgroundRole) {
        bool isValid = false;
        switch (section) {
            case NameSection:
                isValid = isValidName(param->name());
                break;
            case DefaultValueSection:
                isValid = isValidDefaultValue(param->dataType(), param->defaultValue());
                break;
            case UnitSection:
                isValid = isValidUnit(param->unit());
                break;
            case RestrictionSection:
                isValid = isValidRestriction(param, param->restriction());
            default:
                return QVariant();
        }
        return isValid ? QVariant() : m_invalidItemBackgroundBrush;
    }
    return QVariant();
}

bool ParameterTableModel::setData(const QModelIndex &modelIndex, const QVariant &value, int role)
{
    if (data(modelIndex, role) == value)
        return false;

    const int section = modelIndex.column();
    const int row = modelIndex.row();
    Q_ASSERT(row >= 0 && row < m_parameters.count());
    Q_ASSERT(section >= 0 && section < SectionCount);

    const auto param = m_parameters.value(row);

    if (role == Qt::EditRole) {
        switch (section) {
            case NameSection:
                param->setName(value.toString());
                break;
            case DataTypeSection:
                param->setDataType(value.value<lib::Parameter::DataType>());
                break;
            case DefaultValueSection:
                param->setDefaultValue(value.toString());
                break;
            case UnitSection:
                param->setUnit(value.toString());
                break;
            case VisibleSection:
                // Not editable, see CheckStateRole
                Q_UNREACHABLE();
                return false;
            case DocumentationSection:
                param->setDocumentation(value.toString());
                break;
            case RestrictionSection:
                param->setRestriction(value.toString());
                break;
            default:
                Q_UNREACHABLE();
                return false;
        }
    }

    if (role == Qt::CheckStateRole) {
        switch (section) {
            case VisibleSection:
                param->setVisible(value.value<Qt::CheckState>() == Qt::Checked);
                emit dataChanged(modelIndex, modelIndex, QVector<int>() << role);
                break;
            default:
                Q_UNREACHABLE();
                return false;
        }
    }

    // Invalidate the whole line
    emit dataChanged(index(modelIndex.row(), 0, modelIndex.parent()),
                     index(modelIndex.row(), SectionCount, modelIndex.parent()),
                     QVector<int>() << Qt::DisplayRole
                     << Qt::EditRole << Qt::BackgroundRole
                     << Qt::CheckStateRole);
    return true;
}

Qt::ItemFlags ParameterTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    Qt::ItemFlags flags = Qt::ItemIsEnabled;
    if (index.column() == VisibleSection)
        flags |= Qt::ItemIsUserCheckable;
    else
        flags |= Qt::ItemIsEditable;
    return flags;
}

bool ParameterTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
    return false;
}

bool ParameterTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
    return false;
}

QString ParameterTableModel::toString(lib::Parameter::DataType dataType)
{
    switch (dataType) {
        case lib::Parameter::BooleanData:
            return "Boolean";
        case lib::Parameter::IntegerData:
            return "Integer";
        case lib::Parameter::FloatData:
            return "Float";
        case lib::Parameter::EnumData:
            return "Enumeration";
        case lib::Parameter::SingleLineTextData:
            return "String";
        case lib::Parameter::MultiLineTextData:
            return "Text";
        default:
            Q_UNREACHABLE();
            return QString();
    }
}

bool ParameterTableModel::isValidName(const QString &name) const
{
    if (m_nameValidator == nullptr)
        return true;
    QString data = name;
    int pos = 0;
    return m_nameValidator->validate(data, pos) == QValidator::Acceptable;
}

bool ParameterTableModel::isValidUnit(const QString &unit) const
{
    if (m_unitValidator == nullptr)
        return true;
    QString data = unit;
    int pos = 0;
    return m_unitValidator->validate(data, pos) == QValidator::Acceptable;
}

bool ParameterTableModel::isValidDefaultValue(lib::Parameter::DataType dataType, const QString &value) const
{
    QString data = value;
    int pos = 0;
    switch (dataType) {
        case lib::Parameter::BooleanData:
            return true;
        case lib::Parameter::IntegerData:
            if (m_intValueValidator == nullptr)
                return true;
            return m_intValueValidator->validate(data, pos) == QValidator::Acceptable;
        case lib::Parameter::FloatData:
            if (m_floatValueValidator == nullptr)
                return true;
            return m_floatValueValidator->validate(data, pos) == QValidator::Acceptable;
        case lib::Parameter::EnumData:
            return true; // FIXME
        case lib::Parameter::SingleLineTextData:
            return true; // Check for frobidden char? eg. \n, ...
        case lib::Parameter::MultiLineTextData:
            return true;
        default:
            Q_UNREACHABLE();
            return false;
    }
}

// Enumeration: (Abc|Def|Ghi) or [1-4]
// Boolean: (True|False)
bool ParameterTableModel::isValidRestriction(lib::Parameter *definition, const QString &value) const
{
    switch (definition->dataType()) {
        case lib::Parameter::BooleanData:
            return value.isEmpty();
        case lib::Parameter::IntegerData:
            return value.isEmpty();
        case lib::Parameter::FloatData:
            return value.isEmpty();
        case lib::Parameter::EnumData:
            return true; // FIXME
        case lib::Parameter::SingleLineTextData:
            return value.isEmpty();
        case lib::Parameter::MultiLineTextData:
            return value.isEmpty();
        default:
            Q_UNREACHABLE();
            return false;
    }
}
