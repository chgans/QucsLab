/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ComponentImplementationForm.h"

#include "qlCore/LibraryObjects.h"

#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QTextEdit>

SourceCodeViewForm::SourceCodeViewForm(QWidget *parent)
    : QWidget(parent)
    , m_captionLineEdit(new QLineEdit())
    , m_descriptionLineEdit(new QLineEdit())
    , m_languageComboBox(new QComboBox())
    , m_codeEditor(new QTextEdit())
{
    auto formLayout = new QFormLayout();
    setLayout(formLayout);
    formLayout->addRow(new QLabel("Caption"), m_captionLineEdit);
    formLayout->addRow(new QLabel("Description"), m_descriptionLineEdit);
    formLayout->addRow(new QLabel("Language"), m_languageComboBox);
    formLayout->addRow(new QLabel("Code"), m_codeEditor);

    m_codeEditor->setSizePolicy(QSizePolicy::MinimumExpanding,
                                QSizePolicy::MinimumExpanding);

    m_languageToCaption.insert("other", "Other");
    m_captionToDefinition.insert("Other", "");
    m_languageToCaption.insert("spice", "Spice");
    m_captionToDefinition.insert("Spice", "Spice");
    m_languageToCaption.insert("qucs", "Qucs");
    m_captionToDefinition.insert("Qucs", "Spice");
    m_languageToCaption.insert("vhdl", "Vhdl");
    m_captionToDefinition.insert("Vhdl", "VHDL");
    m_languageToCaption.insert("verilog", "Verilog");
    m_captionToDefinition.insert("Verilog", "Verilog");
    m_languageToCaption.insert("veriloga", "Verilog-A");
    m_captionToDefinition.insert("Verilog-A", "Verilog");

    m_languageComboBox->addItems(m_captionToDefinition.keys());
}

void SourceCodeViewForm::setCell(lib::Cell *cell)
{
    m_cell = cell;
}

lib::Cell *SourceCodeViewForm::cell() const
{
    return m_cell;
}

void SourceCodeViewForm::setView(lib::SourceCodeView *view)
{
    m_view = view;

    if (m_view == nullptr) {
        m_captionLineEdit->clear();
        m_descriptionLineEdit->clear();
        m_codeEditor->clear();
        return;
    }

    m_captionLineEdit->setText(view->identification.caption);
    m_descriptionLineEdit->setText(view->identification.description);
    m_codeEditor->setPlainText(view->code);

    auto lang = view->language.toLower();
    if (m_languageToCaption.contains(lang)) {
        auto caption = m_languageToCaption.value(lang);
        m_languageComboBox->setCurrentText(caption);
    }
    else
        m_languageComboBox->setCurrentText("Other");

    // Generate completion
    QStringList completion;
    for (auto port: m_cell->ports)
        completion.append(QString("${%1}").arg(port->name()));
    for (auto param: m_cell->parameters)
        completion.append(QString("${%1}").arg(param->name()));
}

lib::SourceCodeView *SourceCodeViewForm::view() const
{
    return m_view;
}
