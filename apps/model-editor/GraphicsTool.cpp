/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsTool.h"

#include "qlGui/GraphicsItem.h"
#include "qlGui/GraphicsScene.h"
#include "qlGui/GraphicsView.h"
#include "qlCore/SymbolCommand.h"

#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtGui/QIcon>
#include <QtGui/QKeySequence>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QGraphicsPathItem>
#include <QtWidgets/QRubberBand>

GraphicsTool::GraphicsTool(QObject *parent)
    : QObject(parent)
    , m_command(nullptr)
{

}

GraphicsTool::~GraphicsTool()
{
    delete m_command;
}

bool GraphicsTool::hasUndoCommand() const
{
    return m_command != nullptr;
}

UndoCommand *GraphicsTool::takeUndoCommand()
{
    Q_ASSERT(m_command != nullptr);
    UndoCommand *result = m_command;
    m_command = nullptr;
    return result;
}

void GraphicsTool::setUndoCommand(UndoCommand *command)
{
    delete m_command;
    m_command = command;
    emit undoCommandAvailable();
}

QMap<QString, QVariant> GraphicsTool::objectProperties(const QGraphicsItem *item)
{
    QMap<QString, QVariant> result;
    result.insert("location", item->pos());
    result.insert("rotation", item->rotation());
    result.insert("mirrored", false);
    result.insert("visible", true);
    result.insert("locked", false);
    return result;
}

QMap<QString, QVariant> GraphicsTool::strokeProperties(const QPen &pen)
{
    QMap<QString, QVariant> result;

    Qs::StrokeStyle strokeStyle = Qs::SolidStroke;
    switch (pen.style()) {
        case Qt::NoPen:
            strokeStyle = Qs::NoStroke;
            break;
        case Qt::SolidLine:
            strokeStyle = Qs::SolidStroke;
            break;
        case Qt::DashLine:
            strokeStyle = Qs::DashStroke;
            break;
        case Qt::DotLine:
            strokeStyle = Qs::DotStroke;
            break;
        case Qt::DashDotLine:
            strokeStyle = Qs::DashDotStroke;
            break;
        case Qt::DashDotDotLine:
            strokeStyle = Qs::DasDotDotStroke;
            break;
        default:
            break;
    }

    // FIXME: Hard coded, see GraphicsWidget::createPen()
    Qs::StrokeWidth strokeWidth = Qs::SmallestStroke;
    if (pen.widthF() >= 2.5)
        strokeWidth = Qs::LargestStroke;
    else if (pen.widthF() >= 1.0)
        strokeWidth = Qs::LargeStroke;
    else if (pen.widthF() >= 0.5)
        strokeWidth = Qs::MediumStroke;
    else if (pen.widthF() >= 0.25)
        strokeWidth = Qs::SmallStroke;

    result.insert("strokeWidth", strokeWidth);
    result.insert("strokeStyle", strokeStyle);
    result.insert("strokeColor", pen.color());

    return result;
}

QMap<QString, QVariant> GraphicsTool::fillProperties(const QBrush &brush)
{
    QMap<QString, QVariant> result;

    Qs::FillStyle fillStyle = Qs::NoFill;
    switch (brush.style()) {
        case Qt::NoBrush:
            fillStyle = Qs::NoFill;
            break;
        case Qt::SolidPattern:
            fillStyle = Qs::SolidFill;
            break;
        case Qt::HorPattern:
            fillStyle = Qs::HorLineFill;
            break;
        case Qt::VerPattern:
            fillStyle = Qs::VerLineFill;
            break;
        case Qt::CrossPattern:
            fillStyle = Qs::HorVerLineFill;
            break;
        case Qt::BDiagPattern:
            fillStyle = Qs::BwLineFill;
            break;
        case Qt::FDiagPattern:
            fillStyle = Qs::FwLineFill;
            break;
        case Qt::DiagCrossPattern:
            fillStyle = Qs::FwBwLineFill;
            break;
        default:
            break;
    }

    result.insert("fillStyle", fillStyle);
    result.insert("fillColor", brush.color());

    return result;
}

// Rubber band selection
// Click selection (Shift add to selection, ctrl remove from selection)
// Drag selection or item under cursor
// FIXME: Need to enable multiple selection at document level
// TBD: Don't handle move here. To move selection or item under cursor, use 'm' (move tool)

GraphicsSelectTool::GraphicsSelectTool(QObject *parent)
    : GraphicsTool(parent)
    , m_rubberBand(new QRubberBand(QRubberBand::Rectangle))
{
    m_rubberBand->setWindowOpacity(0.3);
    // FIXME: KDE/Plasma want's to animate the resizing which gives bery poor UX (sluggish)
}

GraphicsSelectTool::~GraphicsSelectTool()
{

}

bool GraphicsSelectTool::canSelect(const QPointF &pos)
{
    auto item = m_scene->itemAt(pos, QTransform());
    // A graphics item that is a document object surrogate and which is not locked
    return item != nullptr &&
            item->data(GraphicsScene::DocumentIdProperty).canConvert<ObjectUid>() &&
            item->isEnabled();
}

void GraphicsSelectTool::maybeAddToOrClearSelection(const QPointF &pos)
{
    if (!canSelect(pos))
        m_scene->clearSelection();
    else
        m_scene->itemAt(pos, QTransform())->setSelected(true);
}

void GraphicsSelectTool::maybeRemoveFromOrClearSelection(const QPointF &pos)
{
    if (!canSelect(pos))
        m_scene->clearSelection();
    else
        m_scene->itemAt(pos, QTransform())->setSelected(false);
}

void GraphicsSelectTool::maybeSetOrClearSelection(const QPointF &pos)
{
    m_scene->clearSelection();
    if (canSelect(pos))
        m_scene->itemAt(pos, QTransform())->setSelected(true);
}

bool GraphicsSelectTool::canStartDrag(const QPointF &pos)
{
    return m_buttonState == ButtonDown &&
            QLineF(m_view->mapFromScene(m_buttonDownPos),
                   m_view->mapFromScene(pos)).length() > m_dragThreshold;
}

void GraphicsSelectTool::startRubberBandDrag(const QPointF &pos)
{
    setState(RubberBandSelect);
    m_rubberBand->setGeometry(QRect(m_view->mapToGlobal(m_view->mapFromScene(m_buttonDownPos)),
                                     m_view->mapToGlobal(m_view->mapFromScene(pos))));
    m_rubberBand->show();
}

void GraphicsSelectTool::updateRubberBand(const QPointF &pos)
{
    QPainterPath path;
    path.addRect(QRectF(m_buttonDownPos, pos).normalized());
    m_scene->setSelectionArea(path);
    m_rubberBand->setGeometry(QRect(m_view->mapToGlobal(m_view->mapFromScene(m_buttonDownPos)),
                                     m_view->mapToGlobal(m_view->mapFromScene(pos))).normalized());
}

void GraphicsSelectTool::endRubberBand(const QPointF &pos)
{
    Q_UNUSED(pos);
    setState(ClickSelect);
    m_rubberBand->hide();
}

// TBD: Do we want cursor-based move operation?
// TBD: Or we only support move through the 'm' command?
void GraphicsSelectTool::startSelectionDrag(const QPointF &pos)
{
    Q_UNUSED(pos);
    setState(DragSelect);
}

void GraphicsSelectTool::updateDragSelect(const QPointF &pos)
{
    qDebug() << __FUNCTION__ << m_buttonDownPos << pos;
}

void GraphicsSelectTool::endDragSelect(const QPointF &pos)
{
    Q_UNUSED(pos);
    setState(ClickSelect);
}

void GraphicsSelectTool::setState(GraphicsSelectTool::State state)
{
    qDebug() << m_state << "=>" << state;
    m_state = state;
}

QString GraphicsSelectTool::caption() const
{
    return QStringLiteral("Select tool");
}

QString GraphicsSelectTool::description() const
{
    return QStringLiteral("Select and manipulate objects");
}

QKeySequence GraphicsSelectTool::shortcut() const
{
    return QKeySequence(Qt::Key_Escape);
}

QIcon GraphicsSelectTool::icon() const
{
    return QIcon::fromTheme("edit-select");
}

bool GraphicsSelectTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event)

    if (sourceEvent->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsSelectTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event)

    if (sourceEvent->button() != Qt::LeftButton)
        return false;

    m_buttonState = ButtonDown;
    m_buttonDownPos = sourceEvent->scenePos();
    return true;
}

bool GraphicsSelectTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event)

    if (m_buttonState != ButtonDown)
        return true;

    const auto pos = sourceEvent->scenePos();

    if (m_state == ClickSelect) {
        if (!canStartDrag(pos))
            return true;

        if (canSelect(m_buttonDownPos))
            startSelectionDrag(m_buttonDownPos);
        else
            startRubberBandDrag(m_buttonDownPos);
    }

    if (m_state == DragSelect) {
        updateDragSelect(pos);
        return true;
    }

    if (m_state == RubberBandSelect) {
        updateRubberBand(pos);
        return true;
    }

    Q_UNREACHABLE();
    return true;
}

bool GraphicsSelectTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);

    if (sourceEvent->button() != Qt::LeftButton)
        return false;

    m_buttonState = ButtonUp;

    const auto pos = sourceEvent->scenePos();
    switch (m_state) {
        case GraphicsSelectTool::ClickSelect:
            if (sourceEvent->modifiers() & Qt::ShiftModifier)
                maybeAddToOrClearSelection(pos);
            else if (sourceEvent->modifiers() & Qt::ControlModifier)
                maybeRemoveFromOrClearSelection(pos);
            else
                maybeSetOrClearSelection(pos);
            break;
        case GraphicsSelectTool::DragSelect:
            endDragSelect(pos);
            break;
        case GraphicsSelectTool::RubberBandSelect:
            endRubberBand(pos);
            break;
    }
    return true;
}

void GraphicsSelectTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(pos);
    Q_UNUSED(sourcePos);

    m_view = view;
    m_scene = m_view->graphicsScene();
    m_state = ClickSelect;
    m_view->graphicsScene()->installEventHandler(this);
    //m_rubberBand->setParent();
}

void GraphicsSelectTool::desactivate()
{
    //m_rubberBand->setParent(nullptr);
    m_view->graphicsScene()->uninstallEventHandler(this);
}


GraphicsMoveTool::GraphicsMoveTool(QObject *parent)
    : GraphicsTool(parent)
{

}

GraphicsMoveTool::~GraphicsMoveTool()
{

}

void GraphicsMoveTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    m_view = view;
    m_view->graphicsScene()->installEventHandler(this);

    m_state = WaitForPick;
    if (checkForPick(sourcePos))
        pick(pos);
}

void GraphicsMoveTool::desactivate()
{
    if (hasPicked())
        unpick();
    m_view->setCursor(Qt::ArrowCursor);
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString GraphicsMoveTool::caption() const
{
    return "Move tool";
}

QString GraphicsMoveTool::description() const
{
    return "Move objects around";
}

QKeySequence GraphicsMoveTool::shortcut() const
{
    return QKeySequence("m");
}

QIcon GraphicsMoveTool::icon() const
{
    return QIcon::fromTheme("transform-move");
}

bool GraphicsMoveTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsMoveTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsMoveTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    const auto pos = sourceEvent->scenePos();
    const auto snappedPos = event->scenePos();
    switch (m_state) {
        case WaitForPick:
            checkForPick(pos);
            break;
        case WaitForPlace:
            move(snappedPos);
            break;
    }
    return true;
}

bool GraphicsMoveTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    const auto pos = sourceEvent->scenePos();
    const auto snappedPos = event->scenePos();
    switch (m_state) {
        case WaitForPick:
            if (checkForPick(pos))
                pick(snappedPos);
            break;
        case WaitForPlace:
            place(snappedPos);
            // TBD: auto-repeat vs single shoot
            //checkForPick(pos);
            emit finished();
            break;
    }
    return true;
}

// FIXME: simplify m_items vs m_uids
// We could create a group on the fly, apply a graphical effect
// to the group and destroy the group at the end
bool GraphicsMoveTool::checkForPick(const QPointF &pos)
{
    QList<QGraphicsItem *> items; // FIXME: m_uids vs m_items
    if (!m_view->graphicsScene()->selectedItems().isEmpty())
        items = m_view->graphicsScene()->selectedItems();
    else
        items = m_view->graphicsScene()->items(pos);

    m_uids.clear();
    m_items.clear();
    for (int i=0; i<items.count(); i++) {
        auto const uid = items.at(i)->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
        if (!uid.isNull()) {
            m_uids.append(uid);
            m_items.append(items.at(i));
        }
    }

    if (!m_uids.isEmpty())
        m_view->setCursor(Qt::CrossCursor);
    else
        m_view->setCursor(Qt::ArrowCursor);

    return !m_uids.isEmpty();
}

// FIXME: simplify m_originalItemPositions vs m_deltaPickPositions
// Need only one or the other
void GraphicsMoveTool::pick(const QPointF &pos)
{
    m_originalItemPositions.clear();
    m_deltaPickPositions.clear();
    for (const auto item: m_items) {
        m_originalItemPositions << item->pos();
        m_deltaPickPositions << pos - item->pos();
        item->setOpacity(0.5); // FIXME
    }
    m_state = WaitForPlace;
    m_view->setCursor(Qt::SizeAllCursor);
}

bool GraphicsMoveTool::hasPicked()
{
    return !m_uids.isEmpty();
}

void GraphicsMoveTool::unpick()
{
    for (int i=0; i<m_items.count(); i++) {
        m_items.at(i)->setPos(m_originalItemPositions.at(i));
        m_items.at(i)->setOpacity(1.0); // FIXME
    }
}

void GraphicsMoveTool::place(const QPointF &pos)
{
    auto command = new UndoCommand();
    command->setText(QString("Move %1 object%2").arg(m_items.count()).arg(m_items.count() == 1 ? "" : "s"));
    for (int i=0; i<m_items.count(); i++) {
        m_items.at(i)->setPos(pos - m_deltaPickPositions.at(i));
        m_items.at(i)->setOpacity(1.0); // FIXME
        auto subCommand = new UpdateObjectCommand(command);
        subCommand->setUid(m_uids.at(i));
        subCommand->setProperty("location", m_items.at(i)->pos());
    }
    setUndoCommand(command);
    m_state = WaitForPick;
    m_view->setCursor(Qt::ArrowCursor);
    m_items.clear();
    m_uids.clear();
}

void GraphicsMoveTool::move(const QPointF &pos)
{
    for (int i=0; i<m_items.count(); i++)
        m_items.at(i)->setPos(pos - m_deltaPickPositions.at(i));
}

PlaceEllipseTool::PlaceEllipseTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsEllipseItem())
{
    m_item->setPen(GraphicsScene::defaultPen());
    m_item->setBrush(GraphicsScene::defaultBrush());
}

PlaceEllipseTool::~PlaceEllipseTool()
{
}

QPen PlaceEllipseTool::pen() const
{
    return m_item->pen();
}

QBrush PlaceEllipseTool::brush() const
{
    return m_item->brush();
}

void PlaceEllipseTool::setPen(QPen pen)
{
    if (m_item->pen() == pen)
        return;

    m_item->setPen(pen);
    emit penChanged(pen);
}

void PlaceEllipseTool::setBrush(QBrush brush)
{
    if (m_item->brush() == brush)
        return;

    m_item->setBrush(brush);
    emit brushChanged(brush);
}

void PlaceEllipseTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = view;
    m_item->setPos(pos);
    m_item->setRotation(0.0);
    m_item->setSize(QSizeF());
    m_item->setStartAngle(0.0);
    m_item->setSpanAngle(360.0);
    m_item->setStyle(Qs::FullEllipsoid);

    m_view->graphicsScene()->installEventHandler(this);
}

void PlaceEllipseTool::desactivate()
{
    if (m_item->scene() != nullptr)
        m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceEllipseTool::caption() const
{
    return QStringLiteral("Ellipse tool");
}

QString PlaceEllipseTool::description() const
{
    return QStringLiteral("Place ellipses, circles and arcs");
}

QKeySequence PlaceEllipseTool::shortcut() const
{
    return QKeySequence("e");
}

QIcon PlaceEllipseTool::icon() const
{
    return QIcon::fromTheme("draw-ellipse");
}

Qs::EllipseStyle PlaceEllipseTool::style() const
{
    return Qs::EllipseStyle(m_item->style());
}

void PlaceEllipseTool::setStyle(Qs::EllipseStyle style)
{
    if (m_item->style() == style)
        return;

    m_item->setStyle(style);
    emit styleChanged(style);
}


bool PlaceEllipseTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceEllipseTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceEllipseTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    const auto pos = event->scenePos();
    switch (m_state) {
        case PlaceEllipseTool::WaitForCenter:
            m_item->setPos(pos);
            break;
        case PlaceEllipseTool::WaitForRadius: {
            const qreal length = QLineF(m_item->pos(), pos).length();
            m_item->setSize(QSizeF(2*length, 2*length));
            break;
        }
        case PlaceEllipseTool::WaitForStartAngle: {
            const QLineF l1(m_item->pos(), pos);
            const qreal angle = -l1.angleTo(QLineF(0, 0, 1, 0));
            m_item->setStartAngle(angle);
            break;
        }
        case PlaceEllipseTool::WaitForSpanAngle: {
            const QLineF l1(m_item->pos(), pos);
            QLineF l2(l1);
            l2.setAngle(m_item->startAngle());
            m_item->setSpanAngle(360 - l1.angleTo(l2));
            break;
        }
    }
    return true;
}

bool PlaceEllipseTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    switch (m_state) {
        case PlaceEllipseTool::WaitForCenter:
            m_item->setSize(QSizeF());
            m_view->scene()->addItem(m_item.data());
            m_state = PlaceEllipseTool::WaitForRadius;
            break;
        case PlaceEllipseTool::WaitForRadius:
            if (m_item->style() == Qs::FullEllipsoid) {
                pushCommand();
                m_view->scene()->removeItem(m_item.data());
                m_state = PlaceEllipseTool::WaitForCenter;
            }
            else
                m_state = PlaceEllipseTool::WaitForStartAngle;
            break;
        case PlaceEllipseTool::WaitForStartAngle:
            m_state = PlaceEllipseTool::WaitForSpanAngle;
            break;
        case PlaceEllipseTool::WaitForSpanAngle: {
            m_state = PlaceEllipseTool::WaitForCenter;
            pushCommand();
            m_view->scene()->removeItem(m_item.data());
            m_state = PlaceEllipseTool::WaitForCenter;
            break;
        }
    }
    return true;
}

void PlaceEllipseTool::pushCommand()
{
    auto *command = new CreateObjectCommand();
    command->setTypeName("Ellipse");
    command->setProperties(objectProperties(m_item.data()));
    command->setProperties(strokeProperties(m_item->pen()));
    command->setProperties(fillProperties(m_item->brush()));
    command->setProperty("size", m_item->size());
    command->setProperty("startAngle", m_item->startAngle());
    command->setProperty("spanAngle", m_item->spanAngle());
    command->setProperty("style", m_item->style());
    setUndoCommand(command);
}

PlaceRectangleTool::PlaceRectangleTool(QObject *parent)
    : GraphicsTool(parent)
    , m_item(new GraphicsRectangleItem)
{
    m_item->setPen(GraphicsScene::defaultPen());
    m_item->setBrush(GraphicsScene::defaultBrush());
}

PlaceRectangleTool::~PlaceRectangleTool()
{

}

QPen PlaceRectangleTool::pen() const
{
    return m_item->pen();
}

QBrush PlaceRectangleTool::brush() const
{
    return m_item->brush();
}

void PlaceRectangleTool::setPen(QPen pen)
{
    if (m_item->pen() == pen)
        return;

    m_item->setPen(pen);
    emit penChanged(pen);
}

void PlaceRectangleTool::setBrush(QBrush brush)
{
    if (m_item->brush() == brush)
        return;

    m_item->setBrush(brush);
    emit brushChanged(brush);
}

void PlaceRectangleTool::pushCommand()
{
    auto *command = new CreateObjectCommand();
    command->setTypeName("Rectangle");
    command->setProperties(objectProperties(m_item.data()));
    command->setProperties(strokeProperties(m_item->pen()));
    command->setProperties(fillProperties(m_item->brush()));
    command->setProperty("size", m_item->size());
    setUndoCommand(command);
}

void PlaceRectangleTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = view;
    m_item->setPos(pos);
    m_item->setRotation(0.0);
    m_item->setSize(QSizeF());

    m_view->graphicsScene()->installEventHandler(this);
}

void PlaceRectangleTool::desactivate()
{
    if (m_item->scene() != nullptr)
        m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceRectangleTool::caption() const
{
    return QStringLiteral("Rectangle tool");
}

QString PlaceRectangleTool::description() const
{
    return QStringLiteral("Place rectangles");
}

QKeySequence PlaceRectangleTool::shortcut() const
{
    return QKeySequence("r");
}

QIcon PlaceRectangleTool::icon() const
{
    return QIcon::fromTheme("draw-rectangle");
}

bool PlaceRectangleTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceRectangleTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceRectangleTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    const auto pos = event->scenePos();
    switch (m_state) {
        case PlaceRectangleTool::WaitForCenter:
            m_item->setPos(pos);
            break;
        case PlaceRectangleTool::WaitForCorner: {
            const QLineF line = QLineF(m_item->pos(), pos);
            m_item->setSize(QSizeF(2*qAbs(line.dx()), 2*qAbs(line.dy())));
            break;
        }
    }
    return true;
}

bool PlaceRectangleTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    switch (m_state) {
        case PlaceRectangleTool::WaitForCenter:
            m_item->setSize(QSizeF());
            m_view->scene()->addItem(m_item.data());
            m_state = PlaceRectangleTool::WaitForCorner;
            break;
        case PlaceRectangleTool::WaitForCorner:
            pushCommand();
            m_view->scene()->removeItem(m_item.data());
            m_state = PlaceRectangleTool::WaitForCenter;
            break;
    }
    return true;
}

PlacePortTool::PlacePortTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsPortItem())
{
    // See GraphicsWidget::updatePortItem
    //m_item->setPen(GraphicsScene::defaultPen());
    m_item->setPen(QPen(QColor("#fa0000"), 0.0));
}

PlacePortTool::~PlacePortTool()
{

}

QPen PlacePortTool::pen() const
{
    return m_item->pen();
}

void PlacePortTool::setPen(QPen pen)
{
    if (m_item->pen() == pen)
        return;

    m_item->setPen(pen);
    emit penChanged(pen);
}

void PlacePortTool::pushCommand()
{
    auto *command = new CreateObjectCommand();
    command->setTypeName("Port");
    command->setProperties(objectProperties(m_item.data()));
    command->setProperty("name", m_item->name());
    setUndoCommand(command);
}

void PlacePortTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = view;
    m_item->setPos(pos);
    m_item->setRotation(0.0);

    m_view->graphicsScene()->installEventHandler(this);
    m_view->graphicsScene()->addItem(m_item.data());
}

void PlacePortTool::desactivate()
{
    m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlacePortTool::caption() const
{
    return "Port tool";
}

QString PlacePortTool::description() const
{
    return "Place ports";
}

QKeySequence PlacePortTool::shortcut() const
{
    return QKeySequence("p");
}

QIcon PlacePortTool::icon() const
{
    return QIcon::fromTheme("draw-donut");
}

bool PlacePortTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlacePortTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlacePortTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_item->setPos(event->scenePos());
    return true;
}

bool PlacePortTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    pushCommand();
    return true;
}


PlaceLineTool::PlaceLineTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsLineItem())
{
    m_item->setPen(GraphicsScene::defaultPen());
}

PlaceLineTool::~PlaceLineTool()
{

}

QPen PlaceLineTool::pen() const
{
    return m_item->pen();
}

void PlaceLineTool::setPen(QPen pen)
{
    if (m_item->pen() == pen)
        return;

    m_item->setPen(pen);
    emit penChanged(pen);
}

void PlaceLineTool::pushCommand()
{
    auto *command = new CreateObjectCommand();
    command->setTypeName("Line");
    command->setProperties(objectProperties(m_item.data()));
    command->setProperties(strokeProperties(m_item->pen()));
    command->setProperty("p1", m_item->p1());
    command->setProperty("p2", m_item->p2());
    setUndoCommand(command);
}

void PlaceLineTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = view;
    m_item->setPos(QPointF());
    m_item->setP1(pos);
    m_item->setP2(QPointF());
    m_item->setRotation(0.0);

    m_view->graphicsScene()->installEventHandler(this);
}

void PlaceLineTool::desactivate()
{
    if (m_item->scene() != nullptr)
        m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceLineTool::caption() const
{
    return "Line tool";
}

QString PlaceLineTool::description() const
{
    return "Place lines";
}

QKeySequence PlaceLineTool::shortcut() const
{
    return QKeySequence("l");
}

QIcon PlaceLineTool::icon() const
{
    return QIcon::fromTheme("draw-line");
}

bool PlaceLineTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLineTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLineTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    const auto pos = event->scenePos();
    switch (m_state) {
        case PlaceLineTool::WaitForP1:
            m_item->setP1(pos);
            break;
        case PlaceLineTool::WaitForP2:
            m_item->setP2(pos);
            break;
    }
    return true;
}

bool PlaceLineTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;


    switch (m_state) {
        case PlaceLineTool::WaitForP1:
            m_item->setP2(m_item->p1());
            m_state = PlaceLineTool::WaitForP2;
            m_view->scene()->addItem(m_item.data());
            break;
        case PlaceLineTool::WaitForP2:
            m_state = PlaceLineTool::WaitForP1;
            m_view->scene()->removeItem(m_item.data());
            pushCommand();
            break;
    }
    return true;
}

PlaceLabelTool::PlaceLabelTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsLabelItem())
{
    m_item->setBrush(GraphicsScene::defaultPen().color());
}

PlaceLabelTool::~PlaceLabelTool()
{
}

QBrush PlaceLabelTool::brush() const
{
    return m_item->brush();
}

void PlaceLabelTool::setBrush(QBrush brush)
{
    if (m_item->brush() == brush)
        return;

    m_item->setBrush(brush);
    emit brushChanged(brush);
}

void PlaceLabelTool::activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = view;
    m_item->setPos(pos);
    m_item->setRotation(0.0);
    m_item->setFont(QFont());
    m_item->setText(QString("Text"));

    m_view->graphicsScene()->addItem(m_item.data());
    m_view->graphicsScene()->installEventHandler(this);
}

void PlaceLabelTool::desactivate()
{
    m_view->graphicsScene()->uninstallEventHandler(this);
    m_view->graphicsScene()->removeItem(m_item.data());
}

QString PlaceLabelTool::caption() const
{
    return QStringLiteral("Label tool");
}

QString PlaceLabelTool::description() const
{
    return QStringLiteral("Place text labels");
}

QKeySequence PlaceLabelTool::shortcut() const
{
    return QKeySequence("t");
}

QIcon PlaceLabelTool::icon() const
{
    return QIcon::fromTheme("draw-text");
}

bool PlaceLabelTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLabelTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLabelTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_item->setPos(event->scenePos());
    return true;
}

bool PlaceLabelTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    pushCommand();
    return true;
}

void PlaceLabelTool::pushCommand()
{
    auto *command = new CreateObjectCommand();
    command->setTypeName("Label");
    command->setProperties(objectProperties(m_item.data()));
    command->setProperty("size", m_item->font().pointSizeF());
    command->setProperty("color", m_item->brush().color());
    command->setProperty("text", m_item->text());
    setUndoCommand(command);
}
