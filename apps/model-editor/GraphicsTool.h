/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/Object.h"
#include "qlCore/QlCore.h"
#include "qlGui/GraphicsEventHandler.h"

#include <QtCore/QObject>
#include <QtCore/QScopedPointer>
#include <QtGui/QPen>
#include <QtGui/QBrush>

class GraphicsEllipseItem;
class GraphicsLabelItem;
class GraphicsLineItem;
class GraphicsRectangleItem;
class GraphicsPortItem;
class GraphicsView;
class UndoCommand;

class QGraphicsItem;
class QGraphicsSceneMouseEvent;
class QRubberBand;

// TODO: Bundle tools into a GraphicsItemToolProvider
// - Tool metadata (icons, ...)
// - createPlacementTool()
// - createEditingTool()
//
// See comment in GraphicsEventHandler
//
// TBD: single shot vs auto-repeat, tools need a finished and cancelled signals
// TBD: interactive vs non-interactive
//
// Missing tools:
//  - clone
//  - align objects (GraphicsItem need to know if they are the first and/or last in the selection)
//  - Z management (bring to front/bottom, raise, lower)
//  - snap object to grid
//  - group/ungroup

class GraphicsTool : public QObject, public GraphicsSceneEventHandler
{
    Q_OBJECT

public:
    explicit GraphicsTool(QObject *parent = 0);
    ~GraphicsTool();

    // TODO: properties of the tool bundle
    virtual QString caption() const = 0;
    virtual QString description() const = 0;
    virtual QKeySequence shortcut() const = 0;
    virtual QIcon icon() const = 0;

    // TBD: Might want to separate attach/detach from activate/reactivate/desactivate
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) = 0;
    virtual void desactivate() = 0;

    bool hasUndoCommand() const;
    UndoCommand *takeUndoCommand();

public slots:

signals:
    void undoCommandAvailable();
    void finished();

protected:
    void setUndoCommand(UndoCommand *command);

    QMap<QString, QVariant> objectProperties(const QGraphicsItem *item);
    QMap<QString, QVariant> strokeProperties(const QPen &pen);
    QMap<QString, QVariant> fillProperties(const QBrush &brush);

private:
    UndoCommand *m_command;
};

class GraphicsSelectTool: public GraphicsTool
{
public:
    explicit GraphicsSelectTool(QObject *parent = 0);
    ~GraphicsSelectTool();

private:
    enum State {
        ClickSelect = 0,
        DragSelect,
        RubberBandSelect
    };
    enum ButtonState {
        ButtonDown = 0,
        ButtonUp
    };

    GraphicsView *m_view = nullptr;
    GraphicsScene *m_scene = nullptr;
    QRubberBand *m_rubberBand = nullptr;
    QList<ObjectUid> m_draggedUids;
    State m_state = ClickSelect;
    ButtonState m_buttonState = ButtonUp;
    QPointF m_buttonDownPos;
    int m_dragThreshold = 5; // pixels

    bool canSelect(const QPointF &pos);
    void maybeAddToOrClearSelection(const QPointF &pos);
    void maybeRemoveFromOrClearSelection(const QPointF &pos);
    void maybeSetOrClearSelection(const QPointF &pos);
    bool canStartDrag(const QPointF &pos);
    void startRubberBandDrag(const QPointF &pos);
    void updateRubberBand(const QPointF &pos);
    void endRubberBand(const QPointF &pos);
    void startSelectionDrag(const QPointF &pos);
    void updateDragSelect(const QPointF &pos);
    void endDragSelect(const QPointF &pos);
    void setState(State state);

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class GraphicsMoveTool: public GraphicsTool
{
public:
    explicit GraphicsMoveTool(QObject *parent = 0);
    ~GraphicsMoveTool();

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;

private:
    enum State {
        WaitForPick = 0,
        WaitForPlace
    };
    State m_state = WaitForPick;
    GraphicsView *m_view;
    QList<QGraphicsItem *> m_items;
    QList<ObjectUid> m_uids;
    QList<QPointF> m_deltaPickPositions;
    QList<QPointF> m_originalItemPositions;
    bool checkForPick(const QPointF &pos);
    void pick(const QPointF &pos);
    bool hasPicked();
    void unpick();
    void place(const QPointF &pos);
    void move(const QPointF &pos);
};

class PlaceEllipseTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(QPen pen READ pen WRITE setPen NOTIFY penChanged)
    Q_PROPERTY(QBrush brush READ brush WRITE setBrush NOTIFY brushChanged)
    Q_PROPERTY(Qs::EllipseStyle style READ style WRITE setStyle NOTIFY styleChanged)
    // TODO: bool ellipsoidal
public:
    explicit PlaceEllipseTool(QObject *parent = 0);
    ~PlaceEllipseTool();


    QPen pen() const;
    QBrush brush() const;
    Qs::EllipseStyle style() const;

public slots:
    void setPen(QPen pen);
    void setBrush(QBrush brush);
    void setStyle(Qs::EllipseStyle style);

signals:
    void styleChanged(Qs::EllipseStyle style);
    void penChanged(QPen pen);
    void brushChanged(QBrush brush);

private:
    void pushCommand();
    enum State {
        WaitForCenter,
        WaitForRadius,
        WaitForStartAngle,
        WaitForSpanAngle
    };
    GraphicsView *m_view;
    QScopedPointer<GraphicsEllipseItem> m_item;
    State m_state;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};


class PlaceRectangleTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(QPen pen READ pen WRITE setPen NOTIFY penChanged)
    Q_PROPERTY(QBrush brush READ brush WRITE setBrush NOTIFY brushChanged)

public:
    explicit PlaceRectangleTool(QObject *parent = 0);
    ~PlaceRectangleTool();

    QPen pen() const;
    QBrush brush() const;

public slots:
    void setPen(QPen pen);
    void setBrush(QBrush brush);

signals:
    void penChanged(QPen pen);
    void brushChanged(QBrush brush);

private:
    void pushCommand();
    enum State {
        WaitForCenter,
        WaitForCorner,
    };
    GraphicsView *m_view = nullptr;
    QScopedPointer<GraphicsRectangleItem> m_item;
    State m_state = WaitForCenter;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlacePortTool: public GraphicsTool
{
    Q_OBJECT
    //Q_PROPERTY(bool autoIncrement READ autoIncrement WRITE setAutoIncrement NOTIFY autoIncrementChanged)
    Q_PROPERTY(QPen pen READ pen WRITE setPen NOTIFY penChanged)

public:
    explicit PlacePortTool(QObject *parent = 0);
    ~PlacePortTool();

    QPen pen() const;

public slots:
    void setPen(QPen pen);

signals:
    void penChanged(QPen pen);

private:
    void pushCommand();
    GraphicsView *m_view = nullptr;
    QScopedPointer<GraphicsPortItem> m_item;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlaceLineTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(QPen pen READ pen WRITE setPen NOTIFY penChanged)

public:
    explicit PlaceLineTool(QObject *parent = 0);
    ~PlaceLineTool();

    QPen pen() const;

public slots:
    void setPen(QPen pen);

signals:
    void penChanged(QPen pen);

private:
    enum State {
        WaitForP1,
        WaitForP2,
    };
    void pushCommand();
    GraphicsView *m_view = nullptr;
    QScopedPointer<GraphicsLineItem> m_item;
    State m_state = WaitForP1;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};


class PlaceLabelTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(QBrush brush READ brush WRITE setBrush NOTIFY brushChanged)

public:
    explicit PlaceLabelTool(QObject *parent = 0);
    ~PlaceLabelTool();


    QBrush brush() const;

public slots:
    void setBrush(QBrush brush);

signals:
    void brushChanged(QBrush brush);

private:
    void pushCommand();
    GraphicsView *m_view;
    QScopedPointer<GraphicsLabelItem> m_item;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsView *view, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};
