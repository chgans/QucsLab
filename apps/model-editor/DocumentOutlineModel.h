/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/Object.h"

#include <QtCore/QAbstractItemModel>

class SymbolDocument;

class DocumentOutlineModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit DocumentOutlineModel(QObject *parent = 0);

    void setDocument(const SymbolDocument *document);
    const SymbolDocument *document() const;

    ObjectUid objectUid(const QModelIndex &index) const;
    QModelIndex objectIndex(ObjectUid uid) const;
    QModelIndex documentIndex() const;

private:
    const SymbolDocument *m_document;
    void beginMonitorDocument();
    void endMonitorDocument();

    bool isRootIndex(const QModelIndex &index) const;
    QModelIndex rootIndex() const;
    bool isDocumentIndex(const QModelIndex &index) const;
    QModelIndex itemIndex(int documentId) const;

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};
