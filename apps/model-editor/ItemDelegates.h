/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QStyledItemDelegate>

class QValidator;
class QCompleter;

class EnumItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    EnumItemDelegate(QObject *parent = 0);

    template <typename T>
    void addEnum(T value, const QString &caption)
    {
        addEnum(static_cast<int>(value), caption);
    }

    void addEnum(int value, const QString &caption);

private:
    QList<int> m_values;
    QMap<int, QString> m_valueToCaption;
    QMap<int, int> m_valueToIndex;

    // QItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

class StringItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    StringItemDelegate(QObject *parent = 0);

    void setCompleter(QCompleter *completer);
    QCompleter *comleter() const;
    void setValidator(QValidator *validator);
    QValidator *validator() const;

private:
    QList<int> m_values;
    QMap<int, QString> m_valueToCaption;
    QMap<int, int> m_valueToIndex;
    QValidator *m_validator = nullptr;
    QCompleter *m_completer = nullptr;

    // QItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};
