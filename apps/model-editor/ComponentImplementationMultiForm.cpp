/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ComponentImplementationForm.h"
#include "ComponentImplementationMultiForm.h"

#include "qlCore/LibraryObjects.h"

#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>

SourceCodeViewMultiForm::SourceCodeViewMultiForm(QWidget *parent)
    : QScrollArea(parent)
{
    auto widget = new QWidget();
    m_layout = new QVBoxLayout(this);
    m_layout->addStretch(10);
    widget->setLayout(m_layout);
    setWidget(widget);
    setWidgetResizable(true);
}

void SourceCodeViewMultiForm::setCell(lib::Cell *cell)
{
    m_cell = cell;
}

lib::Cell *SourceCodeViewMultiForm::cell() const
{
    return m_cell;
}

void SourceCodeViewMultiForm::setViews(QList<lib::SourceCodeView *> views)
{
    qDeleteAll(m_groupBoxes);
    m_groupBoxes.clear();
    m_views = views;
    for (auto view: views) {
        auto box = new QGroupBox(view->identification.caption);
        auto form = new SourceCodeViewForm();
        form->setCell(m_cell);
        form->setView(view);
        auto boxLayout = new QVBoxLayout();
        boxLayout->addWidget(form);
        box->setLayout(boxLayout);
        m_layout->insertWidget(0, box);
        m_groupBoxes.append(box);
    }
}

QList<lib::SourceCodeView *> SourceCodeViewMultiForm::views() const
{
    return m_views;
}
