/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsTool.h"
#include "GraphicsWidget.h"
#include "DocumentNavigationWidget.h"
#include "GraphicsToolPropertyWidget.h"

#include "qlGui/GraphicsGrid.h"
#include "qlGui/GraphicsItem.h"
#include "qlGui/GraphicsScene.h"
#include "qlGui/GraphicsSheet.h"
#include "qlGui/GraphicsSnapper.h"
#include "qlGui/GraphicsView.h"
#include "qlGui/GraphicsZoomHandler.h"
#include "qlCore/SymbolObjects.h"
#include "qlCore/SymbolDocument.h"
#include "qlCore/SymbolReader.h"
#include "qlCore/SymbolWriter.h"
#include "qlCore/SymbolCommand.h"

#include <QtCore/QBuffer>
#include <QtCore/QFile>
#include <QtCore/QMimeData>
#include <QtGui/QClipboard>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QUndoStack>
#include <QtWidgets/QUndoView>
#include <QtWidgets/QVBoxLayout>

const QString GraphicsWidget::g_symbolFragmentMimeType("application/vnd+qucs.gui.symbol.fragment+xml");

GraphicsWidget::GraphicsWidget(QWidget *parent)
    : QWidget(parent)
    , m_undoStack(new QUndoStack(this))
    , m_scene(new GraphicsScene(this))
    , m_view(new GraphicsView())
    , m_sheet(new GraphicsSheet())
    , m_grid(new GraphicsGrid())
    , m_snapper(new GraphicsSnapper(this))
    , m_zoomHandler(new GraphicsZoomHandler(this))
    , m_selectTool(new GraphicsSelectTool(this))
    , m_moveTool(new GraphicsMoveTool(this))
    , m_placeEllipseTool(new PlaceEllipseTool(this))
    , m_placeLabelTool(new PlaceLabelTool(this))
    , m_placeLineTool(new PlaceLineTool(this))
    , m_placePortTool(new PlacePortTool(this))
    , m_placeRectangleTool(new PlaceRectangleTool(this))
    , m_toolActionGroup(new QActionGroup(this))
    , m_documentOulineWidget(new DocumentNavigationWidget())
    //, m_documentOulineDockWidget(new QDockWidget())
    , m_toolPropertyWidget(new GraphicsToolPropertyWidget())
    , m_toolPropertyDockWidget(new QDockWidget())
    , m_commandHistoryWidget(new QUndoView())
    , m_commandHistoryDockWidget(new QDockWidget())
{
    m_view->setAcceptDrops(true);
    m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setBackgroundBrush(QColor("#eee8d5"));
    m_view->setInteractive(true);
    m_view->setDragMode(QGraphicsView::NoDrag);

    m_sheet->setRect(QRectF(0, 0, 297.0, 210.0));
    m_sheet->setPos(QPointF(-150, -100));
    m_sheet->setPen(QPen(QColor("#93a1a1"), 0.0));
    m_sheet->setBrush(QColor("#fdf6e3"));

    // TODO: connect to sheet's sceneRectChanged()
    m_grid->setRect(m_sheet->rect());
    m_grid->setPos(m_sheet->pos());
    m_grid->setMajorColor(QColor("#93a1a1"));
    m_grid->setStep(2.0); // mm
    m_grid->setMinimumGridSize(20); // pixels

    // TODO: connect to grid's stepChanged()
    m_snapper->setStep(m_grid->step());

    m_scene->addItem(m_sheet);
    m_scene->addItem(m_grid);
    m_scene->setSceneRect(m_scene->itemsBoundingRect());
    m_view->setScene(m_scene);

    // FIXME: Handlers need to be attached after m_view->setScene(m_scene)
    m_zoomHandler->setView(m_view);
    m_snapper->setView(m_view);

    loadTools();
    createActions();
    createToolBars();
    createDockWidgets();
    setupUndoStack();
    setupClipBoard();

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_view);

    setCurrentTool(m_tools.first());
}

GraphicsWidget::~GraphicsWidget()
{

}

void GraphicsWidget::setDocument(SymbolDocument *document)
{
    if (m_document != nullptr)
        endEditDocument();

    m_document = document;

    if (m_document != nullptr)
        beginEditDocument();
}

void GraphicsWidget::setGraphicsObjects(const QList<draw::DrawingObject *> &objects)
{
    auto items = addSceneItems(objects);
    auto group = m_scene->createItemGroup(items);
    m_view->fitInView(group->sceneBoundingRect(), Qt::KeepAspectRatio);
}

void GraphicsWidget::saveDocument(const QString &path)
{
    SymbolWriter writer;
     QFile file(path);
     if (!file.open(QFile::WriteOnly)) {
         QMessageBox::critical(this, "Error", file.errorString());
         return;
     }
     if (!writer.writeSymbol(&file, m_document->symbol())) {
         QMessageBox::critical(this, "Error", writer.errorString());
         return;
     }
     if (file.error() != QFile::NoError) {
         QMessageBox::critical(this, "Error", file.errorString());
         return;
     }
     file.close();
     m_undoStack->setClean();
}

void GraphicsWidget::openDocument(const QString &path)
{
    SymbolReader reader;
    QFile file(path);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    if (!reader.readSymbol(&file)) {
        QMessageBox::critical(this, "Error", reader.errorString());
        return;
    }
    file.close();

    setDocument(new SymbolDocument(reader.takeSymbol()));
}

void GraphicsWidget::closeDocument()
{
    setDocument(nullptr);
}

QList<QAction *> GraphicsWidget::editActions() const
{
    QList<QAction *> result;
    for (auto action: m_toolActionGroup->actions()) {
        if (action->text() == m_selectTool->caption())
            result.append(action);
        else if (action->text() == m_moveTool->caption())
            result.append(action);
    }
    return result;
}

QList<QAction *> GraphicsWidget::placementActions() const
{
    QList<QAction *> result;
    for (auto action: m_toolActionGroup->actions()) {
        if (action->text() == m_selectTool->caption())
            continue;
        if (action->text() == m_moveTool->caption())
            continue;
        result.append(action);
    }
    return result;
}

QList<QAction *> GraphicsWidget::toolsActions() const
{
    return QList<QAction *>();
}

QList<QDockWidget *> GraphicsWidget::dockWidgets() const
{
    return m_dockWidgets;
}

QList<QToolBar *> GraphicsWidget::toolBars() const
{
    return m_toolBars;
}

bool GraphicsWidget::canUndo() const
{
    return m_undoStack->canUndo();
}

bool GraphicsWidget::canRedo() const
{
    return m_undoStack->canRedo();
}

bool GraphicsWidget::canCut() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canCopy() const
{
    return m_document && !m_document->selection().isEmpty();
}

// FIXME:
bool GraphicsWidget::canPaste() const
{
    return true;
}

bool GraphicsWidget::canMove() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canDelete() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canSelectAll() const
{
    return true;
}

bool GraphicsWidget::canClearSelection() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canInvertSelection() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::isModified() const
{
    return !m_undoStack->isClean();
}

void GraphicsWidget::undo()
{
    m_undoStack->undo();
}

void GraphicsWidget::redo()
{
    m_undoStack->redo();
}

void GraphicsWidget::cut(QClipboard *clipboard)
{
    copy(clipboard);
    deleteIt();
}

// TODO: move mime data format definition to global QucsLab namespace
// FIXME: Review the copy/paste and XML hadling
void GraphicsWidget::copy(QClipboard *clipboard)
{
    QBuffer buffer;
    SymbolWriter writer;
    buffer.open(QBuffer::WriteOnly);
    auto objects = m_document->selectedObjects();
    if (!writer.writeObjects(&buffer, objects))
        return;
    auto mimeData = new QMimeData();
    mimeData->setData(g_symbolFragmentMimeType, buffer.data());
    clipboard->setMimeData(mimeData);
}

void GraphicsWidget::paste(const QClipboard *clipboard)
{
    if (!clipboard->mimeData()->hasFormat(g_symbolFragmentMimeType))
        return;

    // FIXME: Hack to make the reader happy: make the data lloks like a symbol document
    // This rely on another hack in SymbolReader!
    QByteArray data;
    data.append("<symbol xmlns=\"\" version=\"1.0\">\n");
    data.append(clipboard->mimeData()->data(g_symbolFragmentMimeType));
    data.append("</symbol>");
    QBuffer buffer;
    buffer.setData(data);
    buffer.open(QBuffer::ReadOnly);
    SymbolReader reader;
    if (!reader.readObjects(&buffer))
        return;
    auto objects = reader.objects();

    // FIXME need a tool to place arbitrary objects and a better way to manage copy/paste...
    // The way it currently works, is that:
    //  - Objects are deserialised from clipboards's XML
    //  - Objects are then serialised into a Composite 'create' command
    //  - we set a flag saying "We're pasting"
    //  - The command is executed on the document
    //  - The document recreate the objects
    //  - As an observer we receive 'object added' notifications
    //    => the addSceneItem() callback will then add uids into a special list
    //  - We reset the document object selection with this list
    //  - we trigger the 'Move' tool...
    // But this not enough!
    //  - On 'escape' the move tool will restore objects original position, we don't want that,
    //  on 'escape' we wan't the 'paste' to be discarded
    //  - So as a ultimate hack, we have to tell the move tool to emit an 'undo' request instead...
    //
    // Obviously the whole thing needs to be refactored and cleanup!
    //

    m_pastedObjectUids.clear();
    m_pastingObjects = true;
    auto command = new UndoCommand();
    command->setText(QString("Paste %1 object%2").arg(objects.count()).arg(objects.count() == 1 ? "" : "s"));
    for (auto object: objects)
        CreateObjectCommand::fromObject(object, command);
    executeCommand(command);
    m_pastingObjects = false;


    m_document->clearSelection();
    m_document->select(m_pastedObjectUids);

    for (auto action: m_toolActionGroup->actions()) {
        if (action->text() == m_moveTool->caption()) {
            action->trigger();
            break;
        }
    }
}

void GraphicsWidget::moveIt()
{
    setCurrentTool(m_moveTool);
}

void GraphicsWidget::deleteIt()
{
    auto uids = m_document->selection();
    auto *command = new DeleteObjectCommand();
    command->setText(QString("Delete %1 object%2").arg(uids.count()).arg(uids.count() == 1 ? "" : "s"));
    command->setUids(uids);
    executeCommand(command);
}

void GraphicsWidget::selectAll()
{
    m_document->selectAll();
}

void GraphicsWidget::clearSelection()
{
    m_document->clearSelection();
}

void GraphicsWidget::invertSelection()
{
    m_document->invertSelection();
}

void GraphicsWidget::createActions()
{
    for (auto tool: m_tools) {
        auto action = new QAction(tool->icon(), tool->caption());
        action->setShortcut(tool->shortcut());
        action->setToolTip(QString("%1: %2 <i>%3</i>").arg(tool->caption(),
                                                           tool->description(),
                                                           tool->shortcut().toString()));
        action->setCheckable(true);
        action->setChecked(false);
        action->setData(QVariant::fromValue(tool));
        m_toolActionGroup->addAction(action);

        connect(tool, &GraphicsTool::undoCommandAvailable,
                this, [this, tool]() {
            executeCommand(tool->takeUndoCommand());
        });
        connect(tool, &GraphicsTool::finished,
                this, [this]() {
            m_toolActionGroup->actions().first()->trigger();
        });
    }

    m_toolActionGroup->actions().first()->setChecked(true);
    m_toolActionGroup->setExclusive(true);
    connect(m_toolActionGroup, &QActionGroup::triggered,
            this, [this](QAction *triggered) {
       auto tool = triggered->data().value<GraphicsTool*>();
       setCurrentTool(tool);
    });
}

void GraphicsWidget::createToolBars()
{
    auto bar = new QToolBar("Place objects");
    bar->addActions(m_toolActionGroup->actions());
    m_toolBars.append(bar);
}

void GraphicsWidget::createDockWidgets()
{
    //m_documentOulineDockWidget->setObjectName("org.qucs.symboleditor.documentoutline");
    //m_documentOulineDockWidget->setWidget(m_documentOulineWidget);
    //m_documentOulineDockWidget->setWindowTitle("Document outline");
    //m_dockWidgets.append(m_documentOulineDockWidget);

    m_toolPropertyDockWidget->setObjectName("org.qucs.symboleditor.toolproperties");
    m_toolPropertyDockWidget->setWidget(m_toolPropertyWidget);
    m_toolPropertyDockWidget->setWindowTitle("Tool");
    m_dockWidgets.append(m_toolPropertyDockWidget);

    m_commandHistoryWidget->setStack(m_undoStack);
    m_commandHistoryDockWidget->setObjectName("org.qucs.symboleditor.commandhistory");
    m_commandHistoryDockWidget->setWidget(m_commandHistoryWidget);
    m_commandHistoryDockWidget->setWindowTitle("Command history");
    m_commandHistoryDockWidget->hide(); // FIXME: Should be in user settings
    m_dockWidgets.append(m_commandHistoryDockWidget);
}

void GraphicsWidget::setupUndoStack()
{
    connect(m_undoStack, &QUndoStack::canUndoChanged,
            this, &GraphicsWidget::canUndoChanged);
    connect(m_undoStack, &QUndoStack::canRedoChanged,
            this, &GraphicsWidget::canRedoChanged);
    connect(m_undoStack, &QUndoStack::cleanChanged,
            this, [this](bool clean) {
        emit isModifiedChanged(!clean);
    });
}

void GraphicsWidget::setupClipBoard()
{

}

void GraphicsWidget::addSceneItem(const draw::DrawingObject *object)
{
    if (m_pastingObjects) // Special copy/paste treatment, see paste()
        m_pastedObjectUids.insert(object->uid());

    const auto ellipse = qobject_cast<const draw::Ellipse*>(object);
    const auto rectangle = qobject_cast<const draw::Rectangle*>(object);
    const auto port = qobject_cast<const sym::Port*>(object);
    const auto label = qobject_cast<const draw::Label*>(object);
    const auto line = qobject_cast<const draw::Line*>(object);
    if (ellipse != nullptr) {
        auto item = new GraphicsEllipseItem();
        updateEllipseItem(item, ellipse);
        m_scene->addGraphicsItem(item, object->uid());
    }
    else if (rectangle != nullptr) {
        auto item = new GraphicsRectangleItem();
        updateRectangleItem(item, rectangle);
        m_scene->addGraphicsItem(item, object->uid());
    }
    else if (port != nullptr) {
        auto item = new GraphicsPortItem();
        updatePortItem(item, port);
        m_scene->addGraphicsItem(item, object->uid());
    }
    else if (line != nullptr) {
        auto item = new GraphicsLineItem();
        updateLineItem(item, line);
        m_scene->addGraphicsItem(item, object->uid());
    }
    else if (label != nullptr) {
        auto item = new GraphicsLabelItem();
        updateLabelItem(item, label);
        m_scene->addGraphicsItem(item, object->uid());
    }
    else {
        qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
    }
}

QList<QGraphicsItem *> GraphicsWidget::addSceneItems(const QList<draw::DrawingObject *> &objects)
{
    QList<QGraphicsItem *> result;
    for (auto object: objects) {
        const auto ellipse = qobject_cast<const draw::Ellipse*>(object);
        const auto rectangle = qobject_cast<const draw::Rectangle*>(object);
        const auto port = qobject_cast<const sym::Port*>(object);
        const auto label = qobject_cast<const draw::Label*>(object);
        const auto line = qobject_cast<const draw::Line*>(object);
        if (ellipse != nullptr) {
            auto item = new GraphicsEllipseItem();
            updateEllipseItem(item, ellipse);
            m_scene->addGraphicsItem(item, object->uid());
            result.append(item);
        }
        else if (rectangle != nullptr) {
            auto item = new GraphicsRectangleItem();
            updateRectangleItem(item, rectangle);
            m_scene->addGraphicsItem(item, object->uid());
            result.append(item);
        }
        else if (port != nullptr) {
            auto item = new GraphicsPortItem();
            updatePortItem(item, port);
            m_scene->addGraphicsItem(item, object->uid());
            result.append(item);
        }
        else if (line != nullptr) {
            auto item = new GraphicsLineItem();
            updateLineItem(item, line);
            m_scene->addGraphicsItem(item, object->uid());
            result.append(item);
        }
        else if (label != nullptr) {
            auto item = new GraphicsLabelItem();
            updateLabelItem(item, label);
            m_scene->addGraphicsItem(item, object->uid());
            result.append(item);
        }
        else {
            qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
        }
    }
    return result;
}

void GraphicsWidget::setCurrentTool(GraphicsTool *tool)
{
    if (m_currentTool == tool)
        return;

    if (m_currentTool != nullptr) {
        m_currentTool->desactivate();
        m_toolPropertyDockWidget->setWindowTitle("No tool");
        m_toolPropertyWidget->setTool(nullptr);
    }

    m_currentTool = tool;

    if (m_currentTool != nullptr) {
        const auto pos = m_view->mapToScene(m_view->mapFromGlobal(QCursor::pos()));
        const auto snappedPos = m_snapper->snapScenePos(pos);
        m_currentTool->activate(m_view, snappedPos, pos);
        m_toolPropertyDockWidget->setWindowTitle(m_currentTool->caption());
        if (m_currentTool == m_selectTool) {
            m_toolPropertyDockWidget->setWidget(m_documentOulineWidget);
        }
        else {
            m_toolPropertyDockWidget->setWidget(m_toolPropertyWidget);
            m_toolPropertyWidget->setTool(m_currentTool);
        }
    }

}

// Bridge: Document object <-> Graphics item
// TODO: create a Converter class, see GraphicsItemToolProvider in GraphicsTool.h

void GraphicsWidget::updateGraphicsItem(QGraphicsItem *item, const draw::DrawingObject *object)
{
    item->setPos(object->location());
    item->setRotation(object->rotation());
    // object->isMirrored(); // FIXME: use setTransform
    item->setVisible(object->isVisible());
    item->setEnabled(!object->isLocked());
    item->setFlag(QGraphicsItem::ItemIsSelectable);
}

void GraphicsWidget::updateEllipseItem(GraphicsEllipseItem *item, const draw::Ellipse *object)
{
    updateGraphicsItem(item, object);
    item->setSize(object->size());
    item->setStartAngle(object->startAngle());
    item->setSpanAngle(object->spanAngle());
    item->setStyle(object->style());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
    item->setBrush(createBrush(object->fillStyle(), object->fillColor()));
}

void GraphicsWidget::updateLineItem(GraphicsLineItem *item, const draw::Line *object)
{
    updateGraphicsItem(item, object);
    item->setP1(object->p1());
    item->setP2(object->p2());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
}

void GraphicsWidget::updateLabelItem(GraphicsLabelItem *item, const draw::Label *object)
{
    updateGraphicsItem(item, object);
    item->setText(object->text());
    auto font = item->font();
    font.setPointSizeF(object->size());
    item->setFont(font);
    item->setBrush(object->color());
}

void GraphicsWidget::updateRectangleItem(GraphicsRectangleItem *item, const draw::Rectangle *object)
{
    updateGraphicsItem(item, object);
    item->setSize(object->size());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
    item->setBrush(createBrush(object->fillStyle(), object->fillColor()));
}

void GraphicsWidget::updatePortItem(GraphicsPortItem *item, const sym::Port *object)
{
    updateGraphicsItem(item, object);
    item->setName(object->name());
    item->setPen(QPen(QColor("#fa0000"), 0.0)); // Original Qucs Color
    item->setZValue(100); // Ports are always on top
}

void GraphicsWidget::beginEditDocument()
{
    connect(m_document, &SymbolDocument::selectionChanged,
            this, [this]() {
        emit canMoveChanged(canMove());
        emit canDeleteChanged(canDelete());
        emit canCopyChanged(canCopy());
        emit canCutChanged(canCut());
        emit canSelectAllChanged(canSelectAll());
        emit canClearSelectionChanged(canClearSelection());
        emit canInvertSelectionChanged(canInvertSelection());
    });
    connect(m_document, &SymbolDocument::objectAdded,
            this, QOverload<ObjectUid>::of(&GraphicsWidget::addSceneItem));
    connect(m_document, &SymbolDocument::aboutToRemoveObject,
            this, &GraphicsWidget::removeSceneItem);
    connect(m_document, &SymbolDocument::objectUpdated,
            this, &GraphicsWidget::updateSceneItem);
    connect(m_document, &SymbolDocument::selectionChanged,
            this, &GraphicsWidget::updateSceneSelectionFromDocumentSelection);
    connect(m_scene, &GraphicsScene::selectionChanged,
            this, &GraphicsWidget::updateDocumentSelectionFromSceneSelection);
    connect(m_documentOulineWidget, &DocumentNavigationWidget::commandAvailable,
            this, [this]() {
        executeCommand(m_documentOulineWidget->takeCommand());
    });
    for (auto uid: m_document->objectUids())
        if (uid != m_document->symbol()->uid())
            addSceneItem(uid);

    m_documentOulineWidget->setDocument(m_document);
}

void GraphicsWidget::endEditDocument()
{
    m_documentOulineWidget->disconnect(this);
    m_scene->disconnect(this);
    m_document->disconnect(this);
    m_documentOulineWidget->setDocument(nullptr);
    for (auto uid: m_document->objectUids())
        if (uid != m_document->symbol()->uid())
            removeSceneItem(uid);
    m_undoStack->clear();
}

void GraphicsWidget::executeCommand(UndoCommand *command)
{
    command->setDocument(m_document);
    m_undoStack->push(command);
}

QPen GraphicsWidget::createPen(Qs::StrokeWidth width, Qs::StrokeStyle style, const QColor &color)
{
    QPen pen = GraphicsScene::defaultPen();
    switch (width) {
        case Qs::SmallestStroke:
            pen.setWidthF(0.05);
            break;
        case Qs::SmallStroke:
            pen.setWidthF(0.15);
            break;
        case Qs::MediumStroke:
            pen.setWidthF(0.3);
            break;
        case Qs::LargeStroke:
            pen.setWidthF(0.7);
            break;
        case Qs::LargestStroke:
            pen.setWidthF(2.0);
            break;
    }
    switch (style) {
        case Qs::SolidStroke:
            pen.setStyle(Qt::SolidLine);
            break;
        case Qs::DashStroke:
            pen.setStyle(Qt::DashLine);
            break;
        case Qs::DotStroke:
            pen.setStyle(Qt::DotLine);
            break;
        case Qs::DashDotStroke:
            pen.setStyle(Qt::DashDotLine);
            break;
        case Qs::DasDotDotStroke:
            pen.setStyle(Qt::DashDotDotLine);
            break;
        case Qs::NoStroke:
            pen.setStyle(Qt::NoPen);
            break;
    }
    pen.setColor(color);
    return pen;
}

QBrush GraphicsWidget::createBrush(Qs::FillStyle style, const QColor &color)
{
    QBrush brush = GraphicsScene::defaultBrush();
    switch (style) {
        case Qs::SolidFill:
            brush.setStyle(Qt::SolidPattern);
            break;
        case Qs::HorLineFill:
            brush.setStyle(Qt::HorPattern);
            break;
        case Qs::VerLineFill:
            brush.setStyle(Qt::VerPattern);
            break;
        case Qs::FwLineFill:
            brush.setStyle(Qt::FDiagPattern);
            break;
        case Qs::BwLineFill:
            brush.setStyle(Qt::BDiagPattern);
            break;
        case Qs::HorVerLineFill:
            brush.setStyle(Qt::CrossPattern);
            break;
        case Qs::FwBwLineFill:
            brush.setStyle(Qt::DiagCrossPattern);
            break;
        case Qs::NoFill:
            brush.setStyle(Qt::NoBrush);
            break;
    }
    brush.setColor(color);
    return brush;
}

void GraphicsWidget::addSceneItem(ObjectUid documentObjectUid)
{
    addSceneItem(m_document->objectAs<draw::DrawingObject>(documentObjectUid));
}

void GraphicsWidget::removeSceneItem(ObjectUid documentObjectUid)
{
    delete m_scene->takeGraphicsItem(documentObjectUid);
}

void GraphicsWidget::updateSceneItem(ObjectUid documentObjectUid)
{
    if (documentObjectUid == m_document->symbol()->uid())
        return;

    auto item = m_scene->graphicsItem(documentObjectUid);
    auto object = m_document->object(documentObjectUid);
    const auto ellipseObject = qobject_cast<const draw::Ellipse*>(object);
    const auto rectangleObject = qobject_cast<const draw::Rectangle*>(object);
    const auto portObject = qobject_cast<const sym::Port*>(object);
    const auto labelObject = qobject_cast<const draw::Label*>(object);
    const auto lineObject = qobject_cast<const draw::Line*>(object);
    if (ellipseObject  != nullptr) {
        auto ellipseItem = qgraphicsitem_cast<GraphicsEllipseItem *>(item);
        Q_ASSERT(ellipseItem != nullptr);
        updateEllipseItem(ellipseItem, ellipseObject);
    }
    else if (rectangleObject  != nullptr) {
        auto rectangleItem = qgraphicsitem_cast<GraphicsRectangleItem *>(item);
        Q_ASSERT(rectangleItem != nullptr);
        updateRectangleItem(rectangleItem, rectangleObject);
    }
    else if (portObject  != nullptr) {
        auto portItem = qgraphicsitem_cast<GraphicsPortItem *>(item);
        Q_ASSERT(portItem != nullptr);
        updatePortItem(portItem, portObject);
    }
    else if (lineObject  != nullptr) {
        auto lineItem = qgraphicsitem_cast<GraphicsLineItem *>(item);
        Q_ASSERT(lineItem != nullptr);
        updateLineItem(lineItem, lineObject);
    }
    else if (labelObject  != nullptr) {
        auto labelItem = qgraphicsitem_cast<GraphicsLabelItem *>(item);
        Q_ASSERT(labelItem != nullptr);
        updateLabelItem(labelItem, labelObject);
    }
    else
        qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
}

void GraphicsWidget::updateSceneSelectionFromDocumentSelection(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous)
{
    m_updatingSceneSelection = true;
    auto unchanged = current & previous;
    auto deselected = previous - current;
    auto selected = current - unchanged;
    for (const auto &uid: deselected)
        if (uid != m_document->symbol()->uid())
            m_scene->graphicsItem(uid)->setSelected(false);
    for (const auto &uid: selected)
        if (uid != m_document->symbol()->uid())
            m_scene->graphicsItem(uid)->setSelected(true);
    m_updatingSceneSelection = false;
}

void GraphicsWidget::updateDocumentSelectionFromSceneSelection()
{
    if (m_updatingSceneSelection)
        return;
    QSet<ObjectUid> selection;
    for (const auto item: m_scene->selectedItems()) {
        selection.insert(m_scene->uid(item));
    }
    m_document->select(selection);
}

void GraphicsWidget::loadTools()
{
    m_tools << m_selectTool
            << m_moveTool
            << m_placeLineTool
            << m_placeEllipseTool
            << m_placeRectangleTool
            << m_placeLabelTool
            << m_placePortTool;
}
