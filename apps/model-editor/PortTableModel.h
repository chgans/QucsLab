/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/LibraryObjects.h" // for enum
//class PortDefinition;

#include <QtCore/QAbstractTableModel>
#include <QtCore/QList>
#include <QtGui/QBrush>


class QValidator;

class PortTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum {
        NameSection = 0,
        DirectionSection,
        DocumentationSection,
        SectionCount
    };

    explicit PortTableModel(QObject *parent = 0);
    ~PortTableModel();

    void setPortList(const QList<lib::Port *> &list);
    QList<lib::Port *> portList() const;

    void setNameValidator(QValidator *validator);
    QValidator *nameValidator() const;

    void setInvalidItemBackgroundBrush(const QBrush &brush);
    QBrush invalidItemBackgroundBrush() const;

private:
    static QString toString(Qs::PortDirection direction);
    bool isValidName(const QString &name) const;
    QList<lib::Port *> m_ports;
    QValidator *m_nameValidator = nullptr;
    QBrush m_invalidItemBackgroundBrush;

public: //QTableModel interface
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
};
