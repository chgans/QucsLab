/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "FileSystemNavigationWidget.h"
#include "GraphicsWidget.h"
#include "MainWindow.h"

#include "qlCore/SymbolDocument.h"
#include "qlCore/SymbolReader.h"
#include "qlCore/SymbolWriter.h"

#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QToolBar>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_editor(new GraphicsWidget())
    , m_fsNavigationWidget(new FileSystemNavigationWidget())
    , m_fsNavigationDockWidget(new QDockWidget)
{
    createActions();
    createMenus();
    populateMenus();
    connectActions();
    setupFsNavigator();
    setCurrentEditor();
    updateWindowTitle();
}

MainWindow::~MainWindow()
{

}

void MainWindow::newRequested()
{
    closeRequested();
    m_editor->setDocument(new SymbolDocument());
}

void MainWindow::openRequested()
{
    QString filePath = QFileDialog::getOpenFileName(this,
                                                    "Open file",
                                                    DATADIR,
                                                    "Qucs Symbol (*.qsym)");
    if (filePath.isNull())
        return;

    closeRequested();

    m_filePath = filePath;
    m_editor->openDocument(m_filePath);
    updateWindowTitle();
}

void MainWindow::saveRequested()
{
    if (m_filePath.isEmpty())
        saveAsRequested();
    else
        m_editor->saveDocument(m_filePath);
    updateWindowTitle();
}

void MainWindow::saveAsRequested()
{
    QString filePath = QFileDialog::getSaveFileName(this,
                                                    "Save file",
                                                    DATADIR,
                                                    "Qucs Symbol (*.qsym)");
    if (filePath.isNull())
        return;

    if (!filePath.endsWith(".qsym"))
        filePath.append(".qsym");

    m_filePath = filePath;
    m_editor->saveDocument(m_filePath);
    updateWindowTitle();
}

void MainWindow::importRequested()
{
    QString filePath = QFileDialog::getOpenFileName(this,
                                                    "Open file",
                                                    DATADIR,
                                                    "Qucs Legacy Symbol (*.sym)");
    if (filePath.isNull())
        return;

    closeRequested();

    QucsSymbolReader reader;
    QFile file(filePath);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    if (!reader.readSymbol(&file)) {
        QMessageBox::critical(this, "Error", reader.errorString());
        return;
    }
    file.close();

    m_filePath = filePath;
    m_editor->setDocument(new SymbolDocument(reader.takeSymbol()));
    updateWindowTitle();
}

void MainWindow::exportRequested()
{
    QString filePath = QFileDialog::getSaveFileName(this,
                                                    "Export file",
                                                    DATADIR,
                                                    "Qucs Legacy Symbol (*.sym)");
    if (filePath.isNull())
        return;

    if (!filePath.endsWith(".sym"))
        filePath.append(".sym");

    QucsSymbolWriter writer;
    QFile file(filePath);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    if (!writer.writeSymbol(&file, m_editor->document()->symbol())) {
        QMessageBox::critical(this, "Error", writer.errorString());
        return;
    }
    if (file.error() != QFile::NoError) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    file.close();
}

void MainWindow::closeRequested()
{
    if (m_editor->isModified()) {
        auto result = QMessageBox::question(this,
                                            "Save changes?",
                                            "The current document has unsaved changes",
                                            QMessageBox::Save|QMessageBox::Discard|QMessageBox::Cancel);
        if (result == QMessageBox::Save)
            saveRequested();
        else if (result == QMessageBox::Cancel)
            return;
    }
    m_editor->closeDocument();
    m_filePath.clear();
    updateWindowTitle();
}

void MainWindow::exitRequested()
{
    closeRequested();
    QApplication::quit();
}

void MainWindow::undoRequested()
{
    m_editor->undo();
}

void MainWindow::redoRequested()
{
    m_editor->redo();
}

void MainWindow::cutRequested()
{
    m_editor->cut(QGuiApplication::clipboard());
}

void MainWindow::copyRequested()
{
    m_editor->copy(QGuiApplication::clipboard());
}

void MainWindow::pasteRequested()
{
    m_editor->paste(QGuiApplication::clipboard());
}

void MainWindow::moveRequested()
{
    m_editor->moveIt(); // QWidget::move(x, y)
}

void MainWindow::deleteRequested()
{
    m_editor->deleteIt(); // C++ delete
}

void MainWindow::selectAllRequested()
{
    m_editor->selectAll();
}

void MainWindow::clearSelectionRequested()
{
    m_editor->clearSelection();
}

void MainWindow::invertSelectionRequested()
{
    m_editor->invertSelection();
}

void MainWindow::preferencesRequested()
{

}

void MainWindow::fullscreenRequested()
{
    setWindowState(windowState() ^ Qt::WindowFullScreen);
}

void MainWindow::aboutRequested()
{
    QMessageBox::about(this, QString("About %1").arg(qApp->applicationName()),
                       QString("<h2>This is <b>%1 v%2</b></h2>\n"
                               "<p><i>%1</i> is a simple application that allows to draw schematic symbols "
                               "to be used with <i>Qucs</i>, the Quite Universal Circuit Simulator.</p>"
                               "<p>For more infomtations, see "
                               "<a href='https://gitlab.com/chgans/QucsLab/'>https://gitlab.com/chgans/QucsLab/</a> "
                               "and <a href='http://qucs.sourceforge.net/'>http://qucs.sourceforge.net/</a></p>"
                               "<pre>Copyright (C) 2017 Christian Gagneraud &lt;chgans AT gna DOT org&gt;</pre>"
                               "<p>This program is licensed under the terms of the "
                               "<a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU GPL</a>.</p>"
                               ).arg(qApp->applicationName(),
                                     qApp->applicationVersion()));
}

void MainWindow::aboutQtRequested()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::toolBarsSubMenuRequested()
{
    m_toolBarsMenu->clear();
    for (auto toolBar: toolBars()) {
        auto action = new QAction(toolBar->windowTitle());
        action->setCheckable(true);
        action->setChecked(toolBar->isVisible());
        connect(action, &QAction::triggered,
                this, [this, toolBar](bool checked) { toolBar->setVisible(checked); });
        m_toolBarsMenu->addAction(action);
    }
}

void MainWindow::dockWidgetsSubMenuRequested()
{
    m_dockWidgetsMenu->clear();
    for (auto dockWidget: dockWidgets()) {
        auto action = new QAction(dockWidget->windowTitle());
        action->setCheckable(true);
        action->setChecked(dockWidget->isVisible());
        connect(action, &QAction::triggered,
                this, [this, dockWidget](bool checked) { dockWidget->setVisible(checked); });
        m_dockWidgetsMenu->addAction(action);
    }
}

void MainWindow::updateWindowTitle()
{
    setWindowFilePath(m_filePath);
    setWindowModified(m_editor->isModified());
}

void MainWindow::openDocument(const QString &path)
{
    closeRequested();
    m_filePath = path;
    m_editor->openDocument(m_filePath);
    updateWindowTitle();
}

void MainWindow::createMenus()
{
    m_fileMenu = menuBar()->addMenu("&File");
    m_editMenu = menuBar()->addMenu("&Edit");
    m_placeMenu = menuBar()->addMenu("&Place");
    m_viewMenu = menuBar()->addMenu("&View");
    m_toolsMenu = menuBar()->addMenu("&Tools");
    m_windowMenu = menuBar()->addMenu("&Window");
    m_helpMenu = menuBar()->addMenu("&Help");
    m_toolBarsMenu = new QMenu();
    m_dockWidgetsMenu = new QMenu();
}

void MainWindow::createActions()
{
    m_newAction = new QAction(QIcon::fromTheme("document-new"), "&New");
    m_newAction->setShortcut(QKeySequence::New);
    m_openAction = new QAction(QIcon::fromTheme("document-open"), "&Open...");
    m_openAction->setShortcut(QKeySequence::Open);
    m_saveAction = new QAction(QIcon::fromTheme("document-save"), "&Save");
    m_saveAction->setShortcut(QKeySequence::Save);
    m_saveAsAction = new QAction(QIcon::fromTheme("document-save-as"), "Save &as ...");
    m_saveAsAction->setShortcut(QKeySequence::SaveAs);
    m_importAction = new QAction("&Import ...");
    m_exportAction = new QAction("&Export ...");
    m_closeAction = new QAction(QIcon::fromTheme("document-close"), "&Close");
    m_closeAction->setShortcut(QKeySequence::Close);
    m_exitAction = new QAction(QIcon::fromTheme("application-exit"), "E&xit");
    m_exitAction->setShortcut(QKeySequence::Quit);

    m_undoAction = new QAction(QIcon::fromTheme("edit-undo"), "&Undo");
    m_undoAction->setShortcut(QKeySequence::Undo);
    m_redoAction = new QAction(QIcon::fromTheme("edit-redo"), "&Redo");
    m_redoAction->setShortcut(QKeySequence::Redo);
    m_cutAction = new QAction(QIcon::fromTheme("edit-cut"), "Cu&t");
    m_cutAction->setShortcut(QKeySequence::Cut);
    m_copyAction = new QAction(QIcon::fromTheme("edit-copy"), "&Copy");
    m_copyAction->setShortcut(QKeySequence::Copy);
    m_pasteAction = new QAction(QIcon::fromTheme("edit-paste"), "&Paste");
    m_pasteAction->setShortcut(QKeySequence::Paste);
    //    m_moveAction = new QAction(QIcon::fromTheme("transform-move"), "&Move");
    //    m_moveAction->setShortcut(QKeySequence("m"));
    m_deleteAction = new QAction(QIcon::fromTheme("edit-delete"), "&Delete");
    m_deleteAction->setShortcut(QKeySequence::Delete);
    m_selectAllAction = new QAction(QIcon::fromTheme("edit-select-all"), "Select &all");
    m_selectAllAction->setShortcut(QKeySequence("Ctrl+a"));
    m_clearSelectionAction = new QAction(QIcon::fromTheme("edit-select-none"), "Clea&r selection");
    m_clearSelectionAction->setShortcut(QKeySequence("Ctrl+shift+a"));
    m_inverseSelectionAction = new QAction(QIcon::fromTheme("edit-select-invert"), "&Invert selection");
    m_inverseSelectionAction->setShortcut(QKeySequence("Ctrl+i"));

    m_preferencesAction = new QAction("&Options...");
    m_preferencesAction->setShortcut(QKeySequence::Preferences);

    m_toolBarsAction = new QAction("Tool bars");
    m_dockWidgetsAction = new QAction("Docks");
    m_fullscreenAction = new QAction(QIcon::fromTheme("view-fullscreen"), "Full screen");
    m_fullscreenAction->setShortcut(QKeySequence::FullScreen);
    m_fullscreenAction->setCheckable(true);

    m_aboutAction = new QAction(QIcon::fromTheme("help-about"), "About...");
    m_aboutQtAction = new QAction(QIcon::fromTheme("help-about"), "About Qt...");

}

void MainWindow::populateMenus()
{
    m_fileMenu->addAction(m_newAction);
    m_fileMenu->addAction(m_openAction);
    m_fileMenu->addAction(m_saveAction);
    m_fileMenu->addAction(m_saveAsAction);
    m_fileMenu->addAction(m_closeAction);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_importAction);
    m_fileMenu->addAction(m_exportAction);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_exitAction);

    m_editMenu->addAction(m_undoAction);
    m_editMenu->addAction(m_redoAction);
    m_editMenu->addSeparator();
    m_editMenu->addAction(m_cutAction);
    m_editMenu->addAction(m_copyAction);
    m_editMenu->addAction(m_pasteAction);
    m_editMenu->addSeparator();
    m_editMenu->addAction(m_selectAllAction);
    m_editMenu->addAction(m_clearSelectionAction);
    m_editMenu->addAction(m_inverseSelectionAction);
    m_editMenu->addSeparator();
    //m_editMenu->addAction(m_moveAction);
    m_editMenu->addAction(m_deleteAction);

    m_toolBarsAction->setMenu(m_toolBarsMenu);
    m_dockWidgetsAction->setMenu(m_dockWidgetsMenu);

    m_toolsMenu->addSeparator();
    m_toolsMenu->addAction(m_preferencesAction);

    m_windowMenu->addSeparator();
    m_windowMenu->addAction(m_toolBarsAction);
    m_windowMenu->addAction(m_dockWidgetsAction);
    m_windowMenu->addSeparator();
    m_windowMenu->addAction(m_fullscreenAction);

    m_helpMenu->addSeparator();
    m_helpMenu->addAction(m_aboutAction);
    m_helpMenu->addAction(m_aboutQtAction);
}

void MainWindow::setupFsNavigator()
{
    m_fsNavigationDockWidget->setObjectName("org.qucs.symboleditor.filesystem");
    m_fsNavigationDockWidget->setWidget(m_fsNavigationWidget);
    m_fsNavigationDockWidget->setWindowTitle("File system");
    m_fsNavigationWidget->setInitialPath(DATADIR);
    addDockWidget(Qt::LeftDockWidgetArea, m_fsNavigationDockWidget);
    connect(m_fsNavigationWidget, &FileSystemNavigationWidget::fileActivated,
            this, &MainWindow::openDocument);
}

void MainWindow::addPlacementActions(const QList<QAction *> &actions)
{
    m_placeMenu->addActions(actions);
}

void MainWindow::addEditActions(const QList<QAction *> &actions)
{
    m_editMenu->addSeparator();
    m_editMenu->addActions(actions);
}

void MainWindow::addViewActions(const QList<QAction *> &actions)
{
    m_viewMenu->addActions(actions);
}

void MainWindow::addToolBars(const QList<QToolBar *> &toolBars)
{
    for (auto toolBar: toolBars)
        addToolBar(toolBar);
}

void MainWindow::addDockWidgets(const QList<QDockWidget *> &dockWidgets)
{
    for (auto dockWidget: dockWidgets)
        addDockWidget(Qt::RightDockWidgetArea, dockWidget);
}

// TBD: Could use a slot name convetion to automate these
// Or a proxy/wrapper/bridge something
void MainWindow::connectActions()
{
    connect(m_newAction, &QAction::triggered,
            this, &MainWindow::newRequested);
    connect(m_openAction, &QAction::triggered,
            this, &MainWindow::openRequested);
    connect(m_saveAction, &QAction::triggered,
            this, &MainWindow::saveRequested);
    connect(m_saveAsAction, &QAction::triggered,
            this, &MainWindow::saveAsRequested);
    connect(m_closeAction, &QAction::triggered,
            this, &MainWindow::closeRequested);
    connect(m_importAction, &QAction::triggered,
            this, &MainWindow::importRequested);
    connect(m_exportAction, &QAction::triggered,
            this, &MainWindow::exportRequested);
    connect(m_exitAction, &QAction::triggered,
            this, &MainWindow::exitRequested);

    connect(m_undoAction, &QAction::triggered,
            this, &MainWindow::undoRequested);
    connect(m_redoAction, &QAction::triggered,
            this, &MainWindow::redoRequested);
    connect(m_cutAction, &QAction::triggered,
            this, &MainWindow::cutRequested);
    connect(m_copyAction, &QAction::triggered,
            this, &MainWindow::copyRequested);
    connect(m_pasteAction, &QAction::triggered,
            this, &MainWindow::pasteRequested);
    //    connect(m_moveAction, &QAction::triggered,
    //            this, &MainWindow::moveRequested);
    connect(m_deleteAction, &QAction::triggered,
            this, &MainWindow::deleteRequested);
    connect(m_selectAllAction, &QAction::triggered,
            this, &MainWindow::selectAllRequested);
    connect(m_clearSelectionAction, &QAction::triggered,
            this, &MainWindow::clearSelectionRequested);
    connect(m_inverseSelectionAction, &QAction::triggered,
            this, &MainWindow::invertSelectionRequested);

    connect(m_toolBarsMenu, &QMenu::aboutToShow,
            this, &MainWindow::toolBarsSubMenuRequested);
    connect(m_toolBarsMenu, &QMenu::aboutToShow,
            this, &MainWindow::dockWidgetsSubMenuRequested);
    connect(m_preferencesAction, &QAction::triggered,
            this, &MainWindow::preferencesRequested);
    connect(m_fullscreenAction, &QAction::triggered,
            this, &MainWindow::fullscreenRequested);
    connect(m_aboutAction, &QAction::triggered,
            this, &MainWindow::aboutRequested);
    connect(m_aboutQtAction, &QAction::triggered,
            this, &MainWindow::aboutQtRequested);
}

QList<QToolBar *> MainWindow::toolBars() const
{
    return findChildren<QToolBar*>();
}

QList<QDockWidget *> MainWindow::dockWidgets() const
{
    return findChildren<QDockWidget*>();
}

void MainWindow::setCurrentEditor(/* IEditor *editor */)
{
    m_editor->setDocument(new SymbolDocument()); // FIXME
    addEditActions(m_editor->editActions());
    addViewActions(m_editor->viewActions());
    addPlacementActions(m_editor->placementActions());
    addToolBars(m_editor->toolBars());
    addDockWidgets(m_editor->dockWidgets());
    setCentralWidget(m_editor);

    // Initial zoom level, need to execute at next event loop
    // so that the editor has a size
    auto timer = new QTimer(this);
    connect(timer, &QTimer::timeout,
            this, [this, timer]() {
        m_editor->zoomToBox(QRectF(-10, -10, 20, 20), 0);
        timer->deleteLater();
    });
    timer->start(100); // FIXME: b/c window animation!

    m_undoAction->setEnabled(m_editor->canUndo());
    m_redoAction->setEnabled(m_editor->canRedo());
    m_copyAction->setEnabled(m_editor->canCopy());
    m_cutAction->setEnabled(m_editor->canCut());
    m_pasteAction->setEnabled(m_editor->canPaste());
    //m_moveAction->setEnabled(m_editor->canMove());
    m_deleteAction->setEnabled(m_editor->canDelete());
    m_selectAllAction->setEnabled(m_editor->canSelectAll());
    m_clearSelectionAction->setEnabled(m_editor->canClearSelection());
    m_inverseSelectionAction->setEnabled(m_editor->canInvertSelection());

    // FIXME: see connectActions()
    connect(m_editor, &GraphicsWidget::canUndoChanged,
            m_undoAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canRedoChanged,
            m_redoAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canCopyChanged,
            m_copyAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canCutChanged,
            m_cutAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canPasteChanged,
            m_pasteAction, &QAction::setEnabled);
    //    connect(m_editor, &GraphicsWidget::canMoveChanged,
    //            m_moveAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canDeleteChanged,
            m_deleteAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canSelectAllChanged,
            m_selectAllAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canClearSelectionChanged,
            m_clearSelectionAction, &QAction::setEnabled);
    connect(m_editor, &GraphicsWidget::canInvertSelectionChanged,
            m_inverseSelectionAction, &QAction::setEnabled);

    connect(m_editor, &GraphicsWidget::isModifiedChanged,
            this, &MainWindow::updateWindowTitle);
}

// FIXME: Redundant with closeRequested()
void MainWindow::closeEvent(QCloseEvent *event)
{
    if (m_editor->isModified()) {
        auto result = QMessageBox::question(this,
                                            "Save changes?",
                                            "The current document has unsaved changes",
                                            QMessageBox::Save|QMessageBox::Discard|QMessageBox::Cancel);
        if (result == QMessageBox::Save)
            saveRequested();
        else if (result == QMessageBox::Cancel) {
            event->ignore();
            return;
        }
    }
    m_editor->closeDocument();
    event->accept();
}
