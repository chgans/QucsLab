/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/Object.h"
#include "qlCore/QlCore.h"
#include "qlGui/GraphicsEventHandler.h"

#include <QtCore/QObject>
#include <QtCore/QScopedPointer>
#include <QtGui/QPen>
#include <QtGui/QBrush>

class GraphicsEllipseItem;
class GraphicsItemFactory;
class GraphicsHandle;
class GraphicsLabelItem;
class GraphicsLineItem;
class GraphicsRectangleItem;
class GraphicsPortItem;
class GraphicsView;
class GraphicsWidget;
class ObjectPropertyBrowser;
class SymbolDocument;
class UndoCommand;

class QAction;
class QActionGroup;
class QGraphicsItem;
class QGraphicsItemGroup;
class QGraphicsSceneMouseEvent;
class QRubberBand;

// TODO: Bundle tools into a GraphicsItemToolProvider
// - Tool metadata (icons, ...)
// - createPlacementTool()
// - createEditingTool()
//
// See comment in GraphicsEventHandler
//
// TBD: single shot vs auto-repeat, tools need a finished and cancelled signals
// TBD: interactive vs non-interactive
//
// Missing tools:
//  - clone
//  - align objects (GraphicsItem need to know if they are the first and/or last in the selection)
//  - snap object to grid
//  - group/ungroup

class GraphicsTool : public QObject, public GraphicsSceneEventHandler
{
    Q_OBJECT

public:
    explicit GraphicsTool(QObject *parent = 0);
    ~GraphicsTool();

    // TODO: properties of the tool bundle
    virtual QString caption() const = 0;
    virtual QString description() const = 0;
    virtual QKeySequence shortcut() const = 0;
    virtual QIcon icon() const = 0;

    // TBD: Might want to separate attach/detach from activate/reactivate/desactivate
    // TODO: let the tool get the current pos/snappedPos from the editor
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) = 0;
    virtual void desactivate() = 0;

    bool hasUndoCommand() const;
    UndoCommand *takeUndoCommand();

    QWidget *optionWidget() const;

public slots:

signals:
    void undoCommandAvailable();
    void finished();

protected:
    void setUndoCommand(UndoCommand *command);
    void setOptionWidget(QWidget *widget);

private:
    UndoCommand *m_command = nullptr;
    QWidget *m_optionWidget = nullptr;
};

class GraphicsSelectToolOptionWidget;
class GraphicsSelectTool: public GraphicsTool
{
    Q_OBJECT

public:
    explicit GraphicsSelectTool(QObject *parent = 0);
    ~GraphicsSelectTool();

    QList<QAction *> quickActions() const;

private:
    enum State {
        ClickSelect = 0,
        DragSelect,
        RubberBandSelect,
        DragHandle
    };
    enum ButtonState {
        ButtonDown = 0,
        ButtonUp
    };

    GraphicsView *m_view = nullptr;
    GraphicsScene *m_scene = nullptr;
    GraphicsItemFactory *m_factory = nullptr;
    SymbolDocument *m_document;
    QRubberBand *m_rubberBand = nullptr;
    QActionGroup *m_quickActionGroup = nullptr;
    GraphicsSelectToolOptionWidget *m_toolWidget = nullptr;
    State m_state = ClickSelect;
    ButtonState m_buttonState = ButtonUp;
    QPointF m_buttonDownPos;
    int m_dragThreshold = 5; // pixels
    GraphicsHandle *m_handle;
    QPointF m_originalHandlePos;

    void setupQuickActions();
    void setupOptionWidget();
    void setupRubberBand();

    bool canSelect(const QPointF &pos);
    void maybeAddToOrClearSelection(const QPointF &pos);
    void maybeRemoveFromOrClearSelection(const QPointF &pos);
    void maybeSetOrClearSelection(const QPointF &pos);

    bool canStartDrag(const QPointF &pos);

    void startRubberBandDrag(const QPointF &pos);
    void updateRubberBand(const QPointF &pos);
    void endRubberBand(const QPointF &pos);

    void startSelectionDrag(const QPointF &pos);
    void updateDragSelect(const QPointF &pos);
    void endDragSelect(const QPointF &pos);

    bool canDragHandle(const QPointF &pos);
    void startHandleDrag(const QPointF &pos);
    void updateHandle(const QPointF &pos);
    void endDragHandle(const QPointF &pos);

    void setState(State state);

private slots:
    void sendSelectionToBack();
    void lowerSelection();
    void raiseSelection();
    void bringSelectionToFront();


    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class GraphicsMoveTool: public GraphicsTool
{
    Q_OBJECT

public:
    explicit GraphicsMoveTool(QObject *parent = 0);
    ~GraphicsMoveTool();

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;

private:
    enum State {
        WaitForPick = 0,
        WaitForPlace
    };
    State m_state = WaitForPick;
    GraphicsView *m_view;
    QList<QGraphicsItem *> m_items;
    QList<ObjectUid> m_uids;
    QList<QPointF> m_deltaPickPositions;
    QList<QPointF> m_originalItemPositions;
    bool checkForPick(const QPointF &pos);
    void pick(const QPointF &pos);
    bool hasPicked();
    void unpick();
    void place(const QPointF &pos);
    void move(const QPointF &pos);
};

class GraphicsCloneTool: public GraphicsTool
{
    Q_OBJECT

public:
    explicit GraphicsCloneTool(QObject *parent = 0);
    ~GraphicsCloneTool();

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;

private:
    enum State {
        WaitForPick = 0,
        WaitForPlace
    };
    State m_state = WaitForPick;
    GraphicsView *m_view;
    GraphicsItemFactory *m_factory;
    QList<QGraphicsItem *> m_items;
    QList<QGraphicsItem *> m_clones;
    QList<ObjectUid> m_uids;
    QList<QPointF> m_deltaPickPositions;
    QList<QPointF> m_originalItemPositions;
    bool checkForPick(const QPointF &pos);
    void pick(const QPointF &pos);
    bool hasPicked();
    void unpick();
    void place(const QPointF &pos);
    void move(const QPointF &pos);
};

class PlaceItemsTool: public GraphicsTool
{
public:
    explicit PlaceItemsTool(QObject *parent = 0);
    ~PlaceItemsTool();

    void setItems(QList<QGraphicsItem *> &items);

private:
    void pushCommand();
    QScopedPointer<QGraphicsItem> m_itemGroup;
    QPointF m_referencePos;
    GraphicsWidget *m_editor = nullptr;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlaceEllipseTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
    Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
    Q_PROPERTY(Qs::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
    Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

    Q_PROPERTY(Qs::EllipseStyle style READ style WRITE setStyle NOTIFY styleChanged)
    // TODO: bool ellipsoidal
public:
    explicit PlaceEllipseTool(QObject *parent = 0);
    ~PlaceEllipseTool();

    Qs::EllipseStyle style() const;
    Qs::StrokeStyle strokeStyle() const;
    Qs::StrokeWidth strokeWidth() const;
    QColor strokeColor() const;
    Qs::FillStyle fillStyle() const;
    QColor fillColor() const;

public slots:
    void setStyle(Qs::EllipseStyle style);
    void setStrokeStyle(Qs::StrokeStyle strokeStyle);
    void setStrokeWidth(Qs::StrokeWidth strokeWidth);
    void setStrokeColor(QColor strokeColor);
    void setFillStyle(Qs::FillStyle fillStyle);
    void setFillColor(QColor fillColor);

signals:
    void styleChanged(Qs::EllipseStyle style);
    void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
    void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
    void strokeColorChanged(QColor strokeColor);
    void fillStyleChanged(Qs::FillStyle fillStyle);
    void fillColorChanged(QColor fillColor);

private:
    void pushCommand();
    void restart();
    enum State {
        WaitForCenter,
        WaitForRadius,
        WaitForStartAngle,
        WaitForSpanAngle
    };
    GraphicsView *m_view = nullptr;
    GraphicsItemFactory *m_factory = nullptr;
    QScopedPointer<GraphicsEllipseItem> m_item;
    State m_state;
    Qs::StrokeStyle m_strokeStyle;
    Qs::StrokeWidth m_strokeWidth;
    QColor m_strokeColor;
    Qs::FillStyle m_fillStyle;
    QColor m_fillColor;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};


class PlaceRectangleTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
    Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
    Q_PROPERTY(Qs::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
    Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

public:
    explicit PlaceRectangleTool(QObject *parent = 0);
    ~PlaceRectangleTool();

    Qs::StrokeStyle strokeStyle() const;
    Qs::StrokeWidth strokeWidth() const;
    QColor strokeColor() const;
    Qs::FillStyle fillStyle() const;
    QColor fillColor() const;

public slots:
    void setStrokeStyle(Qs::StrokeStyle strokeStyle);
    void setStrokeWidth(Qs::StrokeWidth strokeWidth);
    void setStrokeColor(QColor strokeColor);
    void setFillStyle(Qs::FillStyle fillStyle);
    void setFillColor(QColor fillColor);

signals:
    void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
    void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
    void strokeColorChanged(QColor strokeColor);
    void fillStyleChanged(Qs::FillStyle fillStyle);
    void fillColorChanged(QColor fillColor);

private:
    void pushCommand();
    enum State {
        WaitForCenter,
        WaitForCorner,
    };
    GraphicsView *m_view = nullptr;
    GraphicsItemFactory *m_factory = nullptr;
    QScopedPointer<GraphicsRectangleItem> m_item;
    State m_state = WaitForCenter;
    Qs::StrokeStyle m_strokeStyle;
    Qs::StrokeWidth m_strokeWidth;
    QColor m_strokeColor;
    Qs::FillStyle m_fillStyle;
    QColor m_fillColor;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlacePortTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(bool autoIncrement READ autoIncrement WRITE setAutoIncrement NOTIFY autoIncrementChanged)
    Q_PROPERTY(int sequenceNumber READ sequenceNumber WRITE setSequenceNumber NOTIFY sequenceNumberChanged)

public:
    explicit PlacePortTool(QObject *parent = 0);
    ~PlacePortTool();

    bool autoIncrement() const;
    int sequenceNumber() const;

public slots:
    void setAutoIncrement(bool autoIncrement);
    void setSequenceNumber(int sequenceNumber);

signals:
    void autoIncrementChanged(bool autoIncrement);
    void sequenceNumberChanged(int sequenceNumber);

private:
    void pushCommand();
    GraphicsView *m_view = nullptr;
    GraphicsItemFactory *m_factory = nullptr;
    QScopedPointer<GraphicsPortItem> m_item;
    bool m_autoIncrement = true;
    int m_sequenceNumber = 0;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlaceLineTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
    Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)

public:
    explicit PlaceLineTool(QObject *parent = 0);
    ~PlaceLineTool();

    Qs::StrokeStyle strokeStyle() const;
    Qs::StrokeWidth strokeWidth() const;
    QColor strokeColor() const;

public slots:
    void setStrokeStyle(Qs::StrokeStyle strokeStyle);
    void setStrokeWidth(Qs::StrokeWidth strokeWidth);
    void setStrokeColor(QColor strokeColor);

signals:
    void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
    void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
    void strokeColorChanged(QColor strokeColor);

private:
    enum State {
        WaitForP1,
        WaitForP2,
    };
    void pushCommand();
    GraphicsView *m_view = nullptr;
    GraphicsItemFactory *m_factory = nullptr;
    QScopedPointer<GraphicsLineItem> m_item;
    State m_state = WaitForP1;
    Qs::StrokeStyle m_strokeStyle;
    Qs::StrokeWidth m_strokeWidth;
    QColor m_strokeColor;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};


class PlaceLabelTool: public GraphicsTool
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)

public:
    explicit PlaceLabelTool(QObject *parent = 0);
    ~PlaceLabelTool();

    QColor color() const;
    int fontSize() const;

public slots:
    void setColor(QColor color);
    void setFontSize(int fontSize);

signals:
    void colorChanged(QColor color);
    void fontSizeChanged(int fontSize);

private:
    void pushCommand();
    GraphicsView *m_view;
    GraphicsItemFactory *m_factory = nullptr;
    QScopedPointer<GraphicsLabelItem> m_item;
    QColor m_color;
    int m_fontSize;

    // GraphicsTool interface
public:
    virtual void activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};
