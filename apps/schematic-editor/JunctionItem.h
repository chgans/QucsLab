#ifndef JUNCTIONITEM_H
#define JUNCTIONITEM_H

#include <QtWidgets/QGraphicsItem>

class JunctionItem : public QGraphicsItem
{
public:
    enum {
        Type = UserType + 0x1000 + 0x02,
    };

    explicit JunctionItem(QGraphicsItem *parent = nullptr);
    ~JunctionItem();

private:
    QColor m_color; // TODO: use a stye enum
    const qreal m_size;
    const QRectF m_rect;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual int type() const override { return Type; }

    // QGraphicsItem interface
public:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
};

#endif // JUNCTIONITEM_H
