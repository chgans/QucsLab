#ifndef SYMBOLITEM_H
#define SYMBOLITEM_H

#include "AttributeItem.h"

#include "qlCore/QlCore.h"

#include <QtGui/QPen>
#include <QtWidgets/QGraphicsItem>

namespace sym {
    class Symbol;
}

namespace sch {
    class NetTie;
}

class SymbolItem : public QGraphicsItem, public AttributeItemContainer
{
public:
    enum {
        Type = UserType + 0x1000 + 0x03
    };

    SymbolItem(QGraphicsItem *parent = nullptr);
    ~SymbolItem();

    void setSymbol(sym::Symbol *symbol);

private:
    QScopedPointer<sym::Symbol> m_symbol;
    QRectF m_boundingRect;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // QGraphicsItem interface
public:
    virtual int type() const override;
};

class NetTieItem: public QGraphicsItem, public AttributeItemContainer
{
public:
    NetTieItem(QGraphicsItem *parent = nullptr);
    ~NetTieItem();

    void setTieStyle(Qs::TieStyle style);

private:
    QPen m_pen;
    Qs::TieStyle m_style = Qs::GroundTie;
    QPainterPath m_path;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
};

class SheetPortItem: public QGraphicsItem, public AttributeItemContainer
{
public:
    SheetPortItem(QGraphicsItem *parent = nullptr);
    ~SheetPortItem();

    void setPortStyle(Qs::PortStyle style);

private:
    QPen m_pen;
    Qs::PortStyle m_style = Qs::CirclePort;
    QPainterPath m_path;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
};

class DirectiveItem: public QGraphicsItem, public AttributeItemContainer
{
public:
    DirectiveItem(QGraphicsItem *parent = nullptr);
    ~DirectiveItem();

    void setText(const QString &text);

private:
    QPen m_pen;
    QPainterPath m_path;
    QGraphicsTextItem *m_textItem;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
};

class DiagramItem: public QGraphicsItem, public AttributeItemContainer
{
public:
    DiagramItem(QGraphicsItem *parent = nullptr);
    ~DiagramItem();

    void setText(const QString &text);
    void setSize(const QSizeF &size);

private:
    QPen m_pen;
    QSizeF m_size;
    QGraphicsTextItem *m_textItem;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
};

#endif // SYMBOLITEM_H
