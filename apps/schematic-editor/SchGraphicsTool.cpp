#include "JunctionItem.h"
#include "SchEditor.h"
#include "SchGraphicsTool.h"
#include "WireItem.h"
#include "SymbolItem.h"
#include "ConfigurationItemDelegate.h"
#include "ConfigurationTableModel.h"

#include "qlCore/LibraryManager.h"
#include "qlCore/LibraryObjects.h"
#include "qlCore/SymbolObjects.h"
#include "qlCore/SchematicObjects.h"
#include "qlGui/GraphicsScene.h"
#include "qlGui/GraphicsView.h"

#include <QtCore/QDebug>
#include <QtGui/QIcon>
#include <QtGui/QKeySequence>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QGraphicsOpacityEffect>

#include <cmath>

SchGraphicsTool::SchGraphicsTool(QObject *parent)
    : QObject(parent)
    , GraphicsSceneEventHandler()
{

}

SchGraphicsTool::~SchGraphicsTool()
{

}


SelectTool::SelectTool(QObject *parent)
    : SchGraphicsTool(parent)
{

}

SelectTool::~SelectTool()
{

}

QString SelectTool::caption() const
{
    return "Select";
}

QString SelectTool::description() const
{
    return "Selection and manipulation of objects";
}

QKeySequence SelectTool::shortcut() const
{
    return QKeySequence(Qt::Key_Escape);
}

QIcon SelectTool::icon() const
{
    return QIcon::fromTheme("edit-select");
}

void SelectTool::activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(pos);
    Q_UNUSED(sourcePos);
    m_editor = editor;
    m_editor->scene()->installEventHandler(this);
}

void SelectTool::desactivate()
{
    m_editor->scene()->uninstallEventHandler(this);
}

// FIXME: The event handler should just trigger an action "Edit component"

#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QVBoxLayout>
#include "ConfigurationTableWidget.h"

bool SelectTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    auto item = m_editor->scene()->itemAt(event->scenePos(), m_editor->view()->transform());
    auto symbol = qgraphicsitem_cast<SymbolItem*>(item);
    if (symbol == nullptr)
        return false;
    auto data = symbol->data(0);
    Q_ASSERT(data.canConvert<sch::Component*>());
    auto schObject = data.value<sch::Component*>();
    Q_ASSERT(schObject != nullptr);

    auto libManager = m_editor->libraryManager();
    auto libObject = libManager->cellByUid(schObject->componentName); // FIXME: We should have the full uid

    QDialog dialog;
    dialog.setWindowTitle(QString("%1 properties (%2)").arg(schObject->designator, libObject->identification.caption));
    dialog.setLayout(new QVBoxLayout());
    auto widget = new ConfigurationTableWidget();
    widget->setCellInstance(schObject, libObject);
    dialog.layout()->addWidget(widget);
    auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel | QDialogButtonBox::Ok | QDialogButtonBox::Apply);
    dialog.layout()->addWidget(buttonBox);
    dialog.exec();

    return true;
}


PlaceWireTool::PlaceWireTool(QObject *parent)
    : SchGraphicsTool(parent)
{

}

PlaceWireTool::~PlaceWireTool()
{

}


QString PlaceWireTool::caption() const
{
    return "Wire";
}

QString PlaceWireTool::description() const
{
    return "Place wires";
}

QKeySequence PlaceWireTool::shortcut() const
{
    return QKeySequence("w");
}

QIcon PlaceWireTool::icon() const
{
    return QIcon::fromTheme("standard-connector"); //("node-segment-line");
}

void PlaceWireTool::activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(pos);
    Q_UNUSED(sourcePos);
    m_editor = editor;
    m_editor->scene()->installEventHandler(this);
    m_state = WaitForFirstPoint;
}

void PlaceWireTool::desactivate()
{
    auto points = m_wire->points();
    points.removeLast();
    points.removeLast();
    m_wire->setPoints(points);
    //m_editor->scene()->removeItem(m_wire.data());
    m_editor->scene()->uninstallEventHandler(this);
}

PlaceWireTool::Quadrant PlaceWireTool::quadrant(const QPointF &p1, const QPointF &p2)
{
    const QLineF line(p1, p2);
    const qreal sin = std::sin(line.angle());
    const qreal cos = std::cos(line.angle());
    if (cos>=0 && sin>=0)
        return Quadrant1;
    if (cos<0 && sin>=0)
        return Quadrant2;
    if (cos<0 && sin<0)
        return Quadrant3;
    if (cos>=0 && sin<0)
        return Quadrant4;
    Q_UNREACHABLE();
}

void PlaceWireTool::newWire(const QPointF &pos)
{
    m_wire.take();
    m_wire.reset(new WireItem());
    m_wire->addPoint(pos);
    m_wire->addPoint(pos);
    m_wire->addPoint(pos);
    m_editor->scene()->addItem(m_wire.data());
    m_state = WaitForNextPoint;
}

void PlaceWireTool::newPoint(const QPointF &pos)
{
    m_wire->movePoint(pos, -1);
    m_wire->addPoint(pos);
    m_wire->addPoint(pos);
}


bool PlaceWireTool::graphicsSceneKeyPressEvent(GraphicsScene *scene, QKeyEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}

bool PlaceWireTool::graphicsSceneKeyReleaseEvent(GraphicsScene *scene, QKeyEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}

bool PlaceWireTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    m_state = WaitForFirstPoint;
    return true;
}

bool PlaceWireTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    if (event->flags() & Qt::MouseEventCreatedDoubleClick)
        return true;

    if (m_state == WaitForFirstPoint)
        newWire(event->scenePos());
    else
        newPoint(event->scenePos());

    return true;
}

bool PlaceWireTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (m_state == WaitForFirstPoint)
        return true;

    m_wire->movePoint(event->scenePos(), -1);

    const auto p1 = m_wire->point(-3);
    const auto p3 = m_wire->point(-1);
    const auto p2 = QPointF(p3.x(), p1.y());
    m_wire->movePoint(p2, -2);

    return true;
}

bool PlaceWireTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}


PlaceJunctionTool::PlaceJunctionTool(QObject *parent)
    : SchGraphicsTool(parent)
{

}

PlaceJunctionTool::~PlaceJunctionTool()
{

}

QString PlaceJunctionTool::caption() const
{
    return "Junction";
}

QString PlaceJunctionTool::description() const
{
    return "Place electrical junctions";
}

QKeySequence PlaceJunctionTool::shortcut() const
{
    return QKeySequence("j");
}

QIcon PlaceJunctionTool::icon() const
{
    return QIcon::fromTheme("snap-node-intersection");
}

void PlaceJunctionTool::activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);
    m_editor = editor;
    m_editor->scene()->installEventHandler(this);
    newItem(pos);
}

void PlaceJunctionTool::desactivate()
{
    m_editor->scene()->uninstallEventHandler(this);
    delete m_item;
    m_item=nullptr;
}

void PlaceJunctionTool::newItem(const QPointF &pos)
{
    m_item = new JunctionItem();
    m_item->setPos(pos);
    m_editor->scene()->addItem(m_item);
}

bool PlaceJunctionTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceJunctionTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    newItem(event->scenePos());
    return true;
}

bool PlaceJunctionTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_item->setPos(event->scenePos());
    return false;
}

bool PlaceJunctionTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

PlaceSymbolTool::PlaceSymbolTool(QObject *parent)
    : SchGraphicsTool(parent)
{

}

PlaceSymbolTool::~PlaceSymbolTool()
{

}

void PlaceSymbolTool::setSymbol(sym::Symbol *symbol)
{
    m_symbol = symbol;
}

QString PlaceSymbolTool::caption() const
{
    return "Symbol";
}

QString PlaceSymbolTool::description() const
{
    return "Place a symbol on the sheet";
}

QKeySequence PlaceSymbolTool::shortcut() const
{
    return QKeySequence();
}

QIcon PlaceSymbolTool::icon() const
{
    return QIcon::fromTheme("component");
}

void PlaceSymbolTool::activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(pos);
    Q_UNUSED(sourcePos);

    m_editor = editor;
    m_editor->scene()->installEventHandler(this);
    newItem(pos);
}

void PlaceSymbolTool::desactivate()
{
    delete m_item;
    m_item = nullptr;
    m_editor->scene()->uninstallEventHandler(this);
}

void PlaceSymbolTool::newItem(const QPointF &pos)
{
    if (m_item != nullptr)
        m_item->setGraphicsEffect(nullptr);

    m_item = new SymbolItem();
    m_item->setSymbol(m_symbol);
    m_item->setPos(pos);
    m_editor->scene()->addItem(m_item);

    auto effect = new QGraphicsOpacityEffect();
    effect->setOpacity(0.7);
    m_item->setGraphicsEffect(effect);
}

bool PlaceSymbolTool::graphicsSceneKeyPressEvent(GraphicsScene *scene, QKeyEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}

bool PlaceSymbolTool::graphicsSceneKeyReleaseEvent(GraphicsScene *scene, QKeyEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}

bool PlaceSymbolTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceSymbolTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    newItem(event->scenePos());

    return true;
}

bool PlaceSymbolTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_item->setPos(event->scenePos());
    return true;
}

bool PlaceSymbolTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}
