#include "AttributeItem.h"
#include "WireItem.h"

#include <QtCore/QDebug>
#include <QtCore/QRectF>
#include <QtGui/QPainter>
#include <QtGui/QPainterPath>
#include <QtGui/QPainterPathStroker>

WireItem::WireItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , GraphicsHandleContainer(this)
    , AttributeItemContainer(this)
    , m_pen(Qt::black, 0.2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)
{
    setEnabled(true);
    setFlag(ItemSendsScenePositionChanges);
    setFlag(ItemIsSelectable);
}

WireItem::~WireItem()
{

}

void WireItem::setPoints(const QVector<QPointF> &points)
{
    prepareGeometryChange();
    clearHandles();
    m_points = points;
    for (int index=0; index<m_points.count(); index++) {
        addHandle(index, Qt::SizeAllCursor);
        moveHandle(index, m_points.value(index));
    }
}

QVector<QPointF> WireItem::points() const
{
    return m_points;
}

void WireItem::addPoint(const QPointF &pos)
{
    prepareGeometryChange();
    const int index = m_points.count();
    addHandle(index, Qt::SizeAllCursor);
    moveHandle(index, pos);
    m_points.append(pos);
}

void WireItem::movePoint(const QPointF &pos, int index)
{
    if (index<0)
        index += m_points.count();

    Q_ASSERT(index >=0 && index <m_points.count());

    if (pos == m_points.value(index))
        return;

    prepareGeometryChange();
    m_points[index] = pos;
    moveHandle(index, pos);
}

QPointF WireItem::point(int index) const
{
    if (index<0)
        index += m_points.count();

    Q_ASSERT(index >=0 && index <m_points.count());

    return m_points.value(index);
}

QRectF WireItem::boundingRect() const
{
    QPolygonF poly(m_points);
    const qreal halfPenWidth = m_pen.widthF()/2.0;
    return poly.boundingRect().adjusted(-halfPenWidth, -halfPenWidth,
                                        halfPenWidth, halfPenWidth);
}

QPainterPath WireItem::shape() const
{
    QPainterPath path;
    path.moveTo(m_points.first());
    for (const auto &point: m_points)
        path.lineTo(point);
    QPainterPathStroker stroker;
    stroker.setWidth(m_pen.widthF());
    stroker.setCapStyle(m_pen.capStyle());
    stroker.setJoinStyle(m_pen.joinStyle());
    return stroker.createStroke(path);
}

void WireItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if (m_points.isEmpty())
        return;

    QPainterPath path;
    path.moveTo(m_points.first());
    for (const auto &point: m_points)
        path.lineTo(point);
    painter->setPen(m_pen);
    painter->drawPath(path);
    if (isSelected()) {
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawPath(path);
    }
}

QVariant WireItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemSelectedHasChanged)
        showHandles(value.toBool());
    else if (change == ItemScenePositionHasChanged || change == ItemRotationHasChanged)
        updateHandles();
    return QGraphicsItem::itemChange(change, value);
}

void WireItem::handleScenePositionHasChanged(int id, const QPointF &pos)
{
    Q_ASSERT(id >= 0 && id < m_points.count());
    movePoint(pos, id);
}

void WireItem::updateHandles()
{
    for (int index=0; index<m_points.count(); index++)
        moveHandle(index, m_points.value(index));
}
