#ifndef WIREITEM_H
#define WIREITEM_H

#include "AttributeItem.h"

#include "qlGui/GraphicsHandle.h"

#include <QtGui/QPen>
#include <QtWidgets/QGraphicsItem>

class WireItem : public QGraphicsItem, public GraphicsHandleContainer, public AttributeItemContainer
{
public:
    enum {
        Type = UserType + 0x1000 + 0x01,
    };

    WireItem(QGraphicsItem *parent = nullptr);
    ~WireItem();

    void setPoints(const QVector<QPointF> &points);
    QVector<QPointF> points() const;
    void addPoint (const QPointF &pos);
    void movePoint(const QPointF &pos, int index);
    QPointF point(int index) const;

private:
    void updateHandles();
    QPen m_pen;
    QVector<QPointF> m_points;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual QPainterPath shape() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual int type() const override { return Type; }

    // QGraphicsItem interface
public:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // GraphicsHandleContainer interface
protected:
    virtual void handleScenePositionHasChanged(int id, const QPointF &pos) override;
};

#endif // WIREITEM_H
