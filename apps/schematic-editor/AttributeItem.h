#pragma once

#include <QtCore/QString>
#include <QtWidgets/QGraphicsItem>

class AttributeItem : public QGraphicsItem
{
public:
    AttributeItem(QGraphicsItem *parent = nullptr);
    ~AttributeItem();

    void setName(const QString &name);
    QString name() const;
    void setNameVisible(bool visible);
    bool isNameVisible() const;
    void setValue(const QString &value);
    QString value() const;
    void setValueVisible(bool visible);
    bool isValueVisible() const;

    QRectF textRect() const;

private:
    void updatePosition();
    QGraphicsSimpleTextItem *m_nameItem = nullptr;
    QGraphicsSimpleTextItem *m_separatorItem = nullptr;
    QGraphicsSimpleTextItem *m_valueItem = nullptr;
    QLineF lineToParent() const;
    static const qreal g_textScale;
    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    virtual QPainterPath shape() const override;
};

// TBD: QGraphicsLayoutItem
class AttributeItemContainer
{
public:
    AttributeItemContainer(QGraphicsItem *container);
    ~AttributeItemContainer();

    AttributeItem *addAttribute(const QString &name, const QString &value = QString());
    void removeAttribute(const QString &name);
    void clearAttributes();
    bool hasAttribute(const QString &name) const;
    const AttributeItem *attribute(const QString &name) const;
    AttributeItem *attribute(const QString &name);
    QList<AttributeItem *> attributes();

protected:
    void layoutAttributes();

private:
    QList<AttributeItem *> m_attributes;
    QGraphicsItem *m_container;
    QGraphicsRectItem *m_group;

};
