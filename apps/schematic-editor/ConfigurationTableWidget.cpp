#include "ConfigurationItemDelegate.h"
#include "ConfigurationTableWidget.h"
#include "ConfigurationTableModel.h"

#include "qlCore/LibraryObjects.h"
#include "qlCore/SchematicObjects.h"

#include <QtCore/QSortFilterProxyModel>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

// TODO: add a sort/filter proxy model

ConfigurationTableWidget::ConfigurationTableWidget(QWidget *parent)
    : QWidget(parent)
    , m_model(new ConfigurationTableModel(this))
    , m_sortFilterProxyModel(new QSortFilterProxyModel(m_model))
    , m_view(new QTableView())
    , m_filterLineEdit(new QLineEdit())
{
    setLayout(new QVBoxLayout());
    layout()->addWidget(m_filterLineEdit);
    layout()->addWidget(m_view);

    m_filterLineEdit->setPlaceholderText("Wildcard pattern filter...");
    m_sortFilterProxyModel->setSourceModel(m_model);
    m_sortFilterProxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_sortFilterProxyModel->setFilterKeyColumn(ConfigurationTableModel::NameColumn);
    m_view->setModel(m_sortFilterProxyModel);
    m_view->horizontalHeader()->setStretchLastSection(true);
    m_view->setAlternatingRowColors(true);
    m_view->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_view->setItemDelegateForColumn(ConfigurationTableModel::ValueColumn, new ConfigurationItemDelegate(this));
    m_view->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);

    connect(m_filterLineEdit, &QLineEdit::textChanged,
            m_sortFilterProxyModel, &QSortFilterProxyModel::setFilterWildcard);
}

void ConfigurationTableWidget::setCellInstance(sch::Component *sch, const lib::Cell *lib)
{
    QList<ConfigurationTableModel::Item> items;
    for (int i=0; i<lib->parameters.count(); i++) {
        auto paramDef = lib->parameters.value(i);
        auto schParam = sch->parameterTable.items.value(i);
        items.append(ConfigurationTableModel::Item(paramDef, schParam.value,
                                                   schParam.showName, schParam.showValue));
    }
    m_model->setItems(items);
}
