#include "AttributeItem.h"

#include <QtCore/QDebug>
#include <QtCore/QtMath>
#include <QtGui/QPainter>

#include <cmath>

// Text size is expressed in point. The graphics scene use mm as unit
// 14 points = 19 px = 5 mm = 1.2 em = 120%
// With a font size of 14, the bounding rect is 24 high
// See as well GraphicsLabelItem
const qreal AttributeItem::g_textScale = 5.0/24.0;

AttributeItem::AttributeItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_nameItem(new QGraphicsSimpleTextItem(this))
    , m_separatorItem(new QGraphicsSimpleTextItem(this))
    , m_valueItem(new QGraphicsSimpleTextItem(this))
{
    m_nameItem->setScale(g_textScale);
    m_nameItem->setFlag(ItemIsSelectable, false);
    m_separatorItem->setScale(g_textScale);
    m_separatorItem->setFlag(ItemIsSelectable, false);
    m_separatorItem->setText("=");
    m_valueItem->setScale(g_textScale);
    m_valueItem->setFlag(ItemIsSelectable, false);
    updatePosition();
}

AttributeItem::~AttributeItem()
{

}

void AttributeItem::setName(const QString &name)
{
    if (m_nameItem->text() == name)
        return;

    prepareGeometryChange();
    m_nameItem->setText(name);
    updatePosition();
}

QString AttributeItem::name() const
{
    return m_nameItem->text();
}

void AttributeItem::setNameVisible(bool visible)
{
    prepareGeometryChange();
    m_nameItem->setVisible(visible);
    m_separatorItem->setVisible(visible);
    updatePosition();
}

bool AttributeItem::isNameVisible() const
{
    return m_nameItem->isVisible();
}

void AttributeItem::setValue(const QString &value)
{
    if (m_valueItem->text() == value)
        return;

    prepareGeometryChange();
    m_valueItem->setText(value);
    updatePosition();
}

QString AttributeItem::value() const
{
    return m_valueItem->text();
}

void AttributeItem::setValueVisible(bool visible)
{
    prepareGeometryChange();
    m_valueItem->setVisible(visible);
    updatePosition();
}

bool AttributeItem::isValueVisible() const
{
    return m_valueItem->isVisible();
}

void AttributeItem::updatePosition()
{
    m_nameItem->setPos(0, 0);
    if (m_nameItem->isVisible()) {
        m_separatorItem->setPos(mapRectFromItem(m_nameItem, m_nameItem->boundingRect()).topRight());
        m_valueItem->setPos(mapRectFromItem(m_separatorItem, m_separatorItem->boundingRect()).topRight());
    }
    else
        m_valueItem->setPos(m_nameItem->pos());
}

QRectF AttributeItem::textRect() const
{
    QRectF rect;
    if (m_nameItem->isVisible())
        rect |= mapRectFromItem(m_nameItem, m_nameItem->boundingRect());
    if (m_separatorItem->isVisible())
        rect |= mapRectFromItem(m_separatorItem, m_separatorItem->boundingRect());
    if (m_valueItem->isVisible())
        rect |= mapRectFromItem(m_valueItem, m_valueItem->boundingRect());
    return rect;
}

QLineF AttributeItem::lineToParent() const
{
    const auto target = mapFromParent(parentItem()->boundingRect().center());
    const auto rect = textRect();
    QMap<qreal, QLineF> lines;
    const QLineF l1(rect.left(), rect.center().y(), target.x(), target.y());
    lines.insert(l1.length(), l1);
    const QLineF l2(rect.center().x(), rect.top(), target.x(), target.y());
    lines.insert(l2.length(), l2);
    const QLineF l3(rect.right(), rect.center().y(), target.x(), target.y());
    lines.insert(l3.length(), l3);
    const QLineF l4(rect.center().x(), rect.bottom(), target.x(), target.y());
    lines.insert(l4.length(), l4);
    const QLineF l5(rect.topLeft(), target);
    lines.insert(l5.length(), l5);
    const QLineF l6(rect.topRight(), target);
    lines.insert(l6.length(), l6);
    const QLineF l7(rect.bottomRight(), target);
    lines.insert(l7.length(), l7);
    const QLineF l8(rect.bottomLeft(), target);
    lines.insert(l8.length(), l8);

    return lines.first();
}

QRectF AttributeItem::boundingRect() const
{
    const auto line = lineToParent();
    return textRect() | QRectF(line.p1(), line.p2()).normalized();
}

QPainterPath AttributeItem::shape() const
{
    QPainterPath path;
    path.addRect(textRect());
    return path;
}

void AttributeItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if (isSelected()) {
        const auto rect = textRect();
        const auto line = lineToParent();
        painter->setPen(QPen(Qt::black, 0.0));
        painter->drawLine(line);
        painter->drawRect(rect);
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawLine(line);
        painter->drawRect(rect);
    }
}

AttributeItemContainer::AttributeItemContainer(QGraphicsItem *container)
    : m_container(container)
    , m_group(new QGraphicsRectItem(container))
{

}

AttributeItemContainer::~AttributeItemContainer()
{

}

AttributeItem *AttributeItemContainer::addAttribute(const QString &name, const QString &value)
{
    AttributeItem *item = nullptr;
    for (auto attribute: m_attributes) {
        if (attribute->name() == name) {
            attribute->setValue(value);
            item = attribute;
        }
    }
    if (item == nullptr) {
        item = new AttributeItem(m_group/*m_container*/);
        item->setName(name);
        item->setValue(value);
        item->setFlag(QGraphicsItem::ItemIsSelectable);
        item->setFlag(QGraphicsItem::ItemIsMovable);
        m_attributes.append(item);
        //m_group->addToGroup(item);
    }
    layoutAttributes();
    return item;
}

void AttributeItemContainer::removeAttribute(const QString &name)
{
    for (auto attribute: m_attributes) {
        if (attribute->name() == name) {
            m_attributes.removeOne(attribute);
            delete attribute;
            layoutAttributes();
            break;
        }
    }
}

bool AttributeItemContainer::hasAttribute(const QString &name) const
{
    for (auto attribute: m_attributes)
        if (attribute->name() == name)
            return true;
    return false;
}

const AttributeItem *AttributeItemContainer::attribute(const QString &name) const
{
    for (auto attribute: m_attributes)
        if (attribute->name() == name)
            return attribute;
    return nullptr;
}

AttributeItem *AttributeItemContainer::attribute(const QString &name)
{
    for (auto attribute: m_attributes)
        if (attribute->name() == name)
            return attribute;
    return nullptr;
}

QList<AttributeItem *> AttributeItemContainer::attributes()
{
    return m_attributes;
}

void AttributeItemContainer::layoutAttributes()
{
    if (m_attributes.isEmpty())
        return;
#if 1
    const auto rect = m_container->boundingRect();
    QPointF topLeft = rect.bottomLeft();
    qreal alpha = m_container->transform().m22() * m_container->rotation();
    const qreal cos = std::cos(qDegreesToRadians(alpha));
    const qreal sin = std::sin(qDegreesToRadians(alpha));
    alpha = qRadiansToDegrees(std::atan2(sin, cos)) + 360.0;
    int quadrant = static_cast<int>(std::roundf(alpha/90.0)) % 4;
    switch (quadrant) {
        case 1:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.topLeft();
            else
                topLeft = rect.bottomLeft();
            break;
        case 2:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.topRight();
            else
                topLeft = rect.bottomRight();
            break;
        case 3:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.bottomRight();
            else
                topLeft = rect.topLeft();
            break;
        case 0:
        default:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.bottomLeft();
            else
                topLeft = rect.topLeft();
            break;
    }
    qreal yScale = m_container->transform().m22();
    m_group->setRotation(-alpha);
    m_group->setTransform(QTransform::fromScale(1, yScale));
    m_group->setPos(topLeft);
    QPointF pos;
    for (auto attribute: m_attributes) {
        attribute->setPos(pos);
        pos = m_group->mapFromItem(attribute, attribute->textRect().bottomLeft());
    }
#else
    const auto rect = m_container->boundingRect();
    QPointF topLeft = rect.bottomLeft();
    qreal alpha = m_container->transform().m22() * m_container->rotation();
    const qreal cos = std::cos(qDegreesToRadians(alpha));
    const qreal sin = std::sin(qDegreesToRadians(alpha));
    alpha = qRadiansToDegrees(std::atan2(sin, cos)) + 360.0;
    int quadrant = static_cast<int>(std::roundf(alpha/90.0)) % 4;
    switch (quadrant) {
        case 1:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.topLeft();
            else
                topLeft = rect.bottomLeft();
            break;
        case 2:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.topRight();
            else
                topLeft = rect.bottomRight();
            break;
        case 3:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.bottomRight();
            else
                topLeft = rect.topLeft();
            break;
        case 0:
        default:
            if (m_container->transform().m22() >= 0)
                topLeft = rect.bottomLeft();
            else
                topLeft = rect.topLeft();
            break;
    }
    qreal yScale = m_container->transform().m22();
    qreal top = topLeft.y();
    const qreal left = topLeft.x();
    auto pos = QPointF(left, top); //attribute->mapToParent(left, top);
    for (auto attribute: m_attributes) {
        if (attribute->value() == "X2")
            qt_noop();
        attribute->setRotation(-alpha);
        attribute->setTransform(QTransform::fromScale(1, yScale));
        attribute->setPos(pos);
        pos = m_container->mapFromItem(attribute, attribute->textRect().bottomLeft());
    }
#endif
}
