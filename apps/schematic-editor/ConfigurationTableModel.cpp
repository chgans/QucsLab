#include "ConfigurationTableModel.h"

#include "qlCore/LibraryObjects.h"

ConfigurationTableModel::ConfigurationTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

ConfigurationTableModel::~ConfigurationTableModel()
{

}

void ConfigurationTableModel::setItems(const QList<Item> &items)
{
    beginResetModel();
    m_items = items;
    endResetModel();
}

QList<ConfigurationTableModel::Item> ConfigurationTableModel::items() const
{
    return m_items;
}

ConfigurationTableModel::Item ConfigurationTableModel::item(int index)
{
    Q_ASSERT(index >= 0 && index < m_items.count());
    return m_items.value(index);
}

int ConfigurationTableModel::itemCount() const
{
    return m_items.count();
}

int ConfigurationTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_items.count();
}

int ConfigurationTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return ColumnCount;
}

QVariant ConfigurationTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();
    if (orientation != Qt::Horizontal)
        return QVariant();

    switch (section) {
    case NameColumn:
        return "Name";
    case ValueColumn:
        return "Value";
    case UnitColumn:
        return "Unit";
    default:
        break;
    }

    return QVariant();
}

QVariant ConfigurationTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    Item item = m_items.value(index.row());
    int column = index.column();

    switch (role) {
    case Qt::DisplayRole:
        switch (column) {
        case NameColumn:
            return item.definition->name();
        case ValueColumn:
            return item.value;
        case UnitColumn:
            return item.definition->unit();
        default:
            break;
        }
        break;
    case Qt::EditRole:
        switch (column) {
        case ValueColumn:
            return item.value;
        default:
            break;
        }
        break;
    case Qt::CheckStateRole:
        switch (column) {
        case NameColumn:
            return item.showName ? Qt::Checked : Qt::Unchecked;
        case ValueColumn:
            return item.showValue ? Qt::Checked : Qt::Unchecked;
        default:
            break;
        }
        break;
    case Qt::ToolTipRole:
        return item.definition->documentation();
    case DataTypeRole:
        return item.definition->dataType();
    case DataRestrictionRole:
        return item.definition->restriction();
    case DefaultValueRole:
        return item.definition->defaultValue();
    default:
        break;
    }
    return QVariant();
}

bool ConfigurationTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) == value)
        return false;

    if (index.column() != ValueColumn)
        return false;

    if (role != Qt::EditRole)
        return false;

    m_items[index.row()].value = value.toString();
    emit dataChanged(index, index, QVector<int>() << role);
    emit itemChanged(index.row());
    return true;
}

Qt::ItemFlags ConfigurationTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    int column = index.column();
    switch (column) {
    case NameColumn:
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    case ValueColumn:
        return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsUserCheckable;
    case UnitColumn:
        return Qt::ItemIsEnabled;
    default:
        return Qt::NoItemFlags;
    }
}
