#ifndef NETGRAPH_H
#define NETGRAPH_H

#include <QtCore/QObject>

class NetGraph : public QObject
{
    Q_OBJECT
public:
    explicit NetGraph(QObject *parent = 0);

    void addNet(const QString &name);
    //void addComponent(Component *component);

signals:

public slots:
};

#endif // NETGRAPH_H
