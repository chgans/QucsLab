#include "NavigationDockWidget.h"

#include <QtWidgets/QComboBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>

NavigationDockWidget::NavigationDockWidget(QWidget *parent)
    : QDockWidget(parent)
    , m_comboBox(new QComboBox())
    , m_stackedWidget(new QStackedWidget())
{
    auto container = new QWidget();
    auto layout = new QVBoxLayout();
    layout->addWidget(m_comboBox);
    layout->addWidget(m_stackedWidget);
    layout->setMargin(0);
    layout->setSpacing(1);
    container->setLayout(layout);
    setWidget(container);

    connect(m_comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
            m_stackedWidget, &QStackedWidget::setCurrentIndex);
}

NavigationDockWidget::~NavigationDockWidget()
{

}

void NavigationDockWidget::addNavigationWidget(const QString &text, QWidget *widget)
{
    m_comboBox->addItem(text);
    m_stackedWidget->addWidget(widget);
}

void NavigationDockWidget::setCurrentText(const QString &text)
{
    m_comboBox->setCurrentText(text);
}

void NavigationDockWidget::setCurrentIndex(int index)
{
    m_comboBox->setCurrentIndex(index);
}
