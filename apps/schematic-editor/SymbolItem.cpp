#include "AttributeItem.h"
#include "SymbolItem.h"
#include "../symbol-editor/GraphicsItemFactory.h" // FIXME

#include "qlCore/SymbolObjects.h"
#include "qlGui/GraphicsItem.h"

#include <QtGui/QPainter>
#include <QtWidgets/QGraphicsSimpleTextItem>
#include <QtWidgets/QGraphicsTextItem>

SymbolItem::SymbolItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , AttributeItemContainer(this)
{
    setFlag(ItemSendsGeometryChanges);
    setFlag(ItemIsSelectable);
    setFlag(ItemIsMovable);
    setEnabled(true);
}

SymbolItem::~SymbolItem()
{

}

void SymbolItem::setSymbol(sym::Symbol *symbol)
{
    prepareGeometryChange();
    m_symbol.reset(symbol);

    m_boundingRect = QRectF();
    GraphicsItemFactory factory;
    for (auto item: factory.createItems(m_symbol->childObjects())) {
        if (item->type() == GraphicsPortItem::Type)
            continue;
        item->setFlag(ItemStacksBehindParent);
        item->setFlag(ItemIsSelectable, false);
        item->setEnabled(false);
        item->setParentItem(this);
        m_boundingRect |= mapFromItem(item, item->boundingRect()).boundingRect();
    }
}

QRectF SymbolItem::boundingRect() const
{
    return m_boundingRect;
}

void SymbolItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 0.0));
        painter->drawRect(boundingRect());
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawRect(boundingRect());
    }
}


QVariant SymbolItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemRotationHasChanged || change == ItemTransformHasChanged)
        layoutAttributes();
    return value;
}

NetTieItem::NetTieItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , AttributeItemContainer(this)
{
    setFlag(ItemSendsGeometryChanges);
    setFlag(ItemIsSelectable);
    setFlag(ItemIsMovable);
    setEnabled(true);
    m_pen = QPen(Qt::black, 0.2);
}

NetTieItem::~NetTieItem()
{

}

void NetTieItem::setTieStyle(Qs::TieStyle style)
{
    prepareGeometryChange();
    m_style = style;
    QPainterPath path;
    switch (m_style) {
    case Qs::GroundTie:
        path.moveTo(0, 0);
        path.lineTo(0, 2);
        path.moveTo(-2, 2);
        path.lineTo(2, 2);
        path.lineTo(0, 4);
        path.lineTo(-2, 2);
        break;
    case Qs::EarthTie:
        break;
    case Qs::BarTie:
        break;
    case Qs::ArrowTie:
        break;
    case Qs::CircleTie:
        break;
    case Qs::WaveTie:
        break;
    }
    m_path = path;
}

QRectF NetTieItem::boundingRect() const
{
    const qreal halfPenWidth = m_pen.widthF()/2.0;
    return m_path.boundingRect().adjusted(-halfPenWidth, -halfPenWidth,
                                          halfPenWidth, halfPenWidth);
}

void NetTieItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(m_pen);
    painter->drawPath(m_path);
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 0.0));
        painter->drawRect(boundingRect());
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawRect(boundingRect());
    }
}

QVariant NetTieItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemRotationHasChanged || change == ItemTransformHasChanged)
        layoutAttributes();
    return value;
}

SheetPortItem::SheetPortItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , AttributeItemContainer(this)
{
    setFlag(ItemSendsGeometryChanges);
    setFlag(ItemIsSelectable);
    setFlag(ItemIsMovable);
    setEnabled(true);
    m_pen = QPen(Qt::black, 0.2);
}

SheetPortItem::~SheetPortItem()
{

}

void SheetPortItem::setPortStyle(Qs::PortStyle style)
{
    prepareGeometryChange();
    m_style = style;
    QPainterPath path;
    switch (m_style) {
    case Qs::NoPortStyle:
        break;
    case Qs::RectanglePort:
        path.moveTo(0, 0);
        path.lineTo(2, 0);
        path.addRect(3, -1, 4, 3);
        break;
    case Qs::CirclePort:
        path.moveTo(0, 0);
        path.lineTo(-2, 0);
        path.addEllipse(-4, -1, 2, 2);
        break;
    }
    m_path = path;
}

QRectF SheetPortItem::boundingRect() const
{
    const qreal halfPenWidth = m_pen.widthF()/2.0;
    return m_path.boundingRect().adjusted(-halfPenWidth, -halfPenWidth,
                                          halfPenWidth, halfPenWidth);
}

void SheetPortItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(m_pen);
    painter->drawPath(m_path);
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 0.0));
        painter->drawRect(boundingRect());
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawRect(boundingRect());
    }
}

QVariant SheetPortItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemRotationHasChanged || change == ItemTransformHasChanged)
        layoutAttributes();
    return value;
}

DirectiveItem::DirectiveItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , AttributeItemContainer(this)
    , m_textItem(new QGraphicsTextItem(this))
{
    setFlag(ItemSendsGeometryChanges);
    setFlag(ItemIsSelectable);
    setFlag(ItemIsMovable);
    setEnabled(true);
    m_pen = QPen(Qt::black, 0.2);
    m_textItem->setScale(5.0/24.0); // FIXME: See AttributeItem::g_textScale
}

DirectiveItem::~DirectiveItem()
{

}

void DirectiveItem::setText(const QString &text)
{
    prepareGeometryChange();
    m_textItem->setPlainText(text);
    QPainterPath path;
    const auto rect = m_textItem->mapRectToParent(m_textItem->boundingRect());
    const auto offset = QPointF(1, 1);
    const auto p10 = rect.bottomLeft();
    const auto p11 = p10 + offset;
    const auto p20 = rect.bottomRight();
    const auto p21 = p20 + offset;
    const auto p30 = rect.topRight();
    const auto p31 = p30 + offset;
    path.addRect(rect);
    path.moveTo(p10);
    path.lineTo(p11);
    path.moveTo(p20);
    path.lineTo(p21);
    path.moveTo(p30);
    path.lineTo(p31);
    path.moveTo(p11);
    path.lineTo(p21);
    path.lineTo(p31);
    m_path = path;
}

QRectF DirectiveItem::boundingRect() const
{
    const qreal halfPenWidth = m_pen.widthF()/2.0;
    return m_path.boundingRect().adjusted(-halfPenWidth, -halfPenWidth,
                                          halfPenWidth, halfPenWidth);
}

void DirectiveItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(m_pen);
    painter->drawPath(m_path);
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 0.0));
        painter->drawRect(boundingRect());
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawRect(boundingRect());
    }
}

QVariant DirectiveItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemRotationHasChanged || change == ItemTransformHasChanged)
        layoutAttributes();
    return value;
}

DiagramItem::DiagramItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , AttributeItemContainer(this)
    , m_textItem(new QGraphicsTextItem(this))
{
    setFlag(ItemSendsGeometryChanges);
    setFlag(ItemIsSelectable);
    setFlag(ItemIsMovable);
    setEnabled(true);
    m_pen = QPen(Qt::black, 0.2);
    m_textItem->setScale(5.0/24.0); // FIXME: See AttributeItem::g_textScale
}

DiagramItem::~DiagramItem()
{

}

void DiagramItem::setText(const QString &text)
{
    prepareGeometryChange();
    m_textItem->setPlainText(text);
}

void DiagramItem::setSize(const QSizeF &size)
{
    prepareGeometryChange();
    m_size = size;
}

QRectF DiagramItem::boundingRect() const
{
    const qreal halfPenWidth = m_pen.widthF()/2.0;
    return QRectF(QPoint(), m_size).adjusted(-halfPenWidth, -halfPenWidth,
                                          halfPenWidth, halfPenWidth);
}

void DiagramItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(m_pen);
    painter->drawRect(QRectF(QPoint(), m_size));
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 0.0));
        painter->drawRect(boundingRect());
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->drawRect(boundingRect());
    }
}

QVariant DiagramItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemRotationHasChanged || change == ItemTransformHasChanged)
        layoutAttributes();
    return value;
}


int SymbolItem::type() const
{
    return Type;
}
