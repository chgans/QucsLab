/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "LibraryNavigationWidget.h"

#include <QtWidgets/QFileSystemModel>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>

// TODO: Use a custom QAbstractProxyModel to filter out file names by extension
// but still allow to navigate directories

LibraryNavigationWidget::LibraryNavigationWidget(QWidget *parent)
    : QWidget(parent)
    , m_view(new QTreeView())
    , m_model(new QFileSystemModel(this))
{
    m_model->setReadOnly(true);
    //m_model->setNameFilters(QStringList() << "*.xml");
    QDir::Filters filters = QDir::Dirs | QDir::NoDot
                            | QDir::Files | QDir::Drives
                            | QDir::Readable | QDir::Writable
                            | QDir::Executable | QDir::Hidden;
    m_model->setFilter(filters);
    connect(m_view, &QTreeView::activated,
            this, &LibraryNavigationWidget::openItem);

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_view);

    m_view->setModel(m_model);

    for (int i=0; i<m_model->columnCount(); i++)
        m_view->hideColumn(i);
    m_view->showColumn(0);
}

LibraryNavigationWidget::~LibraryNavigationWidget()
{

}

void LibraryNavigationWidget::setInitialPath(const QString &path)
{
    m_initialPath = path;
    setCurrentDirectory(m_initialPath);
}

QString LibraryNavigationWidget::initialPath() const
{
    return m_initialPath;
}

void LibraryNavigationWidget::openItem(const QModelIndex &index)
{
    const QString fileName = m_model->fileName(index);

    if (fileName == QLatin1String("."))
        return;

    if (fileName == QLatin1String("..")) {
        // cd up: Special behaviour: The fileInfo of ".." is that of the parent directory.
        const QString parentPath = m_model->fileInfo(index).absoluteFilePath();
        setCurrentDirectory(parentPath);
        return;
    }

    const QString path = m_model->filePath(index);
    if (m_model->isDir(index)) {
        const QFileInfo fi = m_model->fileInfo(index);
        if (!fi.isReadable() || !fi.isExecutable())
            return;
        // Change to directory
        setCurrentDirectory(path);
        return;
    }

    // TODO: path is a file that need to be opened
    emit fileActivated(path);
}

void LibraryNavigationWidget::setCurrentDirectory(const QString &directory)
{
    const QString newDirectory = directory.isEmpty() ? QDir::rootPath() : directory;

    QModelIndex oldRootIndex = m_view->rootIndex();
    QModelIndex newRootIndex = m_model->setRootPath(newDirectory);
    Q_ASSERT(newRootIndex.isValid());
    m_view->setRootIndex(newRootIndex);
    const QDir current(QDir::cleanPath(newDirectory));
    if (oldRootIndex.parent() == newRootIndex)   // cdUp, so select the old directory
    {
        m_view->setCurrentIndex(oldRootIndex);
        m_view->scrollTo(oldRootIndex, QAbstractItemView::EnsureVisible);
    }
}
