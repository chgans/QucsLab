#ifndef EDITOR_H
#define EDITOR_H

#include "qlCore/QlCore.h"
#include "qlCore/SchematicReader.h"

#include <QtCore/QList>
#include <QtWidgets/QWidget>

class GraphicsGrid;
class GraphicsScene;
class GraphicsSheet;
class GraphicsSnapper;
class GraphicsView;
class GraphicsZoomHandler;

class SchGraphicsTool;
class SelectTool;
class PlaceJunctionTool;
class PlaceSymbolTool;
class PlaceWireTool;

namespace sym {
    class Symbol;
}

class ExampleNavigationWidget;
class LibraryManager;
class LibraryNavigationWidget;
class MainWindow;
class NavigationDockWidget;

class QActionGroup;
class QDockWidget;
class QToolBar;

class SchEditor : public QWidget
{
    Q_OBJECT
public:
    explicit SchEditor(QWidget *parent = 0);

    GraphicsScene *scene() const;
    GraphicsView *view();
    GraphicsSheet *sheet();
    GraphicsGrid *grid();
    GraphicsSnapper *snapper();
    LibraryManager *libraryManager();

    bool open(const QString &path);
    void activate(MainWindow *mainwindow);

signals:

public slots:
    void placeSymbol(const QString &path);

private:
    void loadTools();
    void createActions();
    void createToolBars();
    void createDockWidgets();
    void setupGraphicsView(Qs::SheetFormat format = Qs::A4Landscape);
    void setupNavigationWidgets();
    void setupUndoStack();
    void setupClipBoard();
    void setupLibraryManager();

    sym::Symbol *loadSymbol(const QString &path);

    GraphicsScene *m_scene = nullptr;
    GraphicsView *m_view = nullptr;
    GraphicsSheet *m_sheet = nullptr;
    GraphicsGrid *m_grid = nullptr;
    GraphicsSnapper *m_snapper = nullptr;
    GraphicsZoomHandler *m_zoomHandler = nullptr;
    SelectTool *m_selectTool = nullptr;
    PlaceJunctionTool *m_placeJunctionTool = nullptr;
    PlaceWireTool *m_placeWireTool = nullptr;
    PlaceSymbolTool *m_placeSymbolTool = nullptr;
    QList<SchGraphicsTool*> m_tools;
    QActionGroup *m_toolActionGroup = nullptr;
    QToolBar *m_toolsToolBar = nullptr;
    LibraryNavigationWidget *m_libraryWidget = nullptr;
    ExampleNavigationWidget *m_exampleWidget = nullptr;
    NavigationDockWidget *m_navigationDockWidget = nullptr;
    LibraryManager *m_libraryManager = nullptr;
    void triggerActionForTool(SchGraphicsTool *tool);
    void activateTool(SchGraphicsTool *tool);
    SchGraphicsTool *m_activeTool = nullptr;
};

#endif // EDITOR_H
