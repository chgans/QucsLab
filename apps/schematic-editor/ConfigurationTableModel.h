#ifndef COMPONENTCONFIGURATIONMODEL_H
#define COMPONENTCONFIGURATIONMODEL_H

#include <QtCore/QAbstractTableModel>

namespace lib
{
    class Parameter;
}

class ConfigurationTableModel : public QAbstractTableModel
{
    Q_OBJECT


public:
    class Item {
    public:
        Item()  {}
        explicit Item(const lib::Parameter *definition, const QString &value,
                      bool showName, bool showValue)
              : definition(definition), value(value)
              , showName(showName), showValue(showValue)
        {}
        Item (const Item &other)
            : index(other.index), definition(other.definition)
            , value(other.value), showName(other.showName)
            , showValue(other.showValue)
        {}
        int index = -1;
        const lib::Parameter *definition;
        QString value;
        bool showName = false;
        bool showValue = false;
    };

    enum {
        NameColumn = 0,
        UnitColumn,
        ValueColumn,
        ColumnCount
    };

    enum {
        DataTypeRole = Qt::UserRole + 0,
        DataRestrictionRole,
        DefaultValueRole
    };

    explicit ConfigurationTableModel(QObject *parent = 0);
    ~ConfigurationTableModel();

    void setItems(const QList<Item> &items);
    QList<Item> items() const;
    Item item(int index);
    int itemCount() const;

signals:
    void itemChanged(int index);


private:
    QList<Item> m_items;

    // QAbstractTableModel interface
public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

#endif // COMPONENTCONFIGURATIONMODEL_H
