#ifndef NAVIGATIONDOCKWIDGET_H
#define NAVIGATIONDOCKWIDGET_H

#include <QtWidgets/QDockWidget>

class QStackedWidget;
class QComboBox;

class NavigationDockWidget : public QDockWidget
{
public:
    NavigationDockWidget(QWidget *parent = nullptr);
    ~NavigationDockWidget();

    void addNavigationWidget(const QString &text, QWidget *widget);
    void setCurrentText(const QString &text);
    void setCurrentIndex(int index);

private:
    QComboBox *m_comboBox;
    QStackedWidget *m_stackedWidget;
};

#endif // NAVIGATIONDOCKWIDGET_H
