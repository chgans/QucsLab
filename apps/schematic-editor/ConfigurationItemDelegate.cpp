#include "ConfigurationItemDelegate.h"
#include "ConfigurationTableModel.h"

#include "qlCore/LibraryObjects.h" // FIXME: Move enums to qlcore.h?
#include "qlCore/QlCore.h"

#include <QtWidgets/QComboBox>
#include <QtWidgets/QLineEdit>

ConfigurationItemDelegate::ConfigurationItemDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{

}

QWidget *ConfigurationItemDelegate::createBoolEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const
{
    Q_UNUSED(restriction);
    auto editor = new QComboBox(parent);
    editor->addItem("True");
    editor->addItem("False");
    editor->setCurrentText(defaultValue);
    return editor;
}

QWidget *ConfigurationItemDelegate::createIntEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const
{
    Q_UNUSED(restriction);
    auto editor = new QLineEdit(parent);
    editor->setPlaceholderText(defaultValue);
    editor = editor;
    return editor;
}

QWidget *ConfigurationItemDelegate::createFloatEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const
{
    Q_UNUSED(restriction);
    auto editor = new QLineEdit(parent);
    editor->clear();
    editor->setPlaceholderText(defaultValue);
    editor->setText(defaultValue);
    return editor;
}

QWidget *ConfigurationItemDelegate::createChoiceEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const
{
    Q_UNUSED(restriction);

    static const QRegularExpression re("^\\((\\w+(\\|\\w+)*)\\)$", QRegularExpression::OptimizeOnFirstUsageOption);
    Q_ASSERT(re.isValid());
    const auto reMatch = re.match(restriction);

    auto editor = new QComboBox(parent);
    if (reMatch.hasMatch()) {
        editor->addItems(reMatch.captured(1).split("|"));
        editor->setCurrentText(defaultValue);
    }
    else {
        qWarning() << "ConfigurationItemDelegate::createChoiceEditor" << "Invalid data restriction:" << restriction;
    }
    return editor;
}

QWidget *ConfigurationItemDelegate::createStringEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const
{
    Q_UNUSED(restriction);
    auto editor = new QLineEdit(parent);
    editor->setPlaceholderText(defaultValue);
    editor->setText(defaultValue);
    return editor;
}

void ConfigurationItemDelegate::setBoolEditorData(QWidget *editor, const QString &value) const
{
    auto boolEditor = static_cast<QComboBox *>(editor);
    boolEditor->setCurrentText(value);
}

void ConfigurationItemDelegate::setIntEditorData(QWidget *editor, const QString &value) const
{
    auto intEditor = static_cast<QLineEdit *>(editor);
    intEditor->setText(value);
}

void ConfigurationItemDelegate::setFloatEditorData(QWidget *editor, const QString &value) const
{
    auto floatEditor = static_cast<QLineEdit *>(editor);
    floatEditor->setText(value);
}

void ConfigurationItemDelegate::setChoiceEditorData(QWidget *editor, const QString &value) const
{
    auto choiceEditor = static_cast<QComboBox *>(editor);
    choiceEditor->setCurrentText(value);
}

void ConfigurationItemDelegate::setStringEditorData(QWidget *editor, const QString &value) const
{
    auto stringEditor = static_cast<QLineEdit *>(editor);
    stringEditor->setText(value);
}

QString ConfigurationItemDelegate::boolEditorData(QWidget *editor) const
{
    auto boolEditor = static_cast<QComboBox *>(editor);
    return boolEditor->currentText();
}

QString ConfigurationItemDelegate::intEditorData(QWidget *editor) const
{
    auto intEditor = static_cast<QLineEdit *>(editor);
    return intEditor->text();
}

QString ConfigurationItemDelegate::floatEditorData(QWidget *editor) const
{
    auto floatEditor = static_cast<QLineEdit *>(editor);
    return floatEditor->text();
}

QString ConfigurationItemDelegate::choiceEditorData(QWidget *editor) const
{
    auto choiceEditor = static_cast<QComboBox *>(editor);
    return choiceEditor->currentText();
}

QString ConfigurationItemDelegate::stringEditorData(QWidget *editor) const
{
    auto stringEditor = static_cast<QLineEdit *>(editor);
    return stringEditor->text();
}

QWidget *ConfigurationItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);

    const auto dataType = index.model()->data(index, ConfigurationTableModel::DataTypeRole).value<lib::Parameter::DataType>();
    const auto defaultValue = index.model()->data(index, ConfigurationTableModel::DefaultValueRole).toString();
    const auto restriction = index.model()->data(index, ConfigurationTableModel::DataRestrictionRole).toString();

    switch (dataType) {
    case lib::Parameter::BooleanData:
        return createBoolEditor(parent, defaultValue, restriction);
    case lib::Parameter::IntegerData:
        return createIntEditor(parent, defaultValue, restriction);
    case lib::Parameter::FloatData:
        return createFloatEditor(parent, defaultValue, restriction);
    case lib::Parameter::EnumData:
        return createChoiceEditor(parent, defaultValue, restriction);
    case lib::Parameter::SingleLineTextData:
        return createStringEditor(parent, defaultValue, restriction);
    case lib::Parameter::MultiLineTextData:
    default:
        Q_UNIMPLEMENTED();
        Q_UNREACHABLE();
        break;
    }
    return new QWidget(parent);
}

void ConfigurationItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    const auto value = index.model()->data(index, Qt::EditRole).toString();
    const auto dataType = index.model()->data(index, ConfigurationTableModel::DataTypeRole).value<lib::Parameter::DataType>();

    switch (dataType) {
    case lib::Parameter::BooleanData:
        setBoolEditorData(editor, value);
        break;
    case lib::Parameter::IntegerData:
        setIntEditorData(editor, value);
        break;
    case lib::Parameter::FloatData:
        setFloatEditorData(editor, value);
        break;
    case lib::Parameter::EnumData:
        setChoiceEditorData(editor, value);
        break;
    case lib::Parameter::SingleLineTextData:
        setStringEditorData(editor, value);
        break;
    case lib::Parameter::MultiLineTextData:
    default:
        Q_UNIMPLEMENTED();
        Q_UNREACHABLE();
        break;
    }
}

void ConfigurationItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    Q_UNUSED(editor);

    const auto dataType = model->data(index, ConfigurationTableModel::DataTypeRole).value<lib::Parameter::DataType>();

    QString value;
    switch (dataType) {
    case lib::Parameter::BooleanData:
        value = boolEditorData(editor);
        break;
    case lib::Parameter::IntegerData:
        value = intEditorData(editor);
        break;
    case lib::Parameter::FloatData:
        value = floatEditorData(editor);
        break;
    case lib::Parameter::EnumData:
        value = choiceEditorData(editor);
        break;
    case lib::Parameter::SingleLineTextData:
        value = stringEditorData(editor);
        break;
    case lib::Parameter::MultiLineTextData:
        Q_UNIMPLEMENTED();
        Q_UNREACHABLE();
        break;
    default:
        break;
    }
    model->setData(index, value);
}

void ConfigurationItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
}
