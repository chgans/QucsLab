#include "ExampleNavigationWidget.h"
#include "JunctionItem.h"
#include "LibraryNavigationWidget.h"
#include "MainWindow.h"
#include "NavigationDockWidget.h"
#include "SchEditor.h"
#include "SchGraphicsTool.h"
#include "SymbolItem.h"
#include "WireItem.h"

#include "../symbol-editor/GraphicsItemFactory.h" // FIXME

#include "qlCore/LibraryObjects.h"
#include "qlCore/LibraryManager.h"
#include "qlCore/SchematicObjects.h"
#include "qlCore/SchematicReader.h"
#include "qlCore/SymbolObjects.h"
#include "qlCore/SymbolReader.h"
#include "qlCore/QucsReader.h"
#include "qlCore/QucsConverter.h"

#include "qlGui/GraphicsGrid.h"
#include "qlGui/GraphicsItem.h"
#include "qlGui/GraphicsScene.h"
#include "qlGui/GraphicsSheet.h"
#include "qlGui/GraphicsSnapper.h"
#include "qlGui/GraphicsView.h"
#include "qlGui/GraphicsZoomHandler.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QRegularExpression>

#include <QtWidgets/QDockWidget>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>

#include <cmath>

SchEditor::SchEditor(QWidget *parent)
    : QWidget(parent)
    , m_scene(new GraphicsScene(this))
    , m_view(new GraphicsView())
    , m_sheet(new GraphicsSheet())
    , m_grid(new GraphicsGrid())
    , m_snapper(new GraphicsSnapper(this))
    , m_zoomHandler(new GraphicsZoomHandler(this))
    , m_selectTool(new SelectTool(this))
    , m_placeJunctionTool(new PlaceJunctionTool(this))
    , m_placeWireTool(new PlaceWireTool(this))
    , m_placeSymbolTool(new PlaceSymbolTool(this))
    , m_toolActionGroup(new QActionGroup(this))
    , m_libraryWidget(new LibraryNavigationWidget())
    , m_exampleWidget(new ExampleNavigationWidget())
    , m_libraryManager(new LibraryManager(this))
{
    loadTools();
    createActions();
    createToolBars();
    createDockWidgets();
    setupGraphicsView();
    setupNavigationWidgets();
    setupUndoStack();
    setupClipBoard();
    setupLibraryManager();

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_view);

    activateTool(m_tools.first());
}

GraphicsScene *SchEditor::scene() const
{
    return m_scene;
}

GraphicsView *SchEditor::view()
{
    return m_view;
}

GraphicsSheet *SchEditor::sheet()
{
    return m_sheet;
}

GraphicsGrid *SchEditor::grid()
{
    return m_grid;
}

GraphicsSnapper *SchEditor::snapper()
{
    return m_snapper;
}

LibraryManager *SchEditor::libraryManager()
{
    return m_libraryManager;
}

bool SchEditor::open(const QString &path)
{
    qucs::Reader reader;

    QFile file(path);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return false;
    }
    reader.setDevice(&file);
    auto qucsSchematic = reader.readSchematic();
    if (qucsSchematic == nullptr) {
        QMessageBox::critical(this, "Error", reader.errorString());
        return false;
    }
    file.close();

    qucs::Converter converter;
    converter.setLibraryManager(m_libraryManager);
    auto schematic = converter.convert(qucsSchematic);

    triggerActionForTool(m_selectTool);

    // FIXME: cleanup the scene
    m_scene->removeItem(m_grid);
    m_scene->removeItem(m_sheet);
    m_scene->clear();
    setupGraphicsView();

    QList<QGraphicsItem *> items;
    for (auto component: schematic->components()) {
        auto symbol = m_libraryManager->createSymbol(component->componentName, component->symbolName);
        if (symbol == nullptr) {
            qWarning() << "No symbol found for" << component->componentName << component->symbolName;
            continue;
        }
        auto symbolItem = new SymbolItem();
        symbolItem->setSymbol(symbol);
        symbolItem->setPos(component->location);
        if (component->isXmirrored)
            symbolItem->setTransform(QTransform( 1,  0, 0,
                                                 0, -1, 0,
                                                 0,  0, 1), /*combine=*/true);
        symbolItem->setRotation(component->rotation);
        auto attribute = symbolItem->addAttribute("RefDes", component->designator);
        attribute->setNameVisible(false);
        attribute->setValueVisible(true);
        for (auto param: component->parameterTable.items) {
            auto paramAttribute = symbolItem->addAttribute(param.name, param.value);
            paramAttribute->setNameVisible(param.showName);
            paramAttribute->setValueVisible(param.showValue);
        }
        symbolItem->setData(0, QVariant::fromValue(component));
        items.append(symbolItem);
    }
    for (auto wire: schematic->wires()) {
        auto wireItem = new WireItem();
        wireItem->addPoint(wire->p1);
        wireItem->addPoint(wire->p2);
        if (wire->netLabel != nullptr) {
            auto attribute = wireItem->addAttribute("NetName", wire->netLabel->net);
            attribute->setNameVisible(false);
        }
        items.append(wireItem);
    }
    for (auto tie: schematic->netTies()) {
        auto tieItem = new NetTieItem();
        tieItem->setTieStyle(tie->style);
        tieItem->setPos(tie->pos);
        tieItem->setRotation(tie->rotation);
        auto attribute = tieItem->addAttribute("NetName", tie->net);
        attribute->setNameVisible(false);
        attribute->setValueVisible(false);
        items.append(tieItem);
    }
    for (auto port: schematic->ports()) {
        auto portItem = new SheetPortItem();
        portItem->setPortStyle(port->style);
        portItem->setPos(port->pos);
        portItem->setRotation(port->rotation);
        auto attribute = portItem->addAttribute("NetName", port->net);
        attribute->setNameVisible(false);
        attribute->setValueVisible(true);
        items.append(portItem);
    }
    for (auto directive: schematic->directives()) {
        auto directiveItem = new DirectiveItem();
        directiveItem->setText(directive->text);
        directiveItem->setPos(directive->pos);
        directiveItem->setRotation(directive->rotation);
        auto attribute = directiveItem->addAttribute("RefDes", directive->designator);
        attribute->setNameVisible(false);
        attribute->setValueVisible(true);
        for (const auto &name: directive->parameters.keys()) {
            const QString value = directive->parameters.value(name).first;
            const bool displayed = directive->parameters.value(name).second;
            attribute = directiveItem->addAttribute(name, value);
            attribute->setNameVisible(false);
            attribute->setValueVisible(displayed);
        }
        items.append(directiveItem);
    }
    GraphicsItemFactory factory;
    for (auto drawing: schematic->drawings()) {
        auto item = factory.createItem(drawing);
        if (item != nullptr)
            items.append(item);
    }
    for (auto diagram: schematic->diagrams()) {
        auto diagramItem = new DiagramItem();
        diagramItem->setText(diagram->text);
        diagramItem->setPos(diagram->pos);
        diagramItem->setSize(diagram->size);
        items.append(diagramItem);
    }

    // TODO: refactor, extent, sheet size and templates

    QRectF extent;
    for (auto item: items) {
        m_scene->addItem(item);
        extent |= item->mapRectToScene(item->boundingRect());
    }

    switch (schematic->sheetFormat()) {
        case Qs::A3Landscape:
            extent = QRectF(0, 0, 420, 297);
            break;
        case Qs::A3Portrait:
            extent = QRectF(0, 0, 297, 420);
            break;
        case Qs::A4Landscape:
            extent = QRectF(0, 0, 297, 210);
            break;
        case Qs::A4Portrait:
            extent = QRectF(0, 0, 210, 297);
            break;
        case Qs::A5Landscape:
            extent = QRectF(0, 0, 148, 210);
            break;
        case Qs::A5Portrait:
            extent = QRectF(0, 0, 210, 148);
            break;
        case Qs::LetterLandscape:
            extent = QRectF(0, 0, 279.4, 215.9);
            break;
        case Qs::LetterPortrait:
            extent = QRectF(0, 0, 215.9, 279.4);
            break;
        case Qs::AutoSheetFormat:
        default:
            extent = extent.adjusted(-20, -20, 20, 20);
            break;
    }
    m_sheet->setSheetArea(extent);
    if (!schematic->sheetTemplateName().isEmpty()) {
        QFile file(QString("%1/%2.qsym").arg(DATADIR "/templates/", schematic->sheetTemplateName()));
        if (file.open(QFile::ReadOnly)) {
            SymbolReader reader;
            if (reader.readSymbol(&file)) {
                GraphicsItemFactory factory;
                auto symbol = reader.takeSymbol();
                auto templateItem = new QGraphicsItemGroup();
                for (auto drawing: symbol->drawings()) {
                    auto item = factory.createItem(drawing);
                    if (item->type() == GraphicsLabelItem::Type) {
                        const auto props = schematic->sheetTemplateProperties();
                        for (const auto &name: props.keys()) {
                            auto label = qgraphicsitem_cast<GraphicsLabelItem*>(item);
                            label->setText(label->text().replace(QString("${%1}").arg(name), props.value(name)));
                        }
                    }
                    templateItem->addToGroup(item);
                }
                templateItem->setPos(m_sheet->boundingRect().center());
                m_scene->addItem(templateItem);
            }
            else
                qWarning() << QString("Failed to load sheet template '%1': %2").arg(file.fileName(), reader.errorString());
        }
        else
            qWarning() << QString("Could not open/find sheet template '%1'").arg(file.fileName());
    }
    m_view->fitInView(m_sheet->rect().adjusted(-20, -20, 20, 20), Qt::KeepAspectRatio);
    m_view->centerOn(m_sheet->rect().center());

    return true;
}

void SchEditor::activate(MainWindow *mainwindow)
{
    mainwindow->addDockWidget(Qt::LeftDockWidgetArea, m_navigationDockWidget);
    mainwindow->addToolBar(m_toolsToolBar);
}

void SchEditor::placeSymbol(const QString &path)
{
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
        return;
    SymbolReader reader;
    if (!reader.readSymbol(&file))
        return;
    m_placeSymbolTool->setSymbol(reader.takeSymbol());
    triggerActionForTool(m_placeSymbolTool);
}

void SchEditor::loadTools()
{
    m_tools << m_selectTool
            << m_placeJunctionTool
            << m_placeWireTool
            << m_placeSymbolTool;
}

void SchEditor::createActions()
{
    for (auto tool: m_tools) {
        auto action = new QAction(tool->icon(), tool->caption());
        action->setShortcut(tool->shortcut());
        action->setCheckable(true);
        m_toolActionGroup->addAction(action);
        connect(action, &QAction::triggered,
                this, [this, tool]() {
            activateTool(tool);
        });
    }
    m_toolActionGroup->setExclusive(true);
    m_toolActionGroup->actions().first()->setChecked(true);
}

void SchEditor::createToolBars()
{
    m_toolsToolBar = new QToolBar("Electrical tools");
    m_toolsToolBar->addActions(m_toolActionGroup->actions());
}

void SchEditor::createDockWidgets()
{
    m_navigationDockWidget = new NavigationDockWidget();
    m_navigationDockWidget->addNavigationWidget("Libraries", m_libraryWidget);
    m_navigationDockWidget->addNavigationWidget("Examples", m_exampleWidget);
    m_navigationDockWidget->setCurrentText("Examples");
}

void SchEditor::setupGraphicsView(Qs::SheetFormat format)
{
    m_view->setAcceptDrops(true);
    m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setBackgroundBrush(QColor("#eee8d5"));
    m_view->setInteractive(true);
    m_view->setDragMode(QGraphicsView::NoDrag);

    m_view->setInteractive(true);

    m_sheet->setRect(QRectF(QPointF(0, 0), QSizeF(297.0, 210.0)));
    m_sheet->setPen(QPen(QColor("#93a1a1"), 0.0));
    m_sheet->setBrush(QColor("#fdf6e3"));

    m_grid->setRect(m_sheet->rect());
    m_grid->setPos(m_sheet->pos());
    m_grid->setMajorColor(QColor("#fdf6e3"/*"#93a1a1"*/).darker(120));
    m_grid->setMinorColor(QColor("#00000000"));//QColor("#657b83"));
    m_grid->setOriginColor(QColor("#859900"));
    m_grid->setOriginVisible(false);
    m_grid->setStep(2.0); // mm
    m_grid->setMinimumGridSize(25); // pixels
    connect(m_sheet, &GraphicsSheet::sheetAreaChanged,
            m_grid, &GraphicsGrid::setRect);

    m_snapper->setStep(m_grid->step());
    connect(m_grid, &GraphicsGrid::stepChanged,
            m_snapper, &GraphicsSnapper::setStep);

    m_scene->addItem(m_sheet);
    m_scene->addItem(m_grid);
    m_scene->setSceneRect(m_scene->itemsBoundingRect());
    m_view->setScene(m_scene);
    connect(m_sheet, &GraphicsSheet::sheetAreaChanged,
            this, [this](const QRectF &rect) {
        const auto w = rect.width();
        const auto h = rect.height();
        m_scene->setSceneRect(m_sheet->boundingRect().adjusted(-w, -h, w, h));
    });

    // FIXME: Handlers need to be attached after m_view->setScene(m_scene)
    m_zoomHandler->setView(m_view);
    m_zoomHandler->setMaximumZoomFactor(50);
    m_zoomHandler->setMinimumZoomFactor(1);
    m_snapper->setView(m_view);
}

void SchEditor::setupNavigationWidgets()
{
    m_libraryWidget->setInitialPath(DATADIR "/library");
    connect(m_libraryWidget, &LibraryNavigationWidget::fileActivated,
            this, &SchEditor::placeSymbol);
    m_exampleWidget->setPath(DATADIR "/qucs-sch-examples");
    connect(m_exampleWidget, &ExampleNavigationWidget::fileActivated,
            this, &SchEditor::open);
}

void SchEditor::setupUndoStack()
{

}

void SchEditor::setupClipBoard()
{

}

void SchEditor::setupLibraryManager()
{
    m_libraryManager->addSystemPath(DATADIR "/../components/spice/");
    m_libraryManager->loadLibraries();
}

sym::Symbol *SchEditor::loadSymbol(const QString &path)
{
    QFile file(path);
    if (!file.open(QFile::ReadOnly)) {
        qWarning() << file.errorString();
        return nullptr;
    }
    SymbolReader reader;
    if (!reader.readSymbol(&file)) {
        qWarning() << reader.errorString();
        return nullptr;
    }
    return reader.takeSymbol();
}

void SchEditor::triggerActionForTool(SchGraphicsTool *tool)
{
    for (auto action: m_toolActionGroup->actions())
        if (action->text() == tool->caption()) {
            action->trigger();
            return;
        }
    Q_UNREACHABLE();
}

void SchEditor::activateTool(SchGraphicsTool *tool)
{
    if (m_activeTool != nullptr)
        m_activeTool->desactivate();
    m_activeTool = tool;
    if (m_activeTool != nullptr)
        m_activeTool->activate(this, QPointF(), QPointF());
}
