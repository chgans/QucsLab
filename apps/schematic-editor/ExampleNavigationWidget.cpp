/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ExampleNavigationWidget.h"

#include <QtWidgets/QFileSystemModel>
#include <QtWidgets/QListView>
#include <QtWidgets/QVBoxLayout>

ExampleNavigationWidget::ExampleNavigationWidget(QWidget *parent)
    : QWidget(parent)
    , m_view(new QListView())
    , m_model(new QFileSystemModel(this))
{
    m_model->setReadOnly(true);
    m_model->setNameFilters(QStringList() << "*.sch");
    m_model->setFilter(QDir::Files | QDir::Readable | QDir::Writable);
    m_view->setModel(m_model);

    connect(m_view, &QListView::activated,
            this, &ExampleNavigationWidget::openItem);

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_view);
}

ExampleNavigationWidget::~ExampleNavigationWidget()
{

}

void ExampleNavigationWidget::setPath(const QString &path)
{
    m_model->setRootPath(path);
    m_view->setRootIndex(m_model->index(path));
}

QString ExampleNavigationWidget::path() const
{
    return m_model->rootPath();
}

void ExampleNavigationWidget::openItem(const QModelIndex &index)
{
    emit fileActivated(m_model->filePath(index));
}

