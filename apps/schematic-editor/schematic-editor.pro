#-------------------------------------------------
#
# Project created by QtCreator 2017-05-13T15:26:17
#
#-------------------------------------------------

QT += core gui widgets

TARGET = schematic-editor
TEMPLATE = app

DEFINES += DATADIR=\\\"$$PWD/../../data/schematics\\\"

SOURCES += main.cpp\
        MainWindow.cpp \
    SchEditor.cpp \
    LibraryNavigationWidget.cpp \
    SchGraphicsTool.cpp \
    WireItem.cpp \
    SymbolItem.cpp \
    JunctionItem.cpp \
    NetGraph.cpp \
    NavigationDockWidget.cpp \
    ExampleNavigationWidget.cpp \
    AttributeItem.cpp \
    ConfigurationTableModel.cpp \
    ConfigurationItemDelegate.cpp \
    ConfigurationTableWidget.cpp

HEADERS  += MainWindow.h \
    SchEditor.h \
    LibraryNavigationWidget.h \
    SchGraphicsTool.h \
    WireItem.h \
    SymbolItem.h \
    JunctionItem.h \
    NetGraph.h \
    NavigationDockWidget.h \
    ExampleNavigationWidget.h \
    AttributeItem.h \
    ConfigurationTableModel.h \
    ConfigurationItemDelegate.h \
    ConfigurationTableWidget.h


# FIXME:
SOURCES += ../symbol-editor/GraphicsItemFactory.cpp
HEADERS += ../symbol-editor/GraphicsItemFactory.h


include($$top_srcdir/QucsLab.pri)
include($$top_srcdir/libs/qlCore/qlCore.pri)
include($$top_srcdir/libs/qlGui/qlGui.pri)
