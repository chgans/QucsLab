#include "JunctionItem.h"

#include <QtGui/QPainter>

JunctionItem::JunctionItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_color(Qt::blue)
    , m_size(0.5)
    , m_rect(-m_size/2.0, -m_size/2.0, m_size, m_size)
{
    //setFlag(ItemIgnoresTransformations);
    setFlag(ItemIsSelectable);
    setEnabled(true);
    setZValue(150);
}

JunctionItem::~JunctionItem()
{

}

QRectF JunctionItem::boundingRect() const
{
    return m_rect;
}

QPainterPath JunctionItem::shape() const
{
    QPainterPath path;
    path.addEllipse(m_rect);
    return path;
}

void JunctionItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setBrush(m_color);
    if (isSelected())
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
    else
        painter->setPen(Qt::NoPen);
    painter->drawEllipse(m_rect);
}

QVariant JunctionItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    Q_UNUSED(change);
    return value;
}
