#include "MainWindow.h"
#include "SchEditor.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_editor(new SchEditor())
{
    setCentralWidget(m_editor);
    m_editor->activate(this);
}

MainWindow::~MainWindow()
{

}
