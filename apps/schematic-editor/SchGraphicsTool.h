#ifndef SCHGRAPHICSTOOL_H
#define SCHGRAPHICSTOOL_H

#include "qlGui/GraphicsEventHandler.h"

#include <QtCore/QObject>
#include <QtCore/QPointF>

namespace sym {
    class Symbol;
}

class SchEditor;
class WireItem;
class JunctionItem;
class SymbolItem;

class SchGraphicsTool : public QObject, public GraphicsSceneEventHandler
{
    Q_OBJECT
public:
    explicit SchGraphicsTool(QObject *parent = nullptr);
    ~SchGraphicsTool();

    // TODO: properties of the tool bundle
    virtual QString caption() const = 0;
    virtual QString description() const = 0;
    virtual QKeySequence shortcut() const = 0;
    virtual QIcon icon() const = 0;

    // TBD: Might want to separate attach/detach from activate/reactivate/desactivate
    // TODO: let the tool get the current pos/snappedPos from the editor
    virtual void activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos) = 0;
    virtual void desactivate() = 0;

};


class SelectTool: public SchGraphicsTool
{
public:
    explicit SelectTool(QObject *parent = nullptr);
    ~SelectTool();

private:
    SchEditor *m_editor = nullptr;

    // SchGraphicsTool interface
public:
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;
    virtual void activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlaceWireTool: public SchGraphicsTool
{
public:
    explicit PlaceWireTool(QObject *parent = nullptr);
    ~PlaceWireTool();

    // SchGraphicsTool interface
public:
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;
    virtual void activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;

private:
    enum State {
        WaitForFirstPoint,
        WaitForNextPoint
    };
    enum Quadrant {
        Quadrant1,
        Quadrant2,
        Quadrant3,
        Quadrant4,
    };

    State m_state = WaitForFirstPoint;
    SchEditor *m_editor = nullptr;
    QScopedPointer<WireItem> m_wire;
    static Quadrant quadrant(const QPointF &p1, const QPointF &p2);
    void newWire(const QPointF &pos = QPointF());
    void newPoint(const QPointF &pos);

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneKeyPressEvent(GraphicsScene *scene, QKeyEvent *event) override;
    virtual bool graphicsSceneKeyReleaseEvent(GraphicsScene *scene, QKeyEvent *event) override;
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlaceJunctionTool: public SchGraphicsTool
{
public:
    explicit PlaceJunctionTool(QObject *parent = nullptr);
    ~PlaceJunctionTool();

    // SchGraphicsTool interface
public:
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;
    virtual void activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;

private:
    SchEditor *m_editor = nullptr;
    JunctionItem *m_item = nullptr;
    void newItem(const QPointF &pos);

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};

class PlaceSymbolTool: public SchGraphicsTool
{
public:
    explicit PlaceSymbolTool(QObject *parent = nullptr);
    ~PlaceSymbolTool();

    void setSymbol(sym::Symbol *symbol);

    // SchGraphicsTool interface
public:
    virtual QString caption() const override;
    virtual QString description() const override;
    virtual QKeySequence shortcut() const override;
    virtual QIcon icon() const override;
    virtual void activate(SchEditor *editor, const QPointF &pos, const QPointF &sourcePos) override;
    virtual void desactivate() override;

private:
    SchEditor *m_editor = nullptr;
    sym::Symbol *m_symbol = nullptr;
    SymbolItem *m_item = nullptr;
    void newItem(const QPointF &pos);

    // GraphicsSceneEventHandler interface
public:
    virtual bool graphicsSceneKeyPressEvent(GraphicsScene *scene, QKeyEvent *event) override;
    virtual bool graphicsSceneKeyReleaseEvent(GraphicsScene *scene, QKeyEvent *event) override;
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent) override;
};
#endif // SCHGRAPHICSTOOL_H
