#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class SchEditor;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    SchEditor *m_editor;
};

#endif // MAINWINDOW_H
