#ifndef CONFIGURATIONITEMDELEGATE_H
#define CONFIGURATIONITEMDELEGATE_H

#include <QtWidgets/QStyledItemDelegate>

class QComboBox;
class QLineEdit;

// Bool, Int, Float, Choice, String, URL, Single-line text, Multi-line text
class ConfigurationItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    ConfigurationItemDelegate(QObject *parent = 0);


private:
    QWidget *createBoolEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const;
    QWidget *createIntEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const;
    QWidget *createFloatEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const;
    QWidget *createChoiceEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const;
    QWidget *createStringEditor(QWidget *parent, const QString &defaultValue, const QString &restriction) const;
    void setBoolEditorData(QWidget *editor, const QString &value) const;
    void setIntEditorData(QWidget *editor, const QString &value) const;
    void setFloatEditorData(QWidget *editor, const QString &value) const;
    void setChoiceEditorData(QWidget *editor, const QString &value) const;
    void setStringEditorData(QWidget *editor, const QString &value) const;
    QString boolEditorData(QWidget *editor) const;
    QString intEditorData(QWidget *editor) const;
    QString floatEditorData(QWidget *editor) const;
    QString choiceEditorData(QWidget *editor) const;
    QString stringEditorData(QWidget *editor) const;

    // QItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    //void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // CONFIGURATIONITEMDELEGATE_H
