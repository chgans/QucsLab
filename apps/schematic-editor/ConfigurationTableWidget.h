#ifndef CONFIGURATIONTABLEWIDGET_H
#define CONFIGURATIONTABLEWIDGET_H

namespace lib
{
    class Cell;
}

namespace sch
{
    class Component;
}

class ConfigurationTableModel;
class QTableView;
class QLineEdit;
class QSortFilterProxyModel;

#include <QtWidgets/QWidget>

class ConfigurationTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ConfigurationTableWidget(QWidget *parent = 0);

    void setCellInstance(/*const */sch::Component *sch, const lib::Cell *lib);

signals:

public slots:

private:
    ConfigurationTableModel *m_model;
    QSortFilterProxyModel *m_sortFilterProxyModel;
    QTableView *m_view;
    QLineEdit *m_filterLineEdit;
};

#endif // CONFIGURATIONTABLEWIDGET_H
