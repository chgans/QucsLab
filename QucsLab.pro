TEMPLATE = subdirs

SUBDIRS += \
    libs \
    tests \
    apps \
    3rdparty

OTHER_FILES += QucsLab.pri .qmake.conf README.md qlib-format.md \
  .gitignore .gitlab-ci.yml

CONFIG += ordered
